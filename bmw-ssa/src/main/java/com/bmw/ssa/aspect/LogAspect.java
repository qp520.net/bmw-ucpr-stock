package com.bmw.ssa.aspect;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * Created by test on 2019/6/28.
 */
@Aspect
@Component
@Slf4j
public class LogAspect {
    @Before("within(com.bmw.ssa.controller.* || com.bmw.ssa.controller.*.*)")
    public void before(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Method method = signature.getMethod();
//        Object[] args = joinPoint.getArgs();
        log.warn("{}.{}:请求参数：{}", method.getDeclaringClass().getName(), method.getName(), StringUtils.join(JSON.toJSONString(request.getParameterMap()), ";"));
    }
    @AfterReturning(value = "within(com.bmw.ssa.controller.* || com.bmw.ssa.controller.*.*)",returning = "rvt")
    public void after(JoinPoint joinPoint, Object rvt){
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        log.warn("{}.{}:返回数据：{}",method.getDeclaringClass().getName(),method.getName(), JSON.toJSONString(rvt));
    }
}
