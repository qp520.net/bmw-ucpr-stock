package com.bmw.ssa.configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.aop.aspectj.AspectJExpressionPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.interceptor.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock
 *
 * @author Bruce Maa
 * @since 2019-07-10.10:48
 */
@EnableTransactionManagement
@Configuration
@MapperScan(basePackages = "com.bmw.ssa.repository")
public class MybatisPlusConfig {

    /**
     * 配置事务
     * @return  事务属性
     */
    private TransactionAttributeSource transactionAttributeSource() {
        NameMatchTransactionAttributeSource matchTransactionAttributeSource = new NameMatchTransactionAttributeSource();

        // 只读事务
        RuleBasedTransactionAttribute readOnlyTx = new RuleBasedTransactionAttribute();
        readOnlyTx.setReadOnly(true);
        readOnlyTx.setPropagationBehavior(TransactionDefinition.PROPAGATION_SUPPORTS);

        // 读写事务
        RuleBasedTransactionAttribute requiredTx = new RuleBasedTransactionAttribute(
                TransactionDefinition.PROPAGATION_REQUIRED,
                Collections.singletonList(new RollbackRuleAttribute(Exception.class))
        );

        Map<String, TransactionAttribute> txMap = new HashMap<>(10);
        txMap.put("add*", requiredTx);
        txMap.put("save*", requiredTx);
        txMap.put("insert*", requiredTx);
        txMap.put("update*", requiredTx);
        txMap.put("delete*", requiredTx);
        txMap.put("get*", readOnlyTx);
        txMap.put("query*", readOnlyTx);
        txMap.put("find*", readOnlyTx);
        txMap.put("select*", readOnlyTx);
        txMap.put("list*", readOnlyTx);

        matchTransactionAttributeSource.setNameMap(txMap);

        return matchTransactionAttributeSource;
    }

    /**
     * 配置事务拦截器
     * @param tx    事务控制
     * @return  事务拦截器
     */
    @Bean(value = "txInterceptor")
    public TransactionInterceptor getTransactionInterceptor(PlatformTransactionManager tx) {
        return new TransactionInterceptor(tx, transactionAttributeSource());
    }

    /**
     * 配置切面拦截
     * @param txInterceptor 拦截器
     * @return  切面
     */
    @Bean
    public AspectJExpressionPointcutAdvisor pointcutAdvisor(TransactionInterceptor txInterceptor) {
        AspectJExpressionPointcutAdvisor pointcutAdvisor = new AspectJExpressionPointcutAdvisor();
        pointcutAdvisor.setAdvice(txInterceptor);
        pointcutAdvisor.setExpression("execution (* com.bmw.ssa.service.impl.*ServiceImpl.*(..))");
        return pointcutAdvisor;
    }
    /**
     * 分页插件
     * @return  分页拦截器
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
