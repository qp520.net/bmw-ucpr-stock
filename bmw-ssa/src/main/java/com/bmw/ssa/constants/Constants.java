package com.bmw.ssa.constants;

/**
 * Created by Administrator on 2019/8/28 0028.
 */
public interface Constants {

    public final static String OPERTION_TYPE_0 = "0";
    public final static String OPERTION_TYPE_1 = "1";
    public final static String OPERTION_TYPE_2 = "2";

    public final static String GET_INSURANCE_SEARCH_CN_8 = "8";
    public final static String GET_INSURANCE_SEARCH_EN_1 = "1";


    public final static String GET_COMMON_DEALER_27077 = "27077";

    public final static int ASA_STATUS_10 = 10;

    public final static int ACCOUNT_27077 = 27077;

    public final static String CUSTOMER_CATEGORY_57 = "57";

    public final static String CUSTOMER_CATEGORY_59 = "59";

    public final static String CUSTOMER_CATEGORY_60 = "60";

    public final static String CUSTOMER_CATEGORY_61 = "61";

    public final static String CUSTOMER_CATEGORY_70 = "70";

    public final static String CUSTOMER_CATEGORY_80 = "80";

    public final static String CUSTOMER_CATEGORY_81 = "81";

    public final static String CUSTOMER_CATEGORY_90 = "90";

    public final static String CUSTOMER_CATEGORY_92 = "92";

    public final static String CUSTOMER_CATEGORY_93 = "93";

    public final static String CUSTOMER_CATEGORY_94 = "94";

    public final static String CUSTOMER_CATEGORY_95 = "95";

    public final static String CUSTOMER_CATEGORY_96 = "96";

    public final static String CUSTOMER_CATEGORY_97 = "97";

    public final static String CUSTOMER_CATEGORY_99 = "99";

    public final static String DISC_0 = "0.0";

    public final static String SPECIALCASE_VIN = "VIN";

    public final static String SPECIALCASE_ASA = "ASA";






}
