package com.bmw.ssa.constants;

/**
 * Created by Administrator on 2019/9/5 0005.
 */
public interface ErrorCode {
    public final static int ERROR_CODE_5003 = 50003;

    public final static String ERROR_MSG_5003 = "我的自定义异常";

    public final static int ERROR_CODE_4000 = 4000;
    public final static String ERROR_MSG_4000 = "缺少必要参数";
}
