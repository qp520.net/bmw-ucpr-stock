package com.bmw.ssa.controller;

import com.bmw.core.web.controller.BaseController;
import com.bmw.ssa.entity.UserAccount;
import com.bmw.ssa.po.CheckforFleetAccountPo;
import com.bmw.ssa.service.AccountService;
import com.bmw.ssa.service.UserAccountService;
import com.bmw.ssa.vo.CheckforFleetAccountVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/8/16 0016.
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/accountController")
@Api(value = "/accountController")
public class AccountController extends BaseController{

    private final AccountService accountService;
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/checkforFleetAccount",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/checkforFleetAccount",notes = "登陆接口",response = CheckforFleetAccountPo.class)
    public ResponseEntity checkforFleetAccount(CheckforFleetAccountVo checkforFleetAccountVo){
        log.debug("开始请求");
        log.debug("请求结束！！！");
        return selectResponse(accountService.checkforFleetAccount(checkforFleetAccountVo));
    }
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/searchkeyAction",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/searchkeyAction",notes = "验证接口",response = CheckforFleetAccountPo.class)
    public ResponseEntity searchkeyAction(CheckforFleetAccountVo checkforFleetAccountVo){
        return selectResponse(accountService.getKeyASA(checkforFleetAccountVo));
    }
}
