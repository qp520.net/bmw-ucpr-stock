package com.bmw.ssa.controller;

import com.bmw.core.web.controller.BaseController;
import com.bmw.ssa.entity.emailVerify.EmailVerifyAccount;
import com.bmw.ssa.service.EmailVerifyService;
import com.bmw.ssa.vo.emailVerify.EmailDomainParameterVo;
import com.bmw.ssa.vo.emailVerify.EmployeeParameterVo;
import com.bmw.ssa.vo.emailVerify.FleetAccountsInfoVo;
import com.bmw.ssa.vo.emailVerify.FleetAccountsParameterVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/9/19 0019.
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/emailVerify")
@Api(value = "/emailVerify",description = "员工邮箱认证")
public class EmailVerifyController extends BaseController{
    private final EmailVerifyService emailVerifyService;

    @RequestMapping(value = "/searchFleetAccounts",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/searchFleetAccounts",notes = "查询用户列表",response = FleetAccountsInfoVo.class)
    public ResponseEntity searchFleetAccounts(FleetAccountsParameterVo fleetAccountsParameterVo){
        return pageResponse(emailVerifyService.getFleetAccount(fleetAccountsParameterVo));
    }

    @RequestMapping(value = "/searchEmployeeEmail",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/searchEmployeeEmail",notes = "员工邮箱列表",response = EmailVerifyAccount.class)
    public ResponseEntity searchEmployeeEmail(EmployeeParameterVo employeeParameterVo){
        return pageResponse(emailVerifyService.getEmployeeEmail(employeeParameterVo));
    }

    @RequestMapping(value = "/getEmailDoMain",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getEmailDoMain",notes = "获取邮箱后缀")
    public ResponseEntity getEmailDoMainList( EmailDomainParameterVo emailDomainParameterVo)throws Exception{
        return selectResponse(emailVerifyService.getEmailDomainsASA(emailDomainParameterVo));
    }

}
