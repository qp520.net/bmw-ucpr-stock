package com.bmw.ssa.controller;

import com.bmw.core.web.controller.BaseController;
import com.bmw.ssa.excel.ExcelUtil;
import com.bmw.ssa.po.fleetCustomer.CustomerSearchPo;
import com.bmw.ssa.service.FleetCustomerService;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerBasicsTempVo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerMainSearchVo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerOtherVo;
import com.netflix.client.http.HttpResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/9/24 0024.
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/fleetCustomer")
@Api(value = "/fleetCustomer",description = "经销商职位")
public class FleetCustomerController extends BaseController {

    private final FleetCustomerService fleetCustomerService;

    @RequestMapping(value = "/searchList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/searchList", notes = "查询分页列表", response = CustomerSearchPo.class)
    public ResponseEntity searchList(SsaCustomerMainSearchVo ssaCustomerMainSearchVo) {
        return pageResponse(fleetCustomerService.getCustomerPageList(ssaCustomerMainSearchVo));
    }

    @RequestMapping(value = "/importCustomerList", method = RequestMethod.POST)
    @ApiOperation(value = "/importCustomerList", notes = "导入经销商列表数据")
    public ResponseEntity importCustomerList(MultipartFile excel, @RequestBody SsaCustomerOtherVo ssaCustomerOtherVo) throws Exception {
        if (ssaCustomerOtherVo.getType() == 1) {
            return saveResponse(fleetCustomerService.importCustomerList(excel));
        } else {
            return saveResponse(fleetCustomerService.importCustomerOtherList(excel));
        }
    }

    @RequestMapping(value = "/doUpdate",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/doUpate",notes = "更新操作")
    public ResponseEntity doUpdate(@RequestBody CustomerSearchPo customerSearchPo)throws Exception{
        return updateResponse(fleetCustomerService.doUpdate(customerSearchPo));
    }

    @RequestMapping(value = "/searchOtherList",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/searchOtherList",notes = "查询其他列表",response = SsaCustomerOtherVo.class)
    public ResponseEntity searchOtherList(SsaCustomerOtherVo ssaCustomerOtherVo){
        return pageResponse(fleetCustomerService.getCustomerOtherPageList(ssaCustomerOtherVo));
    }


    @RequestMapping(value = "/exportCustomerList",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/exportCustomerList",notes = "导出excel,type为1时有数据，下载模板为0")
    public void exportCustomerList(SsaCustomerMainSearchVo ssaCustomerMainSearchVo, int type, HttpServletResponse response){
        List<SsaCustomerBasicsTempVo> list = new ArrayList<>();
        if(type==1){
            list = fleetCustomerService.getCustmerList(ssaCustomerMainSearchVo);
        }
        String fileName = "customerList";
        String sheetName = "sheet";
        ExcelUtil.writeExcel(response,list,fileName,sheetName,new SsaCustomerBasicsTempVo());
    }

    @RequestMapping(value = "/exportOtherList",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/exportOtherList",notes = "导出其他信息,type为1时有数据，下载模板为0")
    public void exportOtherList(HttpServletResponse response,SsaCustomerOtherVo ssaCustomerOtherVo,int type){
        List<SsaCustomerOtherVo> list =new ArrayList<>();
        if(type==1){
            list = fleetCustomerService.getOtherList(ssaCustomerOtherVo);
        }
        String fileName = "otherList";
        String sheetName = "sheet";
        ExcelUtil.writeExcel(response,list,fileName,sheetName,new SsaCustomerOtherVo());
    }

    @RequestMapping(value = "/getInitSearchData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getInitSearchData",notes = "获取初始化数据",response =SsaCustomerMainSearchVo.class )
    @ApiImplicitParams({
            @ApiImplicitParam(value = "国际化标识",name = "languageFlag",dataType = "String",required = true),
            @ApiImplicitParam(value = "dealerid",name = "key",dataType = "String",required = true)
    })
    public ResponseEntity getInitSearchData(@RequestParam(defaultValue = "8") String languageFlag, String key){
        return selectResponse(fleetCustomerService.getInitData(languageFlag,key)) ;
    }
}
