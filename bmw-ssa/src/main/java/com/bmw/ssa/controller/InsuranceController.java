package com.bmw.ssa.controller;

import com.bmw.core.web.controller.BaseController;
import com.bmw.ssa.entity.CheckINSExistVo;
import com.bmw.ssa.entity.DropList;
import com.bmw.ssa.entity.InsuranceDetailData;
import com.bmw.ssa.po.InsuranceDetailDataPo;
import com.bmw.ssa.po.InsuranceHistoryDataPo;
import com.bmw.ssa.service.InsuranceService;
import com.bmw.ssa.vo.InsuranceSearchConditionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@Api(value = "/insuranceController",description = "公积金接口")
@RequestMapping(value = "/insuranceController")
public class InsuranceController extends BaseController {

    private final InsuranceService insuranceService;

    @RequestMapping(value = "/getInsuranceList",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getInsuranceList",notes = "查询公积金申请列表",response = InsuranceDetailDataPo.class)
    public ResponseEntity getInsuranceList(InsuranceSearchConditionVo insuranceSearchConditionVo){
        return pageResponse(insuranceService.getInsuranceList(insuranceSearchConditionVo));
    }


    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/getSearchData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getSearchData",notes = "获取搜索条件",response = InsuranceDetailDataPo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "区域",name = "department",dataType = "String"),
            @ApiImplicitParam(value = "经销商id",name = "customerid",dataType = "String"),
            @ApiImplicitParam(value = "国际化",name = "locale",dataType = "String")
    })
    public ResponseEntity getSearchData(@RequestParam String department,
                                        @RequestParam String locale,@RequestParam String customerid){
        return selectResponse(insuranceService.getSearchDatas(department,locale,customerid));
    }


    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/getInsuranceHistoryData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getInsuranceHistoryData",notes = "获取历史纪录",response = InsuranceHistoryDataPo.class)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "国际化",name = "locale",dataType = "String"),
            @ApiImplicitParam(value = "公积金申请id",name = "insId",dataType = "String")
    })
    public ResponseEntity getInsuranceHistoryData(@RequestParam String locale,@RequestParam  String insId){
        log.debug("请求进来了");
        DropList dropList = new DropList();
        dropList.setLanguageFlag(locale);
        dropList.setId(insId);
        log.debug("请求结束了！！！！！！！！！！！！！！！！！！！！！！！");
        return selectResponse(insuranceService.getinsuranceHistoryList(dropList));
    }

    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/addInsuranceHistory",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/addInsuranceHistory",notes = "写入历史数据")
    public ResponseEntity addInsuranceHistory(@RequestBody InsuranceHistoryDataPo inshPo) {
        return saveResponse(insuranceService.insHistoryInsert(inshPo));
    }


    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/getInsuranceDetail",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getInsuranceDetail",notes = "获取详情数据接口")
    public ResponseEntity getInsuranceDetail(@RequestBody InsuranceSearchConditionVo insuranceSearchConditionVo){
        return selectResponse(insuranceService.getInsuranceDetail(insuranceSearchConditionVo));
    }
    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/doUpdateInsurance",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/doUpdateInsurance",notes = "更新数据")
    public ResponseEntity doUpdateInsurance(@RequestBody InsuranceDetailDataPo insdPo){
        return updateResponse(insuranceService.doUpdate(insdPo));
    }
    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/doInsetInsurance",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/doInsetInsurance",notes = "写入申请数据")
    public ResponseEntity doInsetInsurance(@RequestBody InsuranceDetailDataPo insdPo){
        return saveResponse(insuranceService.doInsert(insdPo));
    }

    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/doApplyInsurance",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/doApplyInsurance",notes = "审批接口")
    public void doApplyInsurance(@RequestBody InsuranceDetailData insFileData){
        insuranceService.doApply(insFileData);
    }
    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/doResetInsurance",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/doResetInsurance",notes = "退回申请")
    public ResponseEntity doResetInsurance(@RequestBody InsuranceDetailData inscDetail){
        return updateResponse(insuranceService.doReset(inscDetail));
    }
    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/doRejectInsurance",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/doRejectInsurance",notes = "拒绝申请接口")
    public ResponseEntity doRejectInsurance(@RequestBody InsuranceDetailData insDetail){
        return updateResponse(insuranceService.doReject(insDetail));
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/getInsuranceAppList",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getInsuranceAppList",notes = "公积金审批列表",response = InsuranceDetailDataPo.class)
    public ResponseEntity getInsuranceAppList (@RequestBody InsuranceSearchConditionVo insuranceSearchConditionVo){
        return pageResponse(insuranceService.getInsuranceAppList(insuranceSearchConditionVo));
    }

    @SuppressWarnings("rewtypes")
    @RequestMapping(value = "/checkKinsExistWeb",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/checkKinsExistWeb",notes = "校验身份证号是否申请过")
    public ResponseEntity checkKinsExistWeb(@RequestBody CheckINSExistVo checkINSExistVo) throws JSONException {
        return selectResponse(insuranceService.checkKinsExistWeb(checkINSExistVo));
    }

}
