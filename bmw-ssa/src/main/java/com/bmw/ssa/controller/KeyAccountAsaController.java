package com.bmw.ssa.controller;

import com.bmw.core.web.controller.BaseController;
import com.bmw.ssa.entity.KeyAccountAsaDetailData;
import com.bmw.ssa.po.KeyAccountAsa.CreateKeyAccountAsaPo;
import com.bmw.ssa.po.KeyAccountAsa.KeyAccountAsaSearchResultData;
import com.bmw.ssa.po.KeyAccountAsa.KeyAccountMainPo;
import com.bmw.ssa.service.KeyAccountAsaService;
import com.bmw.ssa.vo.KeyAccountAsaDetailVo;
import com.bmw.ssa.vo.KeyAccountAsaSearchConditionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//region Description
/**
 * Created by Administrator on 2019/8/30 0030.
 */
@RestController
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@Api(value = "/keyAccountAsa",description = "机构协议申请")
@RequestMapping("/keyAccountAsa")
public class KeyAccountAsaController extends BaseController {
    private final KeyAccountAsaService keyAccountAsaService;

//    @RequestMapping(value = "/getCustomerCategory",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    @ApiOperation(value = "/getCustomerCategory",notes = "获取客户类型一级菜单",response = DropList.class)
//    @ApiImplicitParams({
//            @ApiImplicitParam(paramType = "query",name = "languageFlag",value = "国际化标识",dataType = "String",required = true)
//    })
//    public ResponseEntity getCustomerCategory(@RequestParam(value = "languageFlag",required = true)String languageFlag){
//        return selectResponse(keyAccountAsaService.getCustomerCategoryList(languageFlag));
//    }
//
//    @RequestMapping(value = "/getRemarkList",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    @ApiOperation(value = "/getRemarkList",notes = "获取客户类型二级菜单",response = DropList.class)
//    @ApiImplicitParams({
//            @ApiImplicitParam(paramType = "query",name = "languageFlag",value = "国际化标识",dataType = "String",required = true)
//            ,@ApiImplicitParam(paramType = "query",name="panrentId",value = "一级菜单ID",dataType = "String")
//
//    })
//    public ResponseEntity getRemarkList(@RequestParam(value = "languageFlag",required = true)String languageFlag,String panrentId){
//        return selectResponse(keyAccountAsaService.getCategoryList(languageFlag,panrentId));
//    }

    @RequestMapping(value = "/getKeyAccountInnitData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "languageFlag",value = "国际化标识",dataType = "String",required = true)
            ,@ApiImplicitParam(paramType = "query",name = "dealerId",value = "当前用户id",dataType = "String",required = true)
    })
    @ApiOperation(value = "/getKeyAccountInnitData",notes = "初始化查询条件",response = KeyAccountMainPo.class)
    public ResponseEntity getKeyAccountInnitData(@RequestParam(value = "languageFlag",required = true,defaultValue = "8") String languageFlag,@RequestParam(value = "dealerId",required = true) String dealerId){
        return selectResponse(keyAccountAsaService.getKeyAccountMain(languageFlag,dealerId));
    }

    @RequestMapping(value = "/getKeyAccountAsaDetail",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "languageFlag",value = "国际化标识",dataType = "String",required = true)
            ,@ApiImplicitParam(paramType = "query",name = "dealerId",value = "当前用户id",dataType = "String",required = true)
    })
    @ApiOperation(value = "/getKeyAccountAsaDetail",notes = "初始化新增",response = KeyAccountMainPo.class)

    public ResponseEntity getKeyAccountAsaDetail(@RequestParam (value = "languageFlag",defaultValue = "8")String languageFlag,@RequestParam String dealerId ){
        return selectResponse(keyAccountAsaService.initInsert(languageFlag,Integer.valueOf(dealerId)));
    }

    @RequestMapping(value = "/doInsert",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/doInsert",notes = "新增协议接口")
    public ResponseEntity doInsert(@RequestBody CreateKeyAccountAsaPo createKeyAccountAsaPo) throws Exception {
        return saveResponse(keyAccountAsaService.doInsert(createKeyAccountAsaPo));
    }

    //region Description
    @RequestMapping(value = "/getKeyAccountAsa",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getKeyAccountAsa",notes = "协议申请列表",response =KeyAccountAsaSearchResultData.class)
    public ResponseEntity getKeyAccountAsa( KeyAccountAsaSearchConditionVo kasv){
        return pageResponse(keyAccountAsaService.getKeyAccountAsa(kasv));
    }
    //endregion
    @RequestMapping(value = "/doUpdate",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/doUpdate",notes = "更新接口")
     public ResponseEntity doUpdate(@RequestBody CreateKeyAccountAsaPo createKeyAccountAsaPo) throws Exception {
        return saveResponse(keyAccountAsaService.doUpdate(createKeyAccountAsaPo));
     }

    @ApiOperation(value = "/doSelect",notes = "详情接口",response = KeyAccountAsaDetailData.class)
    @RequestMapping(value = "/doSelect",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "国际化标识",name = "languageFlag",paramType = "query",dataType = "String"),
            @ApiImplicitParam(value = "申请纪录唯一标识",name = "kasaId",paramType = "query",dataType = "String")
    })
    public ResponseEntity doSelect(@RequestBody KeyAccountAsaDetailVo kadv){
        return selectResponse(keyAccountAsaService.doSelect(kadv));
    }
}
