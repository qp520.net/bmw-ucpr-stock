package com.bmw.ssa.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ssa.entity.keyComplaint.KeyComplaintManagementSearchFromData;
import com.bmw.ssa.po.KeyAccountAsa.KeyAccountAsaSearchResultData;
import com.bmw.ssa.service.KeyComplainService;
import com.bmw.ssa.vo.keyComplain.KeyComplaintManagementParameterVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/9/4 0004.
 */
@RestController
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@Api(value = "/keyComplaint",description = "客户投诉咨询")
@RequestMapping(value = "/keyComplaint")
public class KeyComplaintController extends BaseController{

    public final KeyComplainService keyComplainService;
    @RequestMapping(value = "/getSreachFormData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getSreachFormData",notes = "初始化投诉查询条件",response = KeyComplaintManagementSearchFromData.class)
    @ApiImplicitParam(value = "国际化标识",name = "language",required = true,paramType = "query",dataType = "String")
    public ResponseEntity getSreachFormData(String language){
        return selectResponse(keyComplainService.getSearchFrom(language));
    }


    @RequestMapping(value = "/ssaComplaint",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/ssaComplaint",notes = "投诉咨询一览表")
    public  ResponseEntity ssaComplaint(KeyComplaintManagementParameterVo kmpv){
        return pageResponse(keyComplainService.ssaComplaint(kmpv));
    }

}
