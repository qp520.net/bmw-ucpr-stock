package com.bmw.ssa.controller;

import com.bmw.core.web.controller.BaseController;
import com.bmw.ssa.service.NationalAccountService;
import com.bmw.ssa.vo.nationalAccount.NationalAccountSearchConditionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/nationalAccount")
@Api(value = "/nationalAccount",description = "全国协议")
public class NationalAccountController extends BaseController {

    @Autowired
    private NationalAccountService nationalAccountService;

    @RequestMapping(value = "/searchData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/searchData")
    public ResponseEntity searchData(NationalAccountSearchConditionVo nationalAccountSearchConditionVo){
        return pageResponse(nationalAccountService.getNationalAccounts(nationalAccountSearchConditionVo));
    }


}