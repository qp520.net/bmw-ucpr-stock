package com.bmw.ssa.controller;

import com.bmw.core.web.controller.BaseController;
import com.bmw.ssa.entity.speciaDiscount.SpecialDiscountApplicationData;
import com.bmw.ssa.po.KeyComplaint.SelectItem;
import com.bmw.ssa.po.specialDiscount.AddInitPo;
import com.bmw.ssa.service.SpecialDiscountService;
import com.bmw.ssa.vo.specialDiscount.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2019/9/9 0009.
 */
@RestController
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RequestMapping(value = "/speciaDiscount")
@Api(value = "/speciaDiscount",description = "特殊协议申请")
public class SpeciaDiscountController extends BaseController {


    public final  SpecialDiscountService speciaDiscountService;
    @RequestMapping(value = "/getSpecialDiscountApplicationData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/getSpecialDiscountApplicationData",notes = "获取协议申请列表",response = SpecialDiscountApplicationData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "国际化",name="language",dataType = "String"),
            @ApiImplicitParam(value = "经销商id",name="customerId",dataType = "String"),
            @ApiImplicitParam(value = "用户id",name="dealerId",dataType = "String"),
            @ApiImplicitParam(value = "当前登录",name="accountId",dataType = "String")
    })
    @SuppressWarnings("rawtypes")
    public ResponseEntity getSpecialDiscountApplicationData(@RequestParam(defaultValue = "8") String language, String customerId, String  dealerId, String accountId){
        SpecialDiscountApplicationVo spv = new SpecialDiscountApplicationVo();
        spv.setLanguage(language);
        spv.setCustomerId(customerId);
        spv.setAccountId(accountId);
        spv.setDealerId(dealerId);
        return selectResponse(speciaDiscountService.getSpecialDiscountApplicationData(spv));
    }

    @ApiOperation(value = "/getSearchInitData",notes = "初始化查询条件",response = SelectItem.class)
    @RequestMapping(value = "/getSearchInitData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SuppressWarnings("rawtypes")
    public ResponseEntity getSearchInitData(@RequestBody SearchInitVo searchInitVo){
        return selectResponse(speciaDiscountService.getSearchInitData(searchInitVo));
    }

    @ApiOperation(value = "/initAddDate",notes = "初始化新增数据",response = AddInitPo.class)
    @RequestMapping(value = "/initAddData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initAddData(@RequestBody SearchInitVo searchInitVo){
        return selectResponse(speciaDiscountService.getAddInitData(searchInitVo));
    }


    @RequestMapping(value = "/addSda",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/addSda",notes = "新增接口",response = SsaSda.class)
    public ResponseEntity addSda(@RequestBody SsaSda sda) throws Exception{
        return saveResponse(speciaDiscountService.insertSda(sda));
    }

    @ApiOperation(value = "/updateSda",notes = "编辑接口")
    @RequestMapping(value = "/updateSda",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity updateSda(@RequestBody SsaSda ssaSda)throws Exception{
        return updateResponse(speciaDiscountService.updateSda(ssaSda));
    }

    @ApiOperation(value = "/deleteSdaData",notes = "删除sda接口")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "sdaId",name = "sdaId",required = true,dataType = "String")
    })
    @RequestMapping(value = "/deleteSdaData",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity deleteSdaData(String sdaId) throws Exception{
        speciaDiscountService.deleteSda(sdaId);
        return deleteResponse();
    }

    @ApiOperation(value = "/modifyInit",notes = "修改申请初始化")
    @RequestMapping(value = "/modifyInit",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity modifyInit(String sdkNo){
        return selectResponse(speciaDiscountService.modifyGetSDA(sdkNo));
    }

    @ApiOperation(value = "/termSdaProc",notes = "终止接口")
    @RequestMapping(value = "/termSdaProc",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity termSdaProc(@RequestBody ProcVo procVo){
        speciaDiscountService.termSdaProc(procVo);
        return saveResponse(null);
    }

    @ApiOperation(value = "/modify",notes = "修改申请")
    @RequestMapping(value = "/modify",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity modify(@RequestBody SDAModifyVo vo) throws Exception{
        return updateResponse(speciaDiscountService.modifyOk(vo)) ;
    }


    @ApiOperation(value = "/getDisc",notes = "获取折扣信息")
    @RequestMapping(value = "/getDisc",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getDisc(DiscountVo discountVo){
        return selectResponse(speciaDiscountService.getDiscByCategory(discountVo));
    }

}
