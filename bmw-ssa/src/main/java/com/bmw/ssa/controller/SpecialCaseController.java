package com.bmw.ssa.controller;

import com.bmw.core.web.controller.BaseController;
import com.bmw.ssa.excel.ExcelUtil;
import com.bmw.ssa.service.SpecialCaseService;
import com.bmw.ssa.vo.specialCase.ExportInfo;
import com.bmw.ssa.vo.specialCase.SpecialCaseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2019/9/23 0023.
 */
@Slf4j
@Api(value = "/specialCase",description = "特殊审批项")
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/specialCase")
public class SpecialCaseController extends BaseController{
    private final SpecialCaseService specialCaseService;


    @RequestMapping(value = "/getList",method = RequestMethod.GET)
    @ApiOperation(value = "/getList",notes = "查询分页列表",response =SpecialCaseVo.class )
    public ResponseEntity getList(int pageNum,int pageSize){
        return pageResponse(specialCaseService.getSpecialCaseList(pageNum,pageSize));
    }

    @RequestMapping(value = "/insertSepc",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/insertSepc",notes = "写入纪录")
    public ResponseEntity insertSepc(@RequestBody SpecialCaseVo specialCaseVo, MultipartFile excel){
        return saveResponse(specialCaseService.saveSpcialCase(specialCaseVo,excel));
    }

    @RequestMapping(value = "/writeExcel",method = RequestMethod.GET)
    @ApiOperation(value = "/writeExcel",notes = "导出excel")
    public void writeExcel(HttpServletResponse response,SpecialCaseVo specialCaseVo) throws IOException{
        List<ExportInfo> list = specialCaseService.getSpecialCaseVinByNo(specialCaseVo);
        String fileName = "specialCase";
        String sheetName = "sheet";
        ExcelUtil.writeExcel(response,list,fileName,sheetName,new ExportInfo());
    }
    @RequestMapping(value = "/edit",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/edit",notes = "编辑")
    public ResponseEntity edit(@RequestBody SpecialCaseVo specialCaseVo,MultipartFile excel){
        return updateResponse(specialCaseService.updateSpecialCaseByScNo(specialCaseVo,excel));
    }

    @RequestMapping(value = "/detail",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/detail",notes = "获取详情",response = SpecialCaseVo.class)
    public ResponseEntity specialCaseDetail(SpecialCaseVo specialCaseVo){
        return selectResponse(specialCaseService.getSpecialCaseByNo(specialCaseVo));
    }
}
