package com.bmw.ssa.controller;

import com.bmw.core.web.controller.BaseController;
import com.bmw.core.web.exception.BmwException;
import com.bmw.core.web.util.ResponseUtils;
import com.bmw.ssa.entity.UserAccount;
import com.bmw.ssa.service.UserAccountService;
import com.bmw.ssa.vo.LoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2019/8/15 0015.
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/userAccountController")
@Api(value = "/userAccountController")
public class UserAccountController extends BaseController{

    private final UserAccountService userAccountService;
    /**
     * getUserByUserName
     *
     * @return UserAccount
     * -----------------------------------------------------------------------------------------------------------------
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/login",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "/login",notes = "登陆接口",response = UserAccount.class)
    public ResponseEntity getUserByUserName(LoginVo loginVo) {
          UserAccount userAccount=  userAccountService.findUserByUserName(loginVo);
          return selectResponse(userAccount);
    }



}
