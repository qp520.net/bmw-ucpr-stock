package com.bmw.ssa.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by Administrator on 2019/8/21 0021.
 */
@ApiModel
@Data
public class CheckINSExistVo {
    private String insId;
    @ApiModelProperty(value = "用户id")
    private String dealerId;
    @ApiModelProperty(value = "身份证号")
    private String idNo;
    @ApiModelProperty(value = "国际化标识")
    private String languageFlag="8";

    private String typ;

    private String year;
    @ApiModelProperty(value = "姓名")
    private String nameCN;
    @ApiModelProperty(value = "公司全称")
    private String companyCN;

}
