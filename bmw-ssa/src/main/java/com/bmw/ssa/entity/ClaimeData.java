package com.bmw.ssa.entity;


import lombok.Data;

/**
* Project：BMW SSA Web Base
* System：SSA
* Sub System：fleetAccount
* [Annual Sales Agreement(ASA)]tab用bean
* @author lijiwei
*
 */
@Data
public class ClaimeData{
	private String statusText;
	private String dealerChanged;
	private String dealerID;
	private String dealerName;
	private String tradeType;
	private String series;
	private String eSeries;
	private String model;
	private String modelCode;
	private String variant;
	private String vin;
	private String invoicePriceRMB;
	private String retailDate;
	private String handoverDate;
	private String fleetAccountNo;
	private String dmsCustNo;
	private String asaStart;
	private String asaEnd;
	private String company;
	private String subBranch;
	private String employee;
	private String invoiceName;
	private String customerCategory;
	private String asaType;
	private String discountToCustomerPercent;
	private String stickerPriceRMB;
	private String bmwSubsidy;
	private String bmwSubsidyRmb;
	private String bbaNsc;
	private String companyPrivateVehicleType;
	private String asaSdaNo;
	private String sequence;
	private String dealerSubsPercent;
	private String dealerSubsRMB;
	private String plusAmountRMB;
	private String plusReason;
	private String plusComment;
	private String minusAmountRMB;
	private String minusReason;
	private String minusComment;
	private String amountNetRmb;
	private String amountVATRMB;
	private String bmwFinalSupportRMB;
	private String bmwFinalSupportPercent;
	private String paymentReferenceNo;
	private String vkhNumber;
	private String compaign;
	private String redInvoiceNo;
	private String dwpActualPaymentInclVatRmb;
	private String dwpPaymentDate;
	private String retailPriceChanged;
	private String tradeTypeChanged;
	private String customerChanged;
	private String owerTypeChanged;
	private String importDate;
	private String lastActionDate;
	private String yaYear;
	private String gantanhao;
	private String jinghao;
	private String national;
	private String vc_customerCategory;
	private String vc_CompanyPrivateVehicleType;

	
	//其他
	private String statusIcon;
	private String dealerText;
	private String variantText;
	private String vin7;

	private String specialCaseNo;
	private String update_user;
	private String region;
	private String t78;
	private String hq;
	

}
