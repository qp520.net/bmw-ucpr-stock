package com.bmw.ssa.entity;

import lombok.Data;

import java.util.List;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
@Data
public class DropList {
    /**
     * searchKey
     */
    private String ListType;
    private String key1;
    private String key2;
    private String key3;
    private String languageFlag="8";
    private List<DropList> CourseList;
    /**
     * searchResult
     */
    private String id;
    private String value;

    public String getValue() {
        if(value==null){
            value="";
        }
        return value;
    }
}
