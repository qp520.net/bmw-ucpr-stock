package com.bmw.ssa.entity;

import lombok.Data;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
@Data
public class Insurance {
    private Integer id;
    private Integer dealerid;
    private Integer ins_no;
    private Integer status;
    private Integer created_date;
    private Integer applicant_name;
    private Integer applicant_mobile;
    private Integer name_cn;
    private Integer company_cn;
    private Integer id_no;
    private Integer next_approver_role;
    private Integer next_approver_seq;
    private Integer audit_code;
    private Integer audit_id;
    private Integer audit_result;
    private Integer audit_reset_code;
    private Integer audit_reject_code;
    private Integer audit_text;
    private Integer application_date;
    private Integer last_action_date;
    private Integer ins_id;
    private Integer created_by;
    private Integer created_dealer;
    private Integer last_action_user;
    private Integer reset_time;
    private Integer activation_date;
    private Integer type;
    private Integer third_party;
    private Integer rcompany_cn;
}
