package com.bmw.ssa.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by Administrator on 2019/8/21 0021.
 */
@Data
public class InsuranceDetailData {
    @ApiModelProperty(value = "用户id")
    private String dealerId;
    @ApiModelProperty(value = "申请编号")
    private String insNo;
    @ApiModelProperty(value = "身份证号")
    private String idNo;
    @ApiModelProperty(value = "状态")
    private String status;
    @ApiModelProperty(value = "创建时间")
    private String createdDate;
    @ApiModelProperty(value = "生效日期")
    private String activationDate;
    @ApiModelProperty(value = "申请人姓名")
    private String applicantName;
    @ApiModelProperty(value = "申请人手机号")
    private String applicantMobile;
    @ApiModelProperty(value = "申请人邮箱")
    private String applicantMail;
    @ApiModelProperty(value = "")
    private String remarkName;
    @ApiModelProperty(value = "")
    private String remark;
    @ApiModelProperty(value = "公司全称")
    private String companyCN;
    @ApiModelProperty(value = "员工姓名")
    private String nameCN;
    @ApiModelProperty(value = "申请id")
    private String insId;
    @ApiModelProperty(value = "")

    private String nextApproverRole;
    @ApiModelProperty(value = "")
    private String nextApproverSeq;

    private String auditCode;

    private String auditId;

    private String auditResult;
    @ApiModelProperty(value = "重置")
    private String auditResetCode;
    @ApiModelProperty(value = "退回")
    private String auditRejectCode;

    private String auditText;

    private String applicationDate;
    @ApiModelProperty(value = "修改日期")
    private String last_actionDate;
    @ApiModelProperty(value = "申请日期")
    private String application_Date;

    private String action_Date;

    private String createdBy;

    private String approComment;

    private String createdDealer;

    private String lastActionUser;

    private String resetTime;

    private String approvedDate;

    private String userName;

    private String languageFlag="8";

    private String dealerName;

    private String dealerNameView;

    private String statusName;

    private String region;



    private int operationType;
    /**退回或拒绝code*/
    private String operationCode;
    private int pageNumber =1;
    private int pageSize=10;
}
