package com.bmw.ssa.entity;

import lombok.Data;

import java.util.Date;

/**
 * Created by Administrator on 2019/8/21 0021.
 */
@Data
public class InsuranceFileData {
    private String contractId;


    private String insId;


    private Date lastActionDate;


    private String type;


    private String docFilename;

    private String docMimetype;


    private byte[] insDoc;
}
