package com.bmw.ssa.entity;

import lombok.Data;

/**
 * Created by Administrator on 2019/8/21 0021.
 */
@Data
public class InsuranceHistoryData {
    private String changeId;

    private String userName;

    private String changeDate;

    private String actionId;

    private String resultId;

    private String resultText;

    private String insId;

    private String support;
}
