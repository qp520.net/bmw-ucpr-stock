package com.bmw.ssa.entity;

import com.bmw.ssa.po.KeyAccountAsa.HistoryDataPo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Project: BMW SSA Web Base
 * @System: SSA
 * @SubSystem: Key Account ASA
 * @Description: 机构协议申请详细Data
 * @author: andh
 */
@Data
@ApiModel
public class KeyAccountAsaDetailData implements Serializable {
	/**
	 * @Fields serialVersionUID :
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "经销商编号")
	private String dealerId;
	@ApiModelProperty(value = "")
	private String kasaNo;
	@ApiModelProperty(value = "状态")
	private String status;
	@ApiModelProperty(value = "创建日期")
	private String createdDate;
	@ApiModelProperty(value = "激活日期")
	private String activationDate;
	@ApiModelProperty(value = "申请人姓名")
	private String applicantName;
	@ApiModelProperty(value = "手机号码")
	private String applicantMobile;
	@ApiModelProperty(value = "邮箱")
	private String applicantMail;
	@ApiModelProperty(value = "经销商类别")
	private String customerCategory;
	@ApiModelProperty(value = "备注")
	private String remark;
	@ApiModelProperty(value = "机构全称")
	private String companyCn;
	@ApiModelProperty("机构地址")
	private String companyAddress;
	@ApiModelProperty(value = "省")
	private String provience;
	@ApiModelProperty(value = "市")
	private String city;
	@ApiModelProperty(value = "邮政编码")
	private String postcode;
	@ApiModelProperty(value = "区域")
	private String region;
	@ApiModelProperty(value = "负责人")
	private String charger;
	@ApiModelProperty(value = "职位")
	private String title;
	@ApiModelProperty(value = "负责人联系方式")
	private String chargerContact;
	@ApiModelProperty(value = "")
	private String workId;
	@ApiModelProperty(value = "协议支持%申请")
	private String support;
	@ApiModelProperty(value = "有效年")
	private String validYear;
	@ApiModelProperty(value = "备注")
	private String comments;
	@ApiModelProperty(value = "下一个审批角色")
	private String nextApproverRole;
	@ApiModelProperty(value = "")
	private String nextApproverSeq;
	@ApiModelProperty(value = "")
	private String auditCode;
	@ApiModelProperty(value = "")
	private String auditId;
	@ApiModelProperty(value = "")
	private String auditResult;
	@ApiModelProperty(value = "")
	private String auditResetCode;
	@ApiModelProperty(value = "")
	private String auditRejectCode;
	@ApiModelProperty(value = "")
	private String auditText;
	@ApiModelProperty(value = "")
	private String applicationDate;
	@ApiModelProperty(value = "")
	private String last_actionDate;
	@ApiModelProperty(value = "")
	private String kasaId;
	@ApiModelProperty(value = "")
	private String createdBy;
	@ApiModelProperty(value = "")
	private String approComment;
	@ApiModelProperty(value = "")
	private String createdDealer;
	@ApiModelProperty(value = "")
	private String lastActionUser;
	@ApiModelProperty(value = "")
	private String resetTime;
	@ApiModelProperty(value = "")
	private String nodelete;
	@ApiModelProperty(value = "")
	private String approvedDate;
	@ApiModelProperty(value = "")
	private String userName;
	@ApiModelProperty(value = "")
	private String languageFlag="8";
	@ApiModelProperty(value = "")
	private String dealerName;
	@ApiModelProperty(value = "")
	private String dealerNameView;
	@ApiModelProperty(value = "")
	private String statusName;
	@ApiModelProperty(value = "区域")
	private String regionName;
	@ApiModelProperty(value = "")
	private String renew;
	@ApiModelProperty(value = "")
	private String ogKasaId;
	@ApiModelProperty(value = "")
	private int operationType;
	@ApiModelProperty(value = "历史数据")
	private List<HistoryDataPo> historyDataList;

	
}
