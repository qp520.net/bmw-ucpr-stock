package com.bmw.ssa.entity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @类描述：机构市场活动报告申请 Data
 * @创建人： QXO7127
 * @创建时间：Apr 19, 2017 10:04:42 AM
 * @修改人： QXO7127
 * @修改时间：Apr 19, 2017 10:04:42 AM
 * @修改备注：
 */ 
@Data
public class KeyAuthorityAmaDetailData implements Serializable {
	/**
	 * @Fields serialVersionUID :
	 */
	private static final long serialVersionUID = 1L;

	private String dealerId;

	private String kamaNo;

	private String status;

	private String createdDate;

	private String activationDate;

	private String applicantName;

	private String applicantMobile;

	private String applicantMail;

	private String customerCategoryName;
	
	private String customerCategory;

	private String remarkName;
	
	private String remark;

	private String companyCn;

	private String companyAddress;

	private String provience;

	private String city;

	private String postcode;

	private String region;

	private String charger;

	private String title;

	private String chargerContact;

	private String workId;

	private String support; 
	
	private String supportName;

	private Date validYear;
	
	private String validDate;

	private String comments;

	private String nextApproverRole;

	private String nextApproverSeq;

	private String auditCode;

	private String auditId;

	private String auditResult;

	private String auditResetCode;

	private String auditRejectCode;

	private String auditText;

	private String applicationDate;

	private String last_actionDate;
	
	private String application_Date;
	
	private String action_Date; 

	private String kamaId;

	private String createdBy;

	private String approComment;

	private String createdDealer;

	private String lastActionUser;

	private String resetTime;

	private String nodelete;

	private String approvedDate;

	private String userName;

	private String languageFlag="8";

	private String dealerName;
	
	private String dealerNameView;

	private String statusName;

	private String regionName;
	
	private int operationType;
	/**退回或拒绝code*/
	private String operationCode;


	
}