package com.bmw.ssa.entity;

import lombok.Data;

/**
 * Created by Administrator on 2019/8/21 0021.
 */
@Data
public class LevelData {
    private int currentLevel;
    private int allLevel;
    private String referenceId;
    private int claimLimitMin;
    private int claimLimitMax;
    private String anaCode;
    private String status;
    private String appRole;
}
