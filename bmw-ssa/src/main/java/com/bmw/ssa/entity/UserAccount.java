package com.bmw.ssa.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * Created by Administrator on 2019/8/15 0015.
 */

@Data
@ApiModel
public class UserAccount {
    @ApiModelProperty(hidden = true)
    private Integer id;
    @ApiModelProperty(value = "用户名")
    private String user_name;
    @ApiModelProperty(value = "全名称")
    private String full_name;
    @ApiModelProperty(value = "密码")
    private String password;
    @ApiModelProperty(value = "描述")
    private String description;
    @ApiModelProperty(value = "客户id")
    private Integer customere_id;
    @ApiModelProperty(value = "语言id")
    private Integer language_id;
    @ApiModelProperty(value = "部门")
    private String department;
    @ApiModelProperty(value = "邮箱")
    private String email;
    @ApiModelProperty(value = "手机号")
    private String mobile;
    @ApiModelProperty(value = "是否锁定")
    private Integer locked;
    @ApiModelProperty(value = "变更日期")
    private Date update_time;
    @ApiModelProperty(value = "错误次数")
    private Integer error_count;
}
