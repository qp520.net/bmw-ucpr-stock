package com.bmw.ssa.entity.emailVerify;

import java.io.Serializable;

/**
 * Project：BMW SSA Web Base
 * System：SSA
 * Sub System：Admin
 * 员工邮箱验证-画面元素 活性&非活性Bean
 * @author haozhch
 * 
 */
public class EmailExistVerifyData implements Serializable{

	private static final long serialVersionUID = -8672839988501316686L;
	// QXO7127 Jun 23, 2017 5:34:07 PM  验证时间
	private String verifyDate;
	private String idNo;
	private String mailAddress;
	private String mail;
	private String suffix;
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	/**
	 * ID
	 */
	private String id;
	
	/**
	 * NAME
	 */
	private String name;
	/**
	 * PASSCODE
	 */
	private String passCode;
	
	/**
	 * Email验证结果
	 * 
	 */
	private String result;

	/**
	 * 发送结果
	 * 
	 */
	private String send;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the passCode
	 */
	public String getPassCode() {
		return passCode;
	}

	/**
	 * @param passCode the passCode to set
	 */
	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	public String getSend() {
		return send;
	}

	public void setSend(String send) {
		this.send = send;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVerifyDate() {
		return verifyDate;
	}

	public void setVerifyDate(String verifyDate) {
		this.verifyDate = verifyDate;
	}
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	
}
