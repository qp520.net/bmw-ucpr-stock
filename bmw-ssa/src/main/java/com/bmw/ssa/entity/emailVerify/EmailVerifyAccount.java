package com.bmw.ssa.entity.emailVerify;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Project：BMW SSA Web Base
 * System：SSA
 * Sub System：Admin
 * 员工邮箱验证dataBean
 * @author neusoft
 * 
 */
@Data
@ApiModel
public class EmailVerifyAccount {

	/**
	 * seq
	 */
	@ApiModelProperty(value = "",hidden = true)
	private String seq;
	@ApiModelProperty(value = "身份证号码")
	private String idNo;
	/**
	 * Prim
	 */
	@ApiModelProperty(value = "",hidden = true)
	private String prim;
	
	/**
	 * Num
	 */
	@ApiModelProperty(hidden = true)
	private String num;
	
	/**
	 * Customer Name
	 */
	@ApiModelProperty(value = "客户姓名")
	private String customerName;
	
	/**
	 * Company CN
	 */
	@ApiModelProperty(value = "公司全称")
	private String companyCN;
	
	/**
	 * Mail Address
	 */
	@ApiModelProperty(value = "邮箱")
	private String mailAddress;
	
	/**
	 * Verify Result
	 */
	@ApiModelProperty(value = "验证结果")
	private String verifyResult;
	
	/**
	 * Verify Date
	 */
	@ApiModelProperty(value = "验证日期")
	private String verifyDate;
	
	/**
	 * Fleet No.
	 */
	@ApiModelProperty(value = "大客户编号")
	private String fleetNo;
	
	/**
	 * Create Dealer
	 */
	@ApiModelProperty(value = "创建经销商")
	private String createDealer;
	
	/**
	 * send
	 */
	private String send;
	
	/**
	 * result
	 */
	private String result;


}
