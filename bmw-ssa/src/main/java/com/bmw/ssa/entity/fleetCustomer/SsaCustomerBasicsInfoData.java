package com.bmw.ssa.entity.fleetCustomer;


import java.util.Date;

/**
 * @类描述：客户基础信息表返回实体
 * @创建人： QXO7127
 * @创建时间：Jul 27, 2017 3:23:18 PM
 * @修改人： QXO7127
 * @修改时间：Jul 27, 2017 3:23:18 PM
 * @修改备注：
 */
public class SsaCustomerBasicsInfoData {
	private String rowKey;
	// QXO7127 Jul 27, 2017 3:28:38 PM  nsc编号
    private String cbuDea;
    // QXO7127 Jul 27, 2017 3:28:24 PM  bba编号
    private String bbaDea;

    private String dealerName;
    // QXO7127 Jul 27, 2017 3:28:16 PM  职位
    private String userPosition;

    private String userName;
    // QXO7127 Jul 27, 2017 3:28:01 PM  固话
    private String userMobile;
    // QXO7127 Jul 27, 2017 3:28:08 PM  手机
    private String userPhone;

    private String userMail;
    // QXO7127 Jul 27, 2017 3:27:07 PM  加入宝马大客户时间(String)
    private String accessionTime;
 // QXO7127 Jul 27, 2017 3:35:43 PM  加入时间 (Date)
    private Date accessionDateTime;

    public String getCbuDea() {
        return cbuDea;
    }

    public void setCbuDea(String cbuDea) {
        this.cbuDea = cbuDea;
    }

    public String getBbaDea() {
        return bbaDea;
    }

    public void setBbaDea(String bbaDea) {
        this.bbaDea = bbaDea;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(String userPosition) {
        this.userPosition = userPosition;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public String getAccessionTime() {
        return accessionTime;
    }

    public void setAccessionTime(String accessionTime) {
        this.accessionTime = accessionTime;
    }

	public Date getAccessionDateTime() {
		return accessionDateTime;
	}

	public void setAccessionDateTime(Date accessionDateTime) {
		this.accessionDateTime = accessionDateTime;
	}

	public String getRowKey() {
		return rowKey;
	}

	public void setRowKey(String rowKey) {
		this.rowKey = rowKey;
	}
    
}