package com.bmw.ssa.entity.keyComplaint;

import com.bmw.ssa.po.KeyComplaint.SelectItem;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2019/9/4 0004.
 */
@Data
public class KeyComplaintManagementSearchFromData {

    /** Corporation */
    private String corporation;

    /** Priority */
    private String priority;

    /** Priority Item */
    private List<SelectItem> priorityItem;

    /** Complaint Source */
    private String complaintSource;

    /** Complaint Source Item */
    private List<SelectItem> complaintSourceItem;

    /** Complaint Source */
    private String complaintType;

    /** Complaint Source Item */
    private List<SelectItem> complaintTypeItem;

    /** Status */
    private String status;

    /** Status Item */
    private List<SelectItem> statusItem;

    /** Start Date From */
    private Date startDateFrom;

    /** Start Date To */
    private Date startDateTo;

    /** Due Date From */
    private Date dueDateFrom;

    /** Due Date To */
    private Date dueDateTo;

    /** Actual Date From */
    private Date actualDateFrom;

    /** Actual Date To */
    private Date actualDateTo;

    /** Create Date From */
    private Date createDateFrom;

    /** Create Date To */
    private Date createDateTo;

    /** Last Action Date From */
    private Date lastActionDateFrom;

    /** Last Action Date To */
    private Date lastActionDateTo;
}
