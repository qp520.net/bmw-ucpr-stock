package com.bmw.ssa.entity.nationalAccount;

import java.io.Serializable;

/**
 * Project：BMW SSA Web Base
 * System：SSA
 * Sub System：National Account
 * 大客户账户检索结果实体bean
 * @author  Zhanghy
 */

public class NationalAccountsSearchResultEntity implements Serializable{

	private static final long serialVersionUID = -2231946936806654900L;
	private String custid;
	
    private String status;
    private String dealerId;
    
    private String dmsCustomerNo;
    private String companyCN;
    private String newRenew;
    private String fleetAccountNo;
	private String customerCategory;
	private String owner;
    private String subsidyCc;
    private String subsidyEp;
	private String validFrom;
	private String validTo;
	private String lastActionDate;
    private String subsidyCc1;
    private String subsidyCc2;
    private String subsidyCc3;
    private String subsidyEp1;
    private String subsidyEp2;
    private String subsidyEp3;
    private String newFlag;
    
    
    
    public String getNewFlag() {
		return newFlag;
	}

	public void setNewFlag(String newFlag) {
		this.newFlag = newFlag;
	}

	public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDmsCustomerNo() {
        return dmsCustomerNo;
    }

    public void setDmsCustomerNo(String dmsCustomerNo) {
        this.dmsCustomerNo = dmsCustomerNo;
    }
    public String getCompanyCN() {
        return companyCN;
    }

    public void setCompanyCN(String companyCN) {
        this.companyCN = companyCN;
    }

    public String getNewRenew() {
        return newRenew;
    }

    public void setNewRenew(String newRenew) {
        this.newRenew = newRenew;
    }

	public String getFleetAccountNo() {
		return fleetAccountNo;
	}

	public void setFleetAccountNo(String fleetAccountNo) {
		this.fleetAccountNo = fleetAccountNo;
	}

	public String getCustomerCategory() {
		return customerCategory;
	}

	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

    public String getSubsidyCc() {
        return subsidyCc;
    }

    public void setSubsidyCc(String subsidyCc) {
        this.subsidyCc = subsidyCc;
    }

    public String getSubsidyEp() {
        return subsidyEp;
    }

    public void setSubsidyEp(String subsidyEp) {
        this.subsidyEp = subsidyEp;
    }

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public String getLastActionDate() {
		return lastActionDate;
	}

	public void setLastActionDate(String lastActionDate) {
		this.lastActionDate = lastActionDate;
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getSubsidyCc1() {
		return subsidyCc1;
	}

	public void setSubsidyCc1(String subsidyCc1) {
		this.subsidyCc1 = subsidyCc1;
	}

	public String getSubsidyCc2() {
		return subsidyCc2;
	}

	public void setSubsidyCc2(String subsidyCc2) {
		this.subsidyCc2 = subsidyCc2;
	}

	public String getSubsidyCc3() {
		return subsidyCc3;
	}

	public void setSubsidyCc3(String subsidyCc3) {
		this.subsidyCc3 = subsidyCc3;
	}

	public String getSubsidyEp1() {
		return subsidyEp1;
	}

	public void setSubsidyEp1(String subsidyEp1) {
		this.subsidyEp1 = subsidyEp1;
	}

	public String getSubsidyEp2() {
		return subsidyEp2;
	}

	public void setSubsidyEp2(String subsidyEp2) {
		this.subsidyEp2 = subsidyEp2;
	}

	public String getSubsidyEp3() {
		return subsidyEp3;
	}

	public void setSubsidyEp3(String subsidyEp3) {
		this.subsidyEp3 = subsidyEp3;
	}
}
