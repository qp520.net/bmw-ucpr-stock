package com.bmw.ssa.entity.speciaCase;

import java.io.Serializable;

public class SpecialCaseData implements  Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4368800098541707110L;

	private String scNo;
	
	private String scName;
	
	private String asaNo;
	
	private String vinNo;
	
	private String scStatus;
	
	private String scType;
	
	private String createDate;
	
	private String lastDate;
	
	private String scAttachment1;
	
	private String Dealer1;
	
	private String Approver1;
	
	private String scAttachment2;
	
	private String Dealer2;
	
	private String Approver2;
	
	private String scAttachment3;
	
	private String Dealer3;
	
	private String Approver3;
	
	private String scAttachment4;
	
	private String Dealer4;
	
	private String Approver4;
	
	private String scAttachment5;
	
	private String Dealer5;
	
	private String Approver5;
	
	private String scComment1;
	
	private String scComment2;
	
	private String scComment3;
	
	private String asaAndVin;
private String update_user;
	public String getScNo() {
		return scNo;
	}

	public void setScNo(String scNo) {
		this.scNo = scNo;
	}

	public String getScName() {
		return scName;
	}

	public void setScName(String scName) {
		this.scName = scName;
	}

	public String getAsaNo() {
		return asaNo;
	}

	public void setAsaNo(String asaNo) {
		this.asaNo = asaNo;
	}

	public String getVinNo() {
		return vinNo;
	}

	public void setVinNo(String vinNo) {
		this.vinNo = vinNo;
	}

	public String getScStatus() {
		return scStatus;
	}

	public void setScStatus(String scStatus) {
		this.scStatus = scStatus;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getScComment1() {
		return scComment1;
	}

	public void setScComment1(String scComment1) {
		this.scComment1 = scComment1;
	}

	public String getScComment2() {
		return scComment2;
	}

	public void setScComment2(String scComment2) {
		this.scComment2 = scComment2;
	}

	public String getScComment3() {
		return scComment3;
	}

	public void setScComment3(String scComment3) {
		this.scComment3 = scComment3;
	}

	public String getAsaAndVin() {
		return asaAndVin;
	}

	public void setAsaAndVin(String asaAndVin) {
		this.asaAndVin = asaAndVin;
	}

	public String getScType() {
		return scType;
	}

	public void setScType(String scType) {
		this.scType = scType;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public String getScAttachment1() {
		return scAttachment1;
	}

	public void setScAttachment1(String scAttachment1) {
		this.scAttachment1 = scAttachment1;
	}

	public String getDealer1() {
		return Dealer1;
	}

	public void setDealer1(String dealer1) {
		Dealer1 = dealer1;
	}

	public String getApprover1() {
		return Approver1;
	}

	public void setApprover1(String approver1) {
		Approver1 = approver1;
	}

	public String getScAttachment2() {
		return scAttachment2;
	}

	public void setScAttachment2(String scAttachment2) {
		this.scAttachment2 = scAttachment2;
	}

	public String getDealer2() {
		return Dealer2;
	}

	public void setDealer2(String dealer2) {
		Dealer2 = dealer2;
	}

	public String getApprover2() {
		return Approver2;
	}

	public void setApprover2(String approver2) {
		Approver2 = approver2;
	}

	public String getScAttachment3() {
		return scAttachment3;
	}

	public void setScAttachment3(String scAttachment3) {
		this.scAttachment3 = scAttachment3;
	}

	public String getDealer3() {
		return Dealer3;
	}

	public void setDealer3(String dealer3) {
		Dealer3 = dealer3;
	}

	public String getApprover3() {
		return Approver3;
	}

	public void setApprover3(String approver3) {
		Approver3 = approver3;
	}

	public String getScAttachment4() {
		return scAttachment4;
	}

	public void setScAttachment4(String scAttachment4) {
		this.scAttachment4 = scAttachment4;
	}

	public String getDealer4() {
		return Dealer4;
	}

	public void setDealer4(String dealer4) {
		Dealer4 = dealer4;
	}

	public String getApprover4() {
		return Approver4;
	}

	public void setApprover4(String approver4) {
		Approver4 = approver4;
	}

	public String getScAttachment5() {
		return scAttachment5;
	}

	public void setScAttachment5(String scAttachment5) {
		this.scAttachment5 = scAttachment5;
	}

	public String getDealer5() {
		return Dealer5;
	}

	public void setDealer5(String dealer5) {
		Dealer5 = dealer5;
	}

	public String getApprover5() {
		return Approver5;
	}

	public void setApprover5(String approver5) {
		Approver5 = approver5;
	}

	public String getUpdate_user() {
		return update_user;
	}

	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	
}
