package com.bmw.ssa.entity.speciaDiscount;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class SpecialDiscountApplicationData implements Serializable {

	private static final long serialVersionUID = 4712408252436998897L;

	private String status;
	private String exclamationMark;
	
	private String asterisk;
	 
	private String sdaNo;
	 
	private String dmsCustomerNo;

	private String customerCategory;
	private String initiator;
	private String nameCN;

	private String nameEN;

	private String companyCN;

	private String companyEN;

	private String applicantion;

	private String sdaCompanyDepartment;

	private String sdaApplicantTitle;

	private String sdaMobileApplicant;

	private String title;

	private String phone;

	private String vehicleOwnedBy;

	private String sdaContactReference;

	private String sdaPhoneContactRef;

	private String model;

	private String variant;

	private String description;

	private String extCol;

	private String intCol;

	private String expDelivery;

	private String suggestedDealer;

	private String claimDealerID;

	private String dealerName;

	private String stickerPrice;

	private String promotion;

	private String discC;

	private String discB;

	private String discD;

	private String vin;

	private String retailDate;

	private String remark;

	private String applicationDate;

	private String createDate;

	private String lastActionDate;
	
	// 2016/04/07 update by wbo_neu
	private String accountAppliedFrom ; 
    private String accountAppliedTo;
    private String lastActionDateFrom;
    private String lastActionDateTo;
    // 2016/04/07 update by wbo_neu
	private int firstRecord;
	private int lastRecord;
	private String sort;
	private String sortADSC;
	private String title1;
	private String title2;
	private String title3;
	private String rowKey;
 
    private String pageFlag;
    
    //add by xuding 05/24/2017  SDA?????dealer????????
    private List<String> claimDealerIDList;
    //add by xuding 05/25/2017  SDA?????????????????
    private List<String> customerCategoryIDList;
    // QXO7127 Jun 9, 2017 2:34:59 PM  ????list
    private List<String> regionList;

}
