package com.bmw.ssa.entity.speciaDiscount;

import lombok.Data;

@Data
public class SsaSdaHistory {
	private	String	CHANGE_ID	;
	private	String	USER_NAME	;
	private	String	CHANGE_DATE	;
	private	String	ACTION_ID	;
	private	String	RESULT_ID	;
	private	String	RESULT_TEXT	;
	private	String	SDA_ID	;


}
