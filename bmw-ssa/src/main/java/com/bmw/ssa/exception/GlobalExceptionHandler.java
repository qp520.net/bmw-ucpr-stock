package com.bmw.ssa.exception;

import com.bmw.core.web.exception.BmwException;
import com.bmw.core.web.util.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by Administrator on 2019/9/5 0005.
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(value = BmwException.class)
    public ResponseEntity bmwException(BmwException e){
        log.debug("发生业务异常,原因是：{}",e.getMessage());
        return ResponseUtils.failure(e.getCode(),e.getMessage());
    }
}
