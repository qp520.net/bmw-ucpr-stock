package com.bmw.ssa.po;

import com.bmw.core.database.po.BasePO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Administrator on 2019/8/16 0016.
 */
@Data
@ApiModel
public class CheckforFleetAccountPo{
        @ApiModelProperty(value = "公司中文名")
        private String companyCN;

        @ApiModelProperty(value = "不知道干啥的")
        private String result;

        @ApiModelProperty(value = "干啥的状态好像是")
        private String status;

        @ApiModelProperty(value = "经销商Id")
        private String dealerID;
        @ApiModelProperty(value = "账户Id")
        private String accountID;

        @ApiModelProperty(value ="客户类型" )
        private String customerCategory;

        @ApiModelProperty(value = "经销商名字")
        private String dealerName;

        private int parameter_L = 1;
        @ApiModelProperty(value = "车架号")
        private String vin;
        @ApiModelProperty(value = "重置时间")
        private int resetTimes;
}
