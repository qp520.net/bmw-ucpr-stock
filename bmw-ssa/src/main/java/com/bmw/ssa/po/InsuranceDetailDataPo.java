package com.bmw.ssa.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
@Data
@ApiModel
public class InsuranceDetailDataPo {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "经销商id")
    private String dealerId;
    @ApiModelProperty(value = "公积金申请编号")
    private String insNo;
    @ApiModelProperty(value = "身份证号码")
    private String idNo;
    @ApiModelProperty(value = "申请状态码")
    private String status;
    @ApiModelProperty(value = "创建时间")
    private String createdDate;
    @ApiModelProperty(value = "激活时间")
    private String activationDate;
    @ApiModelProperty(value = "申请人姓名")
    private String applicantName;
    @ApiModelProperty(value = "申请人手机号")
    private String applicantMobile;
    @ApiModelProperty(value = "申请人邮箱")
    private String applicantMail;
    @ApiModelProperty(value = "")
    private String remarkName;
    @ApiModelProperty(value = "")
    private String remark;
    @ApiModelProperty(value = "公司名")
    private String companyCN;
    @ApiModelProperty(value = "员工姓名")
    private String nameCN;
    @ApiModelProperty(value = "公积金申请id")
    private String insId;

    @ApiModelProperty(value = "下一个审批岗位")
    private String nextApproverRole;
    @ApiModelProperty(value = "下一个序号")
    private String nextApproverSeq;
    @ApiModelProperty(value = "审批码")
    private String auditCode;
    @ApiModelProperty(value = "审批id")
    private String auditId;
    @ApiModelProperty(value = "审批结果")
    private String auditResult;
    @ApiModelProperty(value = "重置码")
    private String auditResetCode;
    @ApiModelProperty(value = "退回码")
    private String auditRejectCode;
    @ApiModelProperty(value = "审批内容")
    private String auditText;
    @ApiModelProperty(value = "申请时间")
    private String applicationDate;
    @ApiModelProperty(value = "修改时间")
    private String last_actionDate;

    private String application_Date;

    private String action_Date;

    private String createdBy;

    private String approComment;
    @ApiModelProperty
    private String createdDealer;

    private String lastActionUser;
    @ApiModelProperty(value = "重置时间")
    private String resetTime;

    private String approvedDate;

    private String userName;
    @ApiModelProperty(value = "国际化标识")
    private String languageFlag="8";
    @ApiModelProperty(value = "经销商名称")
    private String dealerName;

    private String dealerNameView;
    @ApiModelProperty(value = "状态")
    private String statusName;
    @ApiModelProperty(value = "区域")
    private String region;


}
