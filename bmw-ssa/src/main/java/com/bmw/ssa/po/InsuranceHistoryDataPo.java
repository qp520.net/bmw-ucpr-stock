package com.bmw.ssa.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by Administrator on 2019/8/21 0021.
 */
@Data
@ApiModel
public class InsuranceHistoryDataPo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String changeId;
    @ApiModelProperty(value = "用户")
    private String userName;

    private String changeDate;
    @ApiModelProperty(value = "操作")
    private String actionId;
    @ApiModelProperty(value = "结果")
    private String resultId;
    @ApiModelProperty(value = "备注")
    private String resultText;
    @ApiModelProperty(value = "申请id")
    private String insId;

    private String support;
}
