package com.bmw.ssa.po;

import com.bmw.ssa.entity.DropList;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
@Data
public class InsuranceSearchDataPo implements Serializable {
    private String ListType;

    private String region;

    private String dealer;

    private List<DropList> CourseList;

    /** 机构-客户类别List */
    private List<DropList> customerCategoryList = null;
    /** 大客户-客户类别List */
    private List<DropList> fleetCustomerCategoryList = null;

    /** 状态List */
    private List<DropList> statusList = null;

    /** 经销商List */
    private List<DropList> dealerList = null;

    /** 区域List */
    private List<DropList> regionList = null;

    /** 礼品数量list */
    private List<DropList> giftNumList = null;
    /** 客户类别List */
    private List<DropList> customerCategoryDataeList = null;
    /** 类别List */
    private List<DropList> remarkList57 = null;
    /** 类别List */
    private List<DropList> remarkList94 = null;
    /** 类别List */
    private List<DropList> remarkList96 = null;
    /** 类别List */
    private List<DropList> remarkList60 = null;
    /** 类别List */
    private List<DropList> remarkList = null;
    /** 退回List */
    private List<DropList> ResetList= null;
    /** 拒绝List */
    private List<DropList> RejectList= null;

    private List<DropList> IResetList=null;
    private List<DropList> IRejectList=null;

}
