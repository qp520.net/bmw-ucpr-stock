package com.bmw.ssa.po.KeyAccountAsa;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by Administrator on 2019/8/30 0030.
 */
@Data
@ApiModel
public class CreateKeyAccountAsaPo {
    @ApiModelProperty(value = "协议编号")
    private String agreementNo;
    @ApiModelProperty(value = "状态")
    private Integer status;
    @ApiModelProperty(value = "申请日期")
    private String applicationDate;
    @ApiModelProperty(value = "生效日期")
    private String activationDate;
    @ApiModelProperty(value = "经销商ID")
    private Integer dealerId;
    @ApiModelProperty(value = "经销商名称")
    private String dealerComplay;
    @ApiModelProperty(value = "申请人姓名")
    private String applicantName;
    @ApiModelProperty(value = "申请人手机")
    private String applicantMobile;
    @ApiModelProperty(value = "申请人邮箱")
    private String applicantMail;
    @ApiModelProperty(value = "客户类别")
    private List<CustomerCategoryPo> customerCategoryList;
    @ApiModelProperty(value = "客户类别")
    private String customerCategory;
    @ApiModelProperty(value = "类别")
    private String category;
    @ApiModelProperty(value = "机构全程")
    private String companyCn;
    @ApiModelProperty(value = "机构地址")
    private String companyAddress;
    @ApiModelProperty(value = "省")
    private String provience;
    @ApiModelProperty(value = "市")
    private String city;
    @ApiModelProperty(value = "邮编")
    private String postCode;
    @ApiModelProperty(value = "地区ID")
    private Integer regionId;
    @ApiModelProperty(value = "地区名称")
    private String regionName;
    @ApiModelProperty(value = "负责人")
    private String charger;
    @ApiModelProperty(value = "职位")
    private String title;
    @ApiModelProperty(value = "联系方式")
    private String chargerContact;
    @ApiModelProperty(value = "协议有效期（年）")
    private String validYear;
    @ApiModelProperty(value = "协议支持%申请")
    private String bmwSupport;
    @ApiModelProperty(value = "历史签约id")
    private String ogKasaId;
    @ApiModelProperty(value = "续约")
    private String renew;
    @ApiModelProperty(value = "当前用户")
    private String userName;
    @ApiModelProperty(value = "评论")
    private String remark;
    @ApiModelProperty(value = "创建备注")
    private String comments;
    @ApiModelProperty(value = "提交备注")
    private String auditText;
    @ApiModelProperty(value = "申请纪录id")
    private Integer kasaId;
    @ApiModelProperty(value = "1:申请，0：保存")
    private int operationType;
}
