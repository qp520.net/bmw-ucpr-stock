package com.bmw.ssa.po.KeyAccountAsa;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by Administrator on 2019/8/30 0030.
 */
@Data
@ApiModel
public class CustomerCategoryPo {
    @ApiModelProperty(value = "id")
    private Integer id;
    @ApiModelProperty(value = "客户类别")
    private String customerCategoryName;
    @ApiModelProperty(value = "类别")
    private List<CategotyPo> categotyList;
}
