package com.bmw.ssa.po.KeyAccountAsa;

import lombok.Data;

@Data
public class HistoryDataPo {

	private String userName;

	private String changeId;

	private String changeDate;

	private String actionId;

	private String resultId;

	private String resultText;


}
