package com.bmw.ssa.po.KeyAccountAsa;



import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Project: BMW SSA Web Base
 * @System: SSA
 * @SubSystem: Key Account ASA
 * @Description: 机构协议申请检索结果Data
 * @author: andh
 */
@Data
@ApiModel(value = "机构协议列表")
public class KeyAccountAsaSearchResultData implements Serializable {
	/**
	 * @Fields serialVersionUID :
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "状态")
	private String status;
	@ApiModelProperty(value = "经销商编号")
	private String dealerId;
	@ApiModelProperty(value = "经销商")
	private String dealerName;
	@ApiModelProperty(value = "协议编号")
	private String agreementNo;
	@ApiModelProperty(value = "客户类别")
	private String customerCategory;
	@ApiModelProperty(value = "类别")
	private String category;
	@ApiModelProperty(value = "机构全称")
	private String institutionCns;
	@ApiModelProperty(value = "机构地址")
	private String institutionAddress;
	@ApiModelProperty(value = "省")
	private String provience;
	@ApiModelProperty(value = "市")
	private String city;
	@ApiModelProperty(value = "邮政编码")
	private String postCode;
	@ApiModelProperty(value = "区域")
	private String region;
	@ApiModelProperty(value = "负责人")
	private String contactPerson;
	@ApiModelProperty(value = "职位")
	private String title;
	@ApiModelProperty(value = "联系方式")
	private String phoneNo;
	@ApiModelProperty(value = "协议支持申请%")
	private String bmwSupport;
	@ApiModelProperty(value = "协议有效年")
	private String validYear;
	@ApiModelProperty(value = "申请日期")
	private String applicationDate;
	@ApiModelProperty(value = "生效日期")
	private String activationDate;
	@ApiModelProperty(value = "创建日期")
	private String createDate;
	@ApiModelProperty(value = "最后操作时间")
	private String lastActionDate;
	@ApiModelProperty(value = "")
	private String kasaId;
	@ApiModelProperty(value = "")
	private String renew;
	@ApiModelProperty(value = "")
	private String renewMsg;


}
