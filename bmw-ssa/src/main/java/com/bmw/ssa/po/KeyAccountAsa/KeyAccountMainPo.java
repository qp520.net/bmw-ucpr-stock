package com.bmw.ssa.po.KeyAccountAsa;

import com.bmw.ssa.entity.DropList;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by Administrator on 2019/8/30 0030.
 */
@Data
@ApiModel
public class KeyAccountMainPo {
    @ApiModelProperty(value = "客户类别下拉列表")
    private List<CustomerCategoryPo> customerCategoryList;
    @ApiModelProperty(value = "状态下拉列表")
    private List<DropList> statusList;
    @ApiModelProperty(value = "经销商下拉列表")
    private List<DropList> dealerList;
    @ApiModelProperty(value = "区域下拉列表")
    private List<RegionPo> regionList;
    @ApiModelProperty(value = "年份下拉列表")
    private List<DropList> yearList;
}
