package com.bmw.ssa.po.KeyAccountAsa;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by Administrator on 2019/8/30 0030.
 */
@Data
@ApiModel
public class RegionPo {
    private Integer id;
    @ApiModelProperty(value = "区域名称")
    private String regionName;
}
