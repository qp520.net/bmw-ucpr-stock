package com.bmw.ssa.po.KeyComplaint;

import lombok.Data;

import java.io.Serializable;

/**
 * Project：BMW SSA Web Base
 * System：SSA
 * Sub System：keyAccount
 * 客户投诉/咨询  Data
 * @author liangqs
 * 
 */
@Data
public class KeyComplaintManagementInfoData  {
	

	/** ID */
	private String id;

	/** Status */
	private String status;
	
	/** Complaint Topic */
	private String complaintTopic;
	
	/** VIN */
	private String vin;
	
	/** Customer Owner */
	private String customerOwner;
	
	/** Fleet Account No. */
	private String fleetAccountNo;
	
	/** Corporation */
	private String corporation;
	
	/** Contact */
	private String contact;
	
	/** Priority */
	private String priority;
	
	/** Complaint Type */
	private String ComplaintType;
	
	/** Complaint Source */
	private String complaintSource;
	
	/** Complaint Dealer */
	private String complaintDealer;
	
	/** Start Date */
	private String startDate;
	
	/** Due Date */
	private String dueDate;
	
	/** Actual Date1 */
	private String actualDate1;
	
	/** Follow up Dealer */
	private String followUpDealer;
	
	/** Actor */
	private String actor;
	
	/** Description */
	private String description;
	
	/** Solution */
	private String solution;
	
	/** Creator */
	private String creator;
	
	/** Create Date */
	private String createDate;
	
	/** Last Action Date */
	private String lastActionDate;


}
