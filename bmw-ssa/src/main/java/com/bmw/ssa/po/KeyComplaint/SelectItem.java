package com.bmw.ssa.po.KeyComplaint;

import lombok.Data;

/**
 * Created by Administrator on 2019/9/4 0004.
 */
@Data
public class SelectItem {

    private String key="";
    private String value="";

    private String numKey;
    private String charKey;

    private String value_CN;
    private String value_EN;

    private String active;

    private String reasonID;

    private String reason;
}
