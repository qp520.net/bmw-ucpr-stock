package com.bmw.ssa.po.fleetCustomer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * Created by Administrator on 2019/9/24 0024.
 */
@Data
@ApiModel
public class CustomerSearchPo {
    // QXO7127 Jul 27, 2017 3:28:38 PM  nsc编号
    @ApiModelProperty(value = "nsc编号")
    private String cbuDea;
    // QXO7127 Jul 27, 2017 3:28:24 PM  bba编号
    @ApiModelProperty(value = "bba编号")
    private String bbaDea;
    @ApiModelProperty(value = "经销商名称")
    private String dealerName;
    // QXO7127 Jul 27, 2017 3:28:16 PM  职位
    @ApiModelProperty(value = "职位")
    private String userPosition;
    @ApiModelProperty(value = "姓名")
    private String userName;
    // QXO7127 Jul 27, 2017 3:28:01 PM  固话
    @ApiModelProperty(value = "固话")
    private String userMobile;
    // QXO7127 Jul 27, 2017 3:28:08 PM  手机
    @ApiModelProperty(value = "手机号")
    private String userPhone;

    @ApiModelProperty(value = "用户邮箱")
    private String userMail;
    // QXO7127 Jul 27, 2017 3:27:07 PM  加入宝马大客户时间(String)
    @ApiModelProperty(value = "加入宝马大客户时间")
    private String accessionTime;
    // QXO7127 Jul 27, 2017 3:35:43 PM  加入时间 (Date)
    @ApiModelProperty(value = " 加入时间")
    private Date accessionDateTime;
}
