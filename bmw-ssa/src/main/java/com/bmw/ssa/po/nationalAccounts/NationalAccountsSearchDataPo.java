package com.bmw.ssa.po.nationalAccounts;

import lombok.Data;

import java.util.List;

/**
 * Created by 16074 on 2019/9/29.
 */
@Data
public class NationalAccountsSearchDataPo {
    private String custid;

    private String status;
    private String dealerId;

    private String dmsCustomerNo;
    private String companyCN;
    private String newRenew;
    private String fleetAccountNo;
    private String customerCategory;
    private String owner;
    private String subsidyCc;
    private String subsidyEp;
    private String validFrom;
    private String validTo;
    private String lastActionDate;
    private String subsidyCc1;
    private String subsidyCc2;
    private String subsidyCc3;
    private String subsidyEp1;
    private String subsidyEp2;
    private String subsidyEp3;
    private String newFlag;
    private List<SubBranchDataPo> branchList;
}
