package com.bmw.ssa.po.nationalAccounts;


import java.io.Serializable;

/**
 * Project：BMW SSA Web Base
 * System：SSA
 * Sub System：Admin
 * 分公司一览dataBean
 * @author wangyuchen
 * 
 */
public class SubBranchDataPo implements Serializable{

	private static final long serialVersionUID = -6175601512749095849L;

	private String status;
	private String dmsCustomerNo;
	private String customerCategory;
	private String fleetAccountNo;
	private String company;
	private String salesman;
	private String province;
	private String city;
	private String dealerId;
	private String createdDealerId;
	private String subAsaId;
	
	public String getSubAsaId() {
		return subAsaId;
	}
	public void setSubAsaId(String subAsaId) {
		this.subAsaId = subAsaId;
	}
	public String getDealerId() {
		return dealerId;
	}
	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDmsCustomerNo() {
		return dmsCustomerNo;
	}
	public void setDmsCustomerNo(String dmsCustomerNo) {
		this.dmsCustomerNo = dmsCustomerNo;
	}
	public String getCustomerCategory() {
		return customerCategory;
	}
	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}
	public String getFleetAccountNo() {
		return fleetAccountNo;
	}
	public void setFleetAccountNo(String fleetAccountNo) {
		this.fleetAccountNo = fleetAccountNo;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getSalesman() {
		return salesman;
	}
	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCreatedDealerId() {
		return createdDealerId;
	}
	public void setCreatedDealerId(String createdDealerId) {
		this.createdDealerId = createdDealerId;
	}
	
}
