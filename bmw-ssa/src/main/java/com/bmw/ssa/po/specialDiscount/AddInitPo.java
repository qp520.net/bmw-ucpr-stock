package com.bmw.ssa.po.specialDiscount;

import com.bmw.ssa.vo.specialDiscount.SelectVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by Administrator on 2019/9/11 0011.
 */
@Data
@ApiModel
public class AddInitPo {
    @ApiModelProperty(value = "类别列表")
    private List<SelectItemPo> categoryList;
    @ApiModelProperty(value = "车型列表")
    private List<SelectVo> seriesList;
    @ApiModelProperty(value = "配置列表")
    private List<SelectVo> variantList;
    @ApiModelProperty(value = "经销商列表")
    private List<SelectItemPo> customerList;
}
