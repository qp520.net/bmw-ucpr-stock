package com.bmw.ssa.po.specialDiscount;

import lombok.Data;

import java.util.List;

/**
 * Created by Administrator on 2019/9/9 0009.
 */
@Data
public class SearchInitPo {
    private List<SelectItemPo> customerList;
    private List<SelectItemPo> categoryList;
    private List<SelectItemPo> dealertList;
}
