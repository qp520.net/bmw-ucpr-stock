package com.bmw.ssa.po.specialDiscount;

import lombok.Data;

@Data
public class SelectItemPo {
    private String key1="";
    private String value1="";
    private String numKey;
    private String charKey;

    private String valueCN;
    private String valueEN;

    private String active;

    private String reasonID;

    private String reason;

}
