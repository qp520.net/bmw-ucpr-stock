package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.entity.Account;
import com.bmw.ssa.po.CheckforFleetAccountPo;
import com.bmw.ssa.vo.CheckforFleetAccountVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Administrator on 2019/8/16 0016.
 */
@Mapper
public interface AccountMapper  extends BaseMapper<Account> {

    CheckforFleetAccountPo getAccountForAccount(CheckforFleetAccountVo checkforFleetAccountVo);

    CheckforFleetAccountPo getKeyASA(CheckforFleetAccountVo checkforFleetAccountVo);
}
