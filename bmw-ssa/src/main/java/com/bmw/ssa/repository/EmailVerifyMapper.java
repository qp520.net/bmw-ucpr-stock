package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.entity.emailVerify.EmailExistVerifyData;
import com.bmw.ssa.entity.emailVerify.EmailVerifyAccount;
import com.bmw.ssa.vo.emailVerify.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2019/9/19 0019.
 */
@Mapper
public interface EmailVerifyMapper extends BaseMapper<FleetAccountsParameterVo>{

    List<FleetAccountsInfoVo> getFleetAccount(FleetAccountsParameterVo fleetAccountsParameterVo);

    List<EmailVerifyAccount> getEmployeeEmail(EmployeeParameterVo employeeParameterVo);

    List<String> getEmailDomainsASA(EmailDomainParameterVo emailDomainParameterVo);

    EmailDomainVo getFleetAccountDelerId(EmailDomainParameterVo parameterVo);

    List<String> getFleetAccountParent(EmailDomainParameterVo parameterVo);

    List<String> getEmailDomainsSUB(EmailDomainParameterVo emailDomainParameterVo);

    /**
     * 验证mail是否存在
     * @param idNo
     * @return
     */
    EmailExistVerifyData getIDExistVerify(@Param("idNo") String idNo);
}
