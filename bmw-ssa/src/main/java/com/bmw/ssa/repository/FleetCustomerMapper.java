package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.entity.DropList;
import com.bmw.ssa.po.fleetCustomer.CustomerSearchPo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerBasicsTempVo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerMainSearchVo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerOtherVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2019/9/24 0024.
 */
public interface FleetCustomerMapper extends BaseMapper<SsaCustomerMainSearchVo>{

    List<SsaCustomerBasicsTempVo> getCustomerBasicsInfoDataList(SsaCustomerMainSearchVo ssaCustomerMainSearchVo);

    List<SsaCustomerOtherVo> getCustomerOtherInfoDataList(SsaCustomerOtherVo ssaCustomerOtherVo);

    void batchInsertCustomerBasicsTemp(@Param("list") List<SsaCustomerBasicsTempVo> list);

    void batchInsertCustomerOtherTemp(@Param("list")List<SsaCustomerOtherVo> list);

    void doBasicsUpdate(CustomerSearchPo customerSearchPo);

    void deleteCustomerBasicsByPosition(SsaCustomerBasicsTempVo ssaCustomerBasicsTempVo);

    void deleteCustomerOther(SsaCustomerOtherVo ssaCustomerOtherVo);

    List<DropList> getYearList(@Param("languageFlag") String languageFlag);

}
