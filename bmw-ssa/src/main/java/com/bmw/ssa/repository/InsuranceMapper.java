package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.entity.*;
import com.bmw.ssa.po.InsuranceDetailDataPo;
import com.bmw.ssa.po.InsuranceHistoryDataPo;
import com.bmw.ssa.vo.InsuranceSearchConditionVo;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
@Mapper
public interface InsuranceMapper extends BaseMapper<Insurance>{

    List<InsuranceDetailDataPo> getInsuranceList(InsuranceSearchConditionVo insV);

    InsuranceDetailDataPo getInsuranceDetail(InsuranceSearchConditionVo insVO);

    List<InsuranceHistoryDataPo> getinsuranceHistoryList(DropList dropList);

    int insHistoryInsert(InsuranceHistoryDataPo insp);

    int doUpdate(InsuranceDetailDataPo insp);

    int doInsert(InsuranceDetailDataPo insuranceDetailDataPo);

    void doApply(InsuranceDetailData insuranceDetailData);

    DropList getRegionAndDealerName(DropList dropList);

    int doReset(InsuranceDetailData insuranceDetailData);

    int doReject(InsuranceDetailData insuranceDetailData);

    List<DropList> getRegionListByCharVal(DropList dropList);

    String checkKinsExistWeb(CheckINSExistVo checkINSExistVo);

    String checkInsormail(CheckINSExistVo checkINSExistVo);

    String checkIns(CheckINSExistVo checkINSExistVo);

    List<InsuranceDetailData> getInsuranceAppList(InsuranceSearchConditionVo insV);

}
