package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.entity.DropList;
import com.bmw.ssa.entity.KeyAccountAsaDetailData;
import com.bmw.ssa.po.KeyAccountAsa.*;
import com.bmw.ssa.vo.CheckASAExistVo;
import com.bmw.ssa.vo.KeyAccountAsaDetailVo;
import com.bmw.ssa.vo.KeyAccountAsaSearchConditionVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2019/8/30 0030.
 */
@Mapper
public interface KeyAccountAsaMapper extends BaseMapper<DropList> {
    /**
     * 获取客户类型一级菜单
     *
     * @param languageFlag //国际化标识 1，英文，8汉语
     * @return
     */
    List<CustomerCategoryPo> getCustomerCategoryList(@Param("languageFlag") String languageFlag);

    /**
     * @param languageFlag
     * @param key1
     * @return
     */
    List<CategotyPo> getCategoryList(@Param("languageFlag") String languageFlag, @Param("key1") String key1);

    /**
     * 获得状态下拉列表
     *
     * @param languageFlag
     * @return
     */
    List<DropList> getStatusList(@Param("languageFlag") Integer languageFlag);

    /**
     * 获得经销商列表i
     *
     * @param languageFlag
     * @param key1
     * @return
     */
    List<DropList> getDealerList(@Param("languageFlag") String languageFlag, @Param("key1") String key1);

    /**
     * 获得区域下拉列表
     * @param languageFlag
     * @return
     */
    List<RegionPo> getRegionList(@Param("languageFlag") String languageFlag);

    /**
     * 获得年份下拉列表
     * @param languageFlag
     * @return
     */
    List<DropList> getYearList(@Param("languageFlag") String languageFlag);

    /**
     *新增协议申请
     */

   Integer doInsert(CreateKeyAccountAsaPo createKeyAccountAsaPo);

    /**
     * 新增履历
     * @param createKeyAccountAsaPo
     */
    void  insertHistory(CreateKeyAccountAsaPo createKeyAccountAsaPo);

    /**
     * ASA申请
     * @param createKeyAccountAsaPo
     */
    void doApply(CreateKeyAccountAsaPo createKeyAccountAsaPo);

    /**
     * 取得履历一览
     * @param kadv
     * @return
     */
    List<HistoryDataPo> getHistoryList(KeyAccountAsaDetailVo kadv);

    /**
     * 查询申请列表
     * @param kasv
     * @return
     */
    List<KeyAccountAsaSearchResultData> getKeyAccountAsa(KeyAccountAsaSearchConditionVo kasv);

    /**
     * 取得机构协议申请
     * @param keyAccountAsaDetailVo
     * @return
     */
    KeyAccountAsaDetailData doSelect(KeyAccountAsaDetailVo keyAccountAsaDetailVo);

    /**
     * 编辑机构协议申请
     * @param createKeyAccountAsaPo
     */
    void doUpdate(CreateKeyAccountAsaPo createKeyAccountAsaPo);

    /**
     * 协议有效年,协议状态
     * @param languageFlag
     * @return
     */
//    KeyAccountAsaDetailData getDetailData(String languageFlag);

    /**
     * renewKeyAsa取得历年机构协议申请
     * @param languageFlag
     * @param kasaId
     * @return
     */
    KeyAccountAsaDetailData renewKeyAsaSelect(@Param("languageFlag") String languageFlag,@Param("kasaId") String kasaId);


    String checkKasaExistWeb(CheckASAExistVo checkASAExistVo);

    /**
     * 删除协议
     * @param keyAccountAsaDetailData
     */
    void doDelete(KeyAccountAsaDetailData keyAccountAsaDetailData);

}