package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ssa.po.KeyAccountAsa.KeyAccountAsaSearchResultData;
import com.bmw.ssa.po.KeyComplaint.KeyComplaintManagementInfoData;
import com.bmw.ssa.po.KeyComplaint.SelectItem;
import com.bmw.ssa.vo.KeyAccountAsaSearchConditionVo;
import com.bmw.ssa.vo.keyComplain.KeyComplaintManagementParameterVo;
import com.bmw.ssa.vo.keyComplain.SsaComplaint;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2019/9/4 0004.
 */
@Mapper
public interface KeyComplaintMapper extends BaseMapper<SelectItem> {

    /**
     * 查询优先级
     * @param language
     * @return
     */
    List<SelectItem> getPriorityItem(@Param("language") String language);

    /**
     * 查询投诉来源
     * @param language
     * @return
     */
    List<SelectItem> getComplaintSourceItem(@Param("language")String language);

    /**
     * 投诉类型
     * @param language
     * @return
     */
    List<SelectItem> getComplaintTypeItem(@Param("language")String language);

    /**
     * 状态
     * @param language
     * @return
     */
    List<SelectItem> getStatusItem(@Param("language")String language);

    /**
     *检索客户投诉/咨询一览信息
     * @param kmpv
     * @return
     */

    List<KeyComplaintManagementInfoData> ssaComplaint(@Param("kmpv") KeyComplaintManagementParameterVo kmpv);


    void insertSsaComplaint(SsaComplaint ssaComplaint);

}
