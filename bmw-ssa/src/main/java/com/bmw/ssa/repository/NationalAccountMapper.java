package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.po.nationalAccounts.NationalAccountsSearchDataPo;
import com.bmw.ssa.po.nationalAccounts.SubBranchDataPo;
import com.bmw.ssa.vo.nationalAccount.CreateBranchConditionVo;
import com.bmw.ssa.vo.nationalAccount.NationalAccountSearchConditionVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created by 16074 on 2019/9/29.
 */
@Mapper
public interface NationalAccountMapper extends BaseMapper<NationalAccountSearchConditionVo>{
    /**
     * 查询分页列表
     * @param nationalAccountSearchConditionVo
     * @return
     */

    List<NationalAccountsSearchDataPo> getNationalAccounts(NationalAccountSearchConditionVo nationalAccountSearchConditionVo);

    /**
     * 初始化分公司信息
     * @param createBranchConditionVo
     * @return
     */
    List<SubBranchDataPo> searchAccountBranchList(CreateBranchConditionVo createBranchConditionVo);
}
