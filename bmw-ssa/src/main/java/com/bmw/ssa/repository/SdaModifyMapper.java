package com.bmw.ssa.repository;

import com.bmw.ssa.vo.specialDiscount.SDAModifyVo;

/**
 * Created by Administrator on 2019/9/12 0012.
 */
public interface SdaModifyMapper {

    void insertSDAModify(SDAModifyVo sdaModifyVo);

}
