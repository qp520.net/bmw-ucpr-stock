package com.bmw.ssa.repository;

import com.bmw.ssa.vo.sdaTerminate.SDATerminateVo;

/**
 * Created by Administrator on 2019/9/12 0012.
 */
public interface SdaTerminateMapper {

   void insertSDATerminate(SDATerminateVo vo);
}
