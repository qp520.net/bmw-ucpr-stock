package com.bmw.ssa.repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.entity.DropList;
import com.bmw.ssa.entity.KeyAuthorityAmaSearchData;
import com.bmw.ssa.po.InsuranceSearchDataPo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
public interface SearchDataMapper extends BaseMapper<DropList> {

    /**
     * 获取搜索条件
     * @param key1 1，英文，8，英文
     * @param languageFlag 27077 最大
     * @return
     */
    List<InsuranceSearchDataPo> getAmaDetaDropDownList(@Param("key1") String key1, @Param("languageFlag") String languageFlag);

    /**
     * 获取类别数据
     * @param key1 1，英文，8中文
     * @param languageFlag 27077 最大
     * @return
     */
    List<KeyAuthorityAmaSearchData> getDropDownList(@Param("key1") String key1, @Param("languageFlag") String languageFlag);
}
