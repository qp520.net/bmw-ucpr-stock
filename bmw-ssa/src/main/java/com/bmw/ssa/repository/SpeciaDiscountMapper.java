package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.entity.speciaDiscount.SpecialDiscountApplicationData;
import com.bmw.ssa.entity.speciaDiscount.SsaSdaHistory;
import com.bmw.ssa.po.KeyComplaint.SelectItem;
import com.bmw.ssa.po.specialDiscount.SelectItemPo;
import com.bmw.ssa.vo.specialDiscount.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2019/9/9 0009.
 */
@Mapper
public interface SpeciaDiscountMapper extends BaseMapper<SpecialDiscountApplicationVo>{
    /**
     * 查询特殊协议申请列表
     * @param specialDiscountApplicationVo
     * @return
     */
    List<SpecialDiscountApplicationData> getSpecialDiscountApplicationData(SpecialDiscountApplicationVo specialDiscountApplicationVo);


    List<SelectItemPo> getCustomerData(SearchInitVo searchInitVo);


    List<SelectItemPo> getDealerList(SearchInitVo searchInitVo);

    /**
     * 新增特殊折扣申请
     * @param ssaSda
     */
    Integer  insertSsaSda(SsaSda ssaSda);

    /**
     * 写入操作记录
     * @param ssaSdaHistory
     * @return
     */
    Integer insertSsaSdaHis(SsaSdaHistory ssaSdaHistory);


    Integer insertSDAHistory(SDAHistoryForMT sdaHistoryForMT);

    /**
     * 更新操作
     * @param ssaSda
     */
    void updateSsaSda(SsaSda ssaSda);

    /**
     * 推荐经销商列表
     * @param selectVo
     * @return
     */
    List<SelectVo> getSuggestedList(SelectVo selectVo);

    /**
     * 车型列表
     * @param selectVo
     * @return
     */
    List<SelectVo> getSeriesList();

    List<SelectVo> getSeriesList60();

    List<SelectVo> getSeriesList97();

   List<SelectVo> getSeriesList99();

    /**
     * 根据车型code查询车辆配置列表
     * @param modelCode
     * @return
     */
    List<SelectVo> getVariantList(@Param("modelCode")String modelCode);

    /**
     * 根据category查询车辆所有者的下拉list
     * @param category
     * @return
     */
    List<SelectItemPo> getSearchCategory(@Param("category") String category);

    /**
     * 获取客户类型列表
     * @param language
     * @return
     */
    List<SelectItemPo> getCategoryList(@Param("language") String language);

    /**
     * 写入sda全字段
     * @param ssaSda
     * @return
     */
    Integer insertSelective(SsaSda ssaSda);

    /**
     * 删除sda
     * @param sdaId
     */
    void deleteSsaSda(String sdaId);

    /**
     * 删除历史纪录
     * @param sdaId
     */
    void deleteSsaSdaHistory(String sdaId);

    /**
     * 终止
     * @param procVo
     */
    void termSdaProc(ProcVo procVo);


    /**
     *
     * @param sdkNo
     * @return
     */
    SDAModifyData getSDAByNOForModify(@Param("sdkNo") String sdkNo);


    List<SelectItem> getSDAModifyVariant(String  seriesSec);

    /**
     *
     * @param sdaModifyVo
     */
    void updateSDAMTflag(SDAModifyVo sdaModifyVo);


    DiscountVo getDISC_C_57_70_97(DiscountVo discountVo);

    DiscountVo getDISC_B_57_94_96(DiscountVo discountVo);

    DiscountVo getDISC_B_70(DiscountVo discountVo);

    DiscountVo getDISC_B_59_60_90(DiscountVo discountVo);

    DiscountVo getDISC_C_59_60(DiscountVo discountVo);

    DiscountVo getDISC_D_59(DiscountVo discountVo);

    DiscountVo getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(DiscountVo discountVo);

    DiscountVo getFINAL_PRICE_60(DiscountVo discountVo);

    DiscountVo getDISC_D_61(DiscountVo discountVo);

    String     getDISC_B_61(DiscountVo discountVo);

    String     getDISC_C_61(DiscountVo discountVo);

    String     getDISC_C_90_92_93_95(DiscountVo discountVo);

    String     getDISC_D_90_92_93_95_99(DiscountVo discountVo);

    DiscountVo  getDISC_B_92(DiscountVo discountVo);

    DiscountVo  getDISC_B_93(DiscountVo discountVo);

    DiscountVo  getDISC_B_94(DiscountVo discountVo);

    DiscountVo  getDISC_C_94_96(DiscountVo discountVo);

    DiscountVo  getDISC_B_95(DiscountVo discountVo);

    DiscountVo  getDISC_B_96(DiscountVo discountVo);

    DiscountVo  getDISC_B_99(DiscountVo discountVo);

    DiscountVo  getDISC_C_99(DiscountVo discountVo);
}
