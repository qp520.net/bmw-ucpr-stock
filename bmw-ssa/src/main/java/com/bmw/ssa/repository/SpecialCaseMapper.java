package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.entity.speciaCase.SpecialCaseData;
import com.bmw.ssa.vo.specialCase.ExportInfo;
import com.bmw.ssa.vo.specialCase.SpecialCaseVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2019/9/23 0023.
 */
@Mapper
public interface SpecialCaseMapper extends BaseMapper<SpecialCaseVo>{

    void insertSepc(SpecialCaseVo specialCaseVo);

    void insertSepcVin(@Param("specialCaseVos") List<ExportInfo> specialCaseVos);

    SpecialCaseVo getSpecialCaseNo();

    List<ExportInfo> getSpecialCaseVinByNo(SpecialCaseVo specialCaseVo);

    List<SpecialCaseVo> getSpecialCaseList();

    void  updateSpec(SpecialCaseVo specialCaseVo);

    /**
     * 删除vin
     * @param specialCaseVo
     */
    void deleteScVin(SpecialCaseVo specialCaseVo);

    SpecialCaseVo getSpecialCaseByNo(SpecialCaseVo specialCaseVo);



}
