package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ssa.entity.UserAccount;
import com.bmw.ssa.vo.LevelVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Administrator on 2019/8/15 0015.
 */
@Mapper
public interface UserAccountMapper extends BaseMapper<UserAccount>{

    UserAccount findUserByUserName(String userName);

    /**
     * 市场活动的Level设定
     * @param levelVo
     * @return
     */
    LevelVo selectKeyAccountAmaApprover(LevelVo levelVo);

    LevelVo setKeyAuthorityAmaLevel(LevelVo levelVo);
}
