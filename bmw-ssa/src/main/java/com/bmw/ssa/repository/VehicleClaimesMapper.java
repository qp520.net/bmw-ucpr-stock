package com.bmw.ssa.repository;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ssa.entity.ClaimeData;
import com.bmw.ssa.vo.ClaimsComVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by Administrator on 2019/8/29 0029.
 */
@Mapper
public interface VehicleClaimesMapper {

    Page<ClaimeData> selectClaimeList(ClaimsComVo claimsComVo);
}
