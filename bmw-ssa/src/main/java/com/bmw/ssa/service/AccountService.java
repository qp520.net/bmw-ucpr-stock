package com.bmw.ssa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.entity.Account;
import com.bmw.ssa.po.CheckforFleetAccountPo;
import com.bmw.ssa.vo.CheckforFleetAccountVo;

/**
 * Created by Administrator on 2019/8/16 0016.
 */
public interface AccountService extends IService<Account>{

    CheckforFleetAccountPo checkforFleetAccount(CheckforFleetAccountVo checkforFleetAccountVo);

    CheckforFleetAccountPo getKeyASA(CheckforFleetAccountVo checkforFleetAccountVo);
}
