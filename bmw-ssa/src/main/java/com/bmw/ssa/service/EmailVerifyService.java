package com.bmw.ssa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.entity.emailVerify.EmailExistVerifyData;
import com.bmw.ssa.vo.emailVerify.EmailDomainParameterVo;
import com.bmw.ssa.vo.emailVerify.EmployeeParameterVo;
import com.bmw.ssa.vo.emailVerify.FleetAccountsInfoVo;
import com.bmw.ssa.vo.emailVerify.FleetAccountsParameterVo;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2019/9/19 0019.
 */
public interface EmailVerifyService extends IService<FleetAccountsParameterVo>{

    /**
     * 获取用过户数据列表(大客户列表)
     * @param fleetAccountsParameterVo
     * @return
     */
    PageInfo getFleetAccount(FleetAccountsParameterVo fleetAccountsParameterVo);

    /**
     * 获取员工邮箱列表
     * @param employeeParameterVo
     * @return
     */
    PageInfo getEmployeeEmail(EmployeeParameterVo employeeParameterVo);

    /**
     * 获取邮箱后缀
     * @param emailDomainParameterVo
     * @return
     */
    List<String> getEmailDomainsASA(EmailDomainParameterVo emailDomainParameterVo) throws Exception;

}
