package com.bmw.ssa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.po.fleetCustomer.CustomerSearchPo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerBasicsTempVo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerMainSearchVo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerOtherVo;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Administrator on 2019/9/24 0024.
 */
public interface FleetCustomerService extends IService<SsaCustomerMainSearchVo> {

    /**
     * 查询分页列表
     * @param ssaCustomerMainSearchVo
     * @return
     */
    PageInfo getCustomerPageList(SsaCustomerMainSearchVo ssaCustomerMainSearchVo);


    /**
     * 查询其他列表
     * @param customerOtherVo
     * @return
     */
    PageInfo getCustomerOtherPageList(SsaCustomerOtherVo customerOtherVo);

    /**
     * 导入excel列表
     * @param excel
     * @return
     */
    Integer importCustomerList(MultipartFile excel) throws Exception;

    /**
     * 导入其他信息的数据
     * @param excel
     * @return
     */

    Integer importCustomerOtherList(MultipartFile excel) throws Exception;

    /**
     * 更新基础信息
     * @param customerSearchPo
     */
    Integer doUpdate(CustomerSearchPo customerSearchPo);

    /**
     * 不带分页的基础信息列表
     * @param ssaCustomerMainSearchVo
     * @return
     */
    List<SsaCustomerBasicsTempVo> getCustmerList(SsaCustomerMainSearchVo ssaCustomerMainSearchVo);

    /**
     * 不带分页的其他信息
     * @param ssaCustomerOtherVo
     * @return
     */
    List<SsaCustomerOtherVo> getOtherList(SsaCustomerOtherVo ssaCustomerOtherVo);

    /**
     * 获取初始化数据
     * @return
     */
    SsaCustomerMainSearchVo getInitData(String languageFlag,String key);

}
