package com.bmw.ssa.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.entity.*;
import com.bmw.ssa.po.InsuranceDetailDataPo;
import com.bmw.ssa.po.InsuranceHistoryDataPo;
import com.bmw.ssa.po.InsuranceSearchDataPo;
import com.bmw.ssa.vo.InsuranceSearchConditionVo;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.json.JSONException;

import java.util.List;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
public interface InsuranceService extends IService<Insurance>{

    /**
     * 申请列表
     * @param page
     * @param insuranceSearchConditionVo
     * @return
     */
    PageInfo getInsuranceList(InsuranceSearchConditionVo insuranceSearchConditionVo);

    /**
     * 详情数据
     * @param insearchVo
     * @return
     */
    InsuranceDetailDataPo getInsuranceDetail(InsuranceSearchConditionVo insearchVo);

    /**
     * 获取历史数据
     * @param dropList
     * @return
     */
    List<InsuranceHistoryDataPo> getinsuranceHistoryList(DropList dropList);

    /**
     * 写入历史数据
     * @param insp
     * @return
     */
    int insHistoryInsert(InsuranceHistoryDataPo insp);

    /**
     * 编辑
     * @param insp
     * @return
     */
    int doUpdate(InsuranceDetailDataPo insp);


    /**
     * 活动申请
     * @param insuranceDetailDataPo
     * @return
     */
    int doInsert(InsuranceDetailDataPo insuranceDetailDataPo);

    /**
     * 申请 operationType 0-保存 1-申请 2-审批
     * @param insuranceFileData
     */
    void doApply(InsuranceDetailData insuranceDetailData);

    /**
     * 获取经销商名称和区域
     * @param dropList
     * @return
     */
    DropList getRegionAndDealerName(DropList dropList);

    /**
     * 退回
     * @param insuranceDetailData
     * @return
     */

    int doReset(InsuranceDetailData insuranceDetailData);

    /**
     * 拒绝
     * @param insuranceDetailData
     * @return
     */
    int doReject(InsuranceDetailData insuranceDetailData);

    /**
     * 获取用户下区域
     * @param dropList
     * @return
     */

    List<DropList> getRegionListByCharVal(DropList dropList);

    /**
     *校验身份证号是否申请过公积金
     * insId
     * nameCN
     * dealerId
     * idNo
     * languageFlag
     * @param checkINSExistVo
     * @return
     */
    String checkKinsExistWeb(CheckINSExistVo checkINSExistVo) throws JSONException;

    /**
     *
     * @param checkINSExistVo
     * @return
     */
    String checkInsormail(CheckINSExistVo checkINSExistVo);

    /**
     *
     * @param checkINSExistVo
     * @return
     */
    String checkIns(CheckINSExistVo checkINSExistVo);




    InsuranceSearchDataPo getSearchDatas(String deparment,String locale,String customerid);

    PageInfo getInsuranceAppList(InsuranceSearchConditionVo insuranceSearchConditionVo);


}
