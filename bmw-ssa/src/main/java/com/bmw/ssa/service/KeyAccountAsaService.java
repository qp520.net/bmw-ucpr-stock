package com.bmw.ssa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.entity.DropList;
import com.bmw.ssa.entity.KeyAccountAsaDetailData;
import com.bmw.ssa.po.KeyAccountAsa.*;
import com.bmw.ssa.vo.KeyAccountAsaDetailVo;
import com.bmw.ssa.vo.KeyAccountAsaSearchConditionVo;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2019/8/30 0030.
 */
public interface KeyAccountAsaService extends IService<DropList>{
    /**
     * 获取客户类别一级菜单
     * @param languageFlag //国际化标识，1，英文，2：中文
     * @return
     */
    List<CustomerCategoryPo> getCustomerCategoryList( String languageFlag);

    /**
     * 获取客户类别二级菜单
     * @param languageFlag
     * @param panrentId
     * @return
     */
    List<CategotyPo> getCategoryList(String languageFlag, String panrentId);

    /**
     * 获得状态的下拉列表
     * @param languageFlag
     * @return
     */
    List<DropList> getStatusList(String languageFlag);

    /**
     * 获取经销商下拉列表
     * @param languageFlag
     * @param key1
     * @return
     */
    List<DropList> getDealerList(String languageFlag, String key1);

    /**
     * 获得区域下拉列表
     * @param languageFlag
     * @return
     */
    List<RegionPo> getRegionList(String languageFlag);

    /**
     * 获得年份下拉列表
     * @param languageFlag
     * @return
     */
    List<DropList> getYearList(String languageFlag);

    /**
     * 获取查询条件
     * @param languageFlag
     * @param key1
     * @return
     */
    KeyAccountMainPo getKeyAccountMain(String languageFlag,String key1);

    /**
     * 新增申请的条件数据
     * @param languageFlag
     * @param dealerId
     * @return
     */
    CreateKeyAccountAsaPo initInsert(String languageFlag, Integer dealerId);

    /**
     * 新增申请接口
     * @param createKeyAccountAsaPo
     */
    int doInsert(CreateKeyAccountAsaPo createKeyAccountAsaPo) throws Exception;


    /**
     * 查询分页列表
     * @param kasv
     * @return
     */
    PageInfo getKeyAccountAsa(@Param("kasv")KeyAccountAsaSearchConditionVo kasv);

    /**
     * 查询申请详情
     * @param keyAccountAsaDetailVo
     * @return
     */
    KeyAccountAsaDetailData doSelect(KeyAccountAsaDetailVo keyAccountAsaDetailVo);

    /**
     * 更新申请数据
     * @param createKeyAccountAsaPo
     */
    int doUpdate(CreateKeyAccountAsaPo createKeyAccountAsaPo) throws Exception;






}
