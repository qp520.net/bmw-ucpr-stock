package com.bmw.ssa.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.entity.keyComplaint.KeyComplaintManagementSearchFromData;
import com.bmw.ssa.po.KeyAccountAsa.KeyAccountAsaSearchResultData;
import com.bmw.ssa.po.KeyComplaint.KeyComplaintManagementInfoData;
import com.bmw.ssa.po.KeyComplaint.SelectItem;
import com.bmw.ssa.vo.KeyAccountAsaSearchConditionVo;
import com.bmw.ssa.vo.keyComplain.KeyComplaintManagementParameterVo;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

/**
 * Created by Administrator on 2019/9/4 0004.
 */
public interface KeyComplainService extends IService<SelectItem>{

    /**
     * 初始化投诉页面
     * @param language
     * @return
     */
    KeyComplaintManagementSearchFromData getSearchFrom(String language);


    PageInfo ssaComplaint(KeyComplaintManagementParameterVo kasv);


}
