package com.bmw.ssa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.po.nationalAccounts.SubBranchDataPo;
import com.bmw.ssa.vo.nationalAccount.CreateBranchConditionVo;
import com.bmw.ssa.vo.nationalAccount.NationalAccountSearchConditionVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * Created by 16074 on 2019/9/29.
 */
public interface NationalAccountService extends IService<NationalAccountSearchConditionVo> {

    /**
     * 查询分页列表
     * @param nationalAccountSearchConditionVo
     * @return
     */

    PageInfo getNationalAccounts(NationalAccountSearchConditionVo nationalAccountSearchConditionVo);

    /**
     * 初始化分公司信息
     * @param createBranchConditionVo
     * @return
     */
    List<SubBranchDataPo> searchAccountBranchList(CreateBranchConditionVo createBranchConditionVo);
}
