package com.bmw.ssa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.entity.DropList;
import com.bmw.ssa.po.InsuranceSearchDataPo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2019/8/21 0021.
 */
public interface SearchDateService extends IService<DropList> {

    /**
     * 获取搜索条件
     * @param key1 1，英文，8，英文
     * @param languageFlag 27077 最大
     * @return
     */
    InsuranceSearchDataPo getAmaDetaDropDownList(String key1,String languageFlag);

    /**
     * 获取类别数据
     * @param key1 1，英文，8中文
     * @param languageFlag 27077 最大
     * @return
     */
    InsuranceSearchDataPo getDropDownList(String key1, String languageFlag);
}
