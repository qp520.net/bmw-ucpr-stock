package com.bmw.ssa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.entity.speciaCase.SpecialCaseData;
import com.bmw.ssa.vo.specialCase.ExportInfo;
import com.bmw.ssa.vo.specialCase.SpecialCaseVo;
import com.github.pagehelper.PageInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Administrator on 2019/9/23 0023.
 */
public interface SpecialCaseService extends IService<SpecialCaseVo>{

    /**
     *
     * @param specialCaseVo
     * @return
     */
    Integer saveSpcialCase (SpecialCaseVo specialCaseVo, MultipartFile excel);

    /**
     * 查询vin
     * @param specialCaseVo
     * @return
     */
    List<ExportInfo> getSpecialCaseVinByNo(SpecialCaseVo specialCaseVo);

    /**
     * 查询分页列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo getSpecialCaseList(int pageNum,int pageSize);

    /**
     * 编辑
     * @param specialCaseVo
     * @param file
     * @return
     */
    Integer updateSpecialCaseByScNo(SpecialCaseVo specialCaseVo,MultipartFile file);


    SpecialCaseVo getSpecialCaseByNo(SpecialCaseVo specialCaseVo);

}
