package com.bmw.ssa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ssa.po.specialDiscount.AddInitPo;
import com.bmw.ssa.po.specialDiscount.SearchInitPo;
import com.bmw.ssa.vo.sdaTerminate.SDATerminateVo;
import com.bmw.ssa.vo.specialDiscount.*;
import com.github.pagehelper.PageInfo;

/**
 * Created by Administrator on 2019/9/9 0009.
 */
public interface SpecialDiscountService extends IService<SpecialDiscountApplicationVo>{
    /**
     * 查询特殊协议申请列表
     * @param specialDiscountApplicationVo
     * @return
     */
    PageInfo getSpecialDiscountApplicationData(SpecialDiscountApplicationVo specialDiscountApplicationVo);


    /**
     * 初始化查询条件
     * @param searchInitVo
     * @return
     */
    SearchInitPo getSearchInitData(SearchInitVo searchInitVo);

    /**
     * 初始化新增数据
     * @param searchInitVo
     * @return
     */

    AddInitPo getAddInitData(SearchInitVo searchInitVo);


    /**
     * 新增接口sda
     * @param ssaSda
     * @return
     * @throws Exception
     */
    Integer insertSda(SsaSda ssaSda) throws Exception;

    /**
     * 编辑ssd
     * @param ssaSda
     * @return
     * @throws Exception
     */
    Integer updateSda(SsaSda ssaSda) throws Exception;


    /**
     * 删除申请
     * @param sdaId
     * @throws Exception
     */

    void deleteSda(String sdaId) throws Exception;

    /**
     * 进入修改申请用的初始化数据
     * @param sdkNo
     * @return
     */
    SDAModifyData modifyGetSDA(String sdkNo);

    /**
     * 终止
     * @param procVo
     */
    void termSdaProc(ProcVo procVo);

    /**
     * 修改申请
     * @param vo
     */
    Integer modifyOk(SDAModifyVo vo) throws Exception;


    Integer terminateApplication(SDATerminateVo vo) throws Exception;

    /**
     * 获取折扣信息
     * @param discountVo
     * @return
     */

    DiscountVo getDiscByCategory(DiscountVo discountVo);
}
