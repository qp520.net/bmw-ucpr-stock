package com.bmw.ssa.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.core.web.exception.BmwException;
import com.bmw.ssa.entity.UserAccount;
import com.bmw.ssa.vo.LevelVo;
import com.bmw.ssa.vo.LoginVo;

/**
 * Created by Administrator on 2019/8/15 0015.
 */
public interface UserAccountService extends IService<UserAccount>{

    UserAccount findUserByUserName(LoginVo loginVo);

    /***
     * 市场活动的Level设定
     * @param levelVo
     * @return
     */
    LevelVo selectKeyAccountAmaApprover(LevelVo levelVo);
}
