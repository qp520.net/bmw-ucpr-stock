package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.entity.Account;
import com.bmw.ssa.po.CheckforFleetAccountPo;
import com.bmw.ssa.repository.AccountMapper;
import com.bmw.ssa.service.AccountService;
import com.bmw.ssa.vo.CheckforFleetAccountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2019/8/16 0016.
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper,Account> implements AccountService {
    @Autowired
    AccountMapper accountMapper;
    @Override
    public CheckforFleetAccountPo checkforFleetAccount(CheckforFleetAccountVo checkforFleetAccountVo) {
        return accountMapper.getAccountForAccount(checkforFleetAccountVo);
    }

    @Override
    public CheckforFleetAccountPo getKeyASA(CheckforFleetAccountVo checkforFleetAccountVo) {
        return accountMapper.getKeyASA(checkforFleetAccountVo);
    }
}
