package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.constants.Constants;
import com.bmw.ssa.entity.emailVerify.EmailVerifyAccount;
import com.bmw.ssa.repository.EmailVerifyMapper;
import com.bmw.ssa.service.EmailVerifyService;
import com.bmw.ssa.vo.emailVerify.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2019/9/19 0019.
 */
@Service
public class EmailVerifyServiceImpl extends ServiceImpl<EmailVerifyMapper, FleetAccountsParameterVo> implements EmailVerifyService {
    @Autowired
    private EmailVerifyMapper emailVerifyMapper;

    @Override
    public PageInfo getFleetAccount(FleetAccountsParameterVo fleetAccountsParameterVo) {
        PageHelper.startPage(fleetAccountsParameterVo.getPageNum(), fleetAccountsParameterVo.getPageSize());
        return new PageInfo<FleetAccountsInfoVo>(emailVerifyMapper.getFleetAccount(fleetAccountsParameterVo));
    }

    @Override
    public PageInfo getEmployeeEmail(EmployeeParameterVo employeeParameterVo) {
        String fleetOnly = "fleet only";
        PageHelper.startPage(employeeParameterVo.getPageNum(), employeeParameterVo.getPageSize());
        if(fleetOnly.equals(employeeParameterVo.getCustomerName())){
            employeeParameterVo.setHandleClass("A");
        }else if(StringUtils.isNotEmpty(employeeParameterVo.getDealerId())
                && StringUtils.isNotEmpty(employeeParameterVo.getAccountId())){
            employeeParameterVo.setHandleClass("B");
        }else {
            employeeParameterVo.setHandleClass("C");
        }
        return new PageInfo<EmailVerifyAccount>(emailVerifyMapper.getEmployeeEmail(employeeParameterVo));
    }

    @Override
    public List<String> getEmailDomainsASA(EmailDomainParameterVo emailDomainParameterVo) throws Exception{
        String[] statusArray = {"85", "86", "87", "88", "89"};
        if (!(StringUtils.isNoneEmpty(emailDomainParameterVo.getStatus())
                && ArrayUtils.toString(statusArray).contains(emailDomainParameterVo.getStatus()))) {
            return emailVerifyMapper.getEmailDomainsASA(emailDomainParameterVo);
        } else {
            EmailDomainVo dealer = emailVerifyMapper.getFleetAccountDelerId(emailDomainParameterVo);
            if (dealer.getDealerId().equals(Constants.GET_COMMON_DEALER_27077)) {
                return emailVerifyMapper.getFleetAccountParent(emailDomainParameterVo);
            } else {
                return emailVerifyMapper.getEmailDomainsSUB(emailDomainParameterVo);
            }
        }
    }



}
