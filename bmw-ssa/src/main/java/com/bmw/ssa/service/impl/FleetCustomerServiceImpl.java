package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.excel.ExcelUtil;
import com.bmw.ssa.po.fleetCustomer.CustomerSearchPo;
import com.bmw.ssa.repository.FleetCustomerMapper;
import com.bmw.ssa.repository.KeyAccountAsaMapper;
import com.bmw.ssa.service.FleetCustomerService;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerBasicsTempVo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerMainSearchVo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerOtherVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/9/24 0024.
 */
@Service
public class FleetCustomerServiceImpl extends ServiceImpl<FleetCustomerMapper,SsaCustomerMainSearchVo> implements FleetCustomerService {
    @Autowired
    private FleetCustomerMapper fleetCustomerMapper;

    @Autowired
    private KeyAccountAsaMapper keyAccountAsaMapper;

    @Override
    public PageInfo getCustomerPageList(SsaCustomerMainSearchVo ssaCustomerMainSearchVo) {
        PageHelper.startPage(ssaCustomerMainSearchVo.getPageNum(),ssaCustomerMainSearchVo.getPageSize());
        return new PageInfo(fleetCustomerMapper.getCustomerBasicsInfoDataList(ssaCustomerMainSearchVo));
    }

    @Override
    @Transactional
    public Integer importCustomerList(MultipartFile excel) throws Exception{
        List<Object> dataList =  ExcelUtil.readExcel(excel,new SsaCustomerBasicsTempVo());
        List<SsaCustomerBasicsTempVo> customerList = new ArrayList<>();
        assert dataList != null;
        for (Object obj :dataList) {
            SsaCustomerBasicsTempVo temp = (SsaCustomerBasicsTempVo) obj;
            fleetCustomerMapper.deleteCustomerBasicsByPosition(temp);
            customerList.add(temp);
        }
        if(customerList.size() > 0){
            fleetCustomerMapper.batchInsertCustomerBasicsTemp(customerList);

        }
        return 1;
    }

    @Override
    @Transactional
    public Integer importCustomerOtherList(MultipartFile excel) throws Exception{
        List<Object> dataList =  ExcelUtil.readExcel(excel,new SsaCustomerOtherVo());
        List<SsaCustomerOtherVo> otherList = new ArrayList<>();
        assert dataList != null;
        for (Object obj :dataList) {
            SsaCustomerOtherVo temp = (SsaCustomerOtherVo) obj;
            fleetCustomerMapper.deleteCustomerOther(temp);
            otherList.add(temp);
        }
        if(otherList.size() > 0){
            fleetCustomerMapper.batchInsertCustomerOtherTemp(otherList);

        }
        return 1;
    }

    @Override
    public Integer doUpdate(CustomerSearchPo customerSearchPo) {
        fleetCustomerMapper.doBasicsUpdate(customerSearchPo);
        return 1;
    }

    @Override
    public PageInfo getCustomerOtherPageList(SsaCustomerOtherVo customerOtherVo) {
        PageHelper.startPage(customerOtherVo.getPageNum(),customerOtherVo.getPageSize());
        return new PageInfo(fleetCustomerMapper.getCustomerOtherInfoDataList(customerOtherVo));
    }

    @Override
    public List<SsaCustomerBasicsTempVo> getCustmerList(SsaCustomerMainSearchVo ssaCustomerMainSearchVo) {
        return fleetCustomerMapper.getCustomerBasicsInfoDataList(ssaCustomerMainSearchVo);
    }

    @Override
    public List<SsaCustomerOtherVo> getOtherList(SsaCustomerOtherVo ssaCustomerOtherVo) {
        return fleetCustomerMapper.getCustomerOtherInfoDataList(ssaCustomerOtherVo);
    }

    @Override
    public SsaCustomerMainSearchVo getInitData(String languageFlag,String key) {
        SsaCustomerMainSearchVo ssaCustomerMainSearchVo = new SsaCustomerMainSearchVo();
        ssaCustomerMainSearchVo.setDealerList(keyAccountAsaMapper.getDealerList(languageFlag,key));
        ssaCustomerMainSearchVo.setRegionList(keyAccountAsaMapper.getRegionList(languageFlag));
        ssaCustomerMainSearchVo.setYearList(fleetCustomerMapper.getYearList(languageFlag));
        return ssaCustomerMainSearchVo;
    }
}
