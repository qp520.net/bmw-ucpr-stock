package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.constants.Constants;
import com.bmw.ssa.entity.*;
import com.bmw.ssa.po.InsuranceDetailDataPo;
import com.bmw.ssa.po.InsuranceHistoryDataPo;
import com.bmw.ssa.po.InsuranceSearchDataPo;
import com.bmw.ssa.repository.InsuranceMapper;
import com.bmw.ssa.service.InsuranceService;
import com.bmw.ssa.service.SearchDateService;
import com.bmw.ssa.vo.InsuranceSearchConditionVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
@Service
public class InsuranceServiceImpl extends ServiceImpl<InsuranceMapper,Insurance> implements InsuranceService {

    @Autowired
    private InsuranceMapper insuranceMapper;

    @Autowired
    private SearchDateService searchService;
    @Override
    public PageInfo getInsuranceList(InsuranceSearchConditionVo insuranceSearchConditionVo) {
        PageHelper.startPage(insuranceSearchConditionVo.getPageNumber(),insuranceSearchConditionVo.getPageSize());
        return new PageInfo(insuranceMapper.getInsuranceList(insuranceSearchConditionVo));
    }

    @Override
    public InsuranceDetailDataPo getInsuranceDetail(InsuranceSearchConditionVo insearchVo) {
        return insuranceMapper.getInsuranceDetail(insearchVo);
    }

    @Override
    public List<InsuranceHistoryDataPo> getinsuranceHistoryList(DropList dropList) {
        return insuranceMapper.getinsuranceHistoryList(dropList);
    }

    @Override
    public int insHistoryInsert(InsuranceHistoryDataPo insp) {
        return insuranceMapper.insHistoryInsert(insp);
    }

    @Override
    public int doUpdate(InsuranceDetailDataPo insp) {
        return insuranceMapper.doUpdate(insp);
    }

    @Override
    public int doInsert(InsuranceDetailDataPo insuranceDetailDataPo) {
        return insuranceMapper.doInsert(insuranceDetailDataPo);
    }

    @Override
    public void doApply(InsuranceDetailData insuranceDetailData) {
        insuranceMapper.doApply(insuranceDetailData);
    }

    @Override
    public DropList getRegionAndDealerName(DropList dropList) {
        return insuranceMapper.getRegionAndDealerName(dropList);
    }

    @Override
    public int doReset(InsuranceDetailData insuranceDetailData) {
        return insuranceMapper.doReset(insuranceDetailData);
    }

    @Override
    public int doReject(InsuranceDetailData insuranceDetailData) {
        return insuranceMapper.doReject(insuranceDetailData);
    }

    @Override
    public List<DropList> getRegionListByCharVal(DropList dropList) {
        return insuranceMapper.getRegionListByCharVal(dropList);
    }

    @Override
    public String checkKinsExistWeb(CheckINSExistVo checkINSExistVo) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        String result = insuranceMapper.checkKinsExistWeb(checkINSExistVo);
        jsonObject.put("status",result);
        return jsonObject.toString() ;
    }

    @Override
    public String checkInsormail(CheckINSExistVo checkINSExistVo) {
        return insuranceMapper.checkInsormail(checkINSExistVo);
    }

    @Override
    public String checkIns(CheckINSExistVo checkINSExistVo) {
        return insuranceMapper.checkIns(checkINSExistVo);
    }

    @Override
    public InsuranceSearchDataPo getSearchDatas(String deparment, String locale, String customerid) {
        InsuranceSearchDataPo dropDown = searchService.getDropDownList(Constants.GET_COMMON_DEALER_27077,locale);
        if (!customerid.equals(Constants.GET_COMMON_DEALER_27077)) {
            DropList dropList = new DropList();
            dropList.setId(customerid);
            dropList.setLanguageFlag(locale);
            //获取经销商名称和区域
            DropList dropListVo=getRegionAndDealerName(dropList);
            if(dropListVo !=null){
                dropDown.setRegion(dropListVo.getKey1());
                dropDown.setDealer(dropListVo.getKey2());
                dropListVo.setKey1("");
                dropListVo.setKey2("");
                dropDown.getDealerList().clear();
                dropDown.getDealerList().add(dropListVo);
            }
        }
        //获取用户下区域
        DropList dropList = new DropList();
        if ("North".equals(deparment) || "South".equals(deparment)
                || "East".equals(deparment) || "West".equals(deparment)
                || "Southeast".equals(deparment)) {
            dropList.setId(deparment);

        }
        dropList.setLanguageFlag(locale);
        List<DropList> resultList =  getRegionListByCharVal(dropList);
        List<DropList> regionList = new ArrayList<DropList>();
        for (DropList resultData : resultList) {
            if ("5".equals(resultData.getId())) {
                resultData.setId("");
            }
            regionList.add(resultData);
        }
        dropDown.setRegionList(regionList);
        return dropDown;
    }

    @Override
    public PageInfo getInsuranceAppList(InsuranceSearchConditionVo insuranceSearchConditionVo) {
        PageHelper.startPage(insuranceSearchConditionVo.getPageNumber(),insuranceSearchConditionVo.getPageSize());
        return new PageInfo(insuranceMapper.getInsuranceAppList(insuranceSearchConditionVo));
    }
}
