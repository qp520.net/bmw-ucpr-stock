package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.constants.Constants;
import com.bmw.ssa.entity.DropList;
import com.bmw.ssa.entity.KeyAccountAsaDetailData;
import com.bmw.ssa.po.KeyAccountAsa.*;
import com.bmw.ssa.repository.KeyAccountAsaMapper;
import com.bmw.ssa.service.KeyAccountAsaService;
import com.bmw.ssa.utils.KeyAccountAsaEnum;
import com.bmw.ssa.vo.KeyAccountAsaDetailVo;
import com.bmw.ssa.vo.KeyAccountAsaSearchConditionVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2019/8/30 0030.
 */
@Service
public class KeyAccountAsaServiceImpl extends ServiceImpl<KeyAccountAsaMapper,DropList> implements KeyAccountAsaService {
    @Autowired
    private KeyAccountAsaMapper keyAccountAsaMapper;

    @Override
    public List<CustomerCategoryPo> getCustomerCategoryList(String languageFlag) {
        return keyAccountAsaMapper.getCustomerCategoryList(languageFlag);
    }

    @Override
    public List<CategotyPo> getCategoryList(String languageFlag, String panrentId) {
        //根据一级菜单获取二级菜单的父ID
        int key = KeyAccountAsaEnum.getvalue(Integer.valueOf(panrentId));
        return keyAccountAsaMapper.getCategoryList(languageFlag,String.valueOf(key));
    }

    @Override
    public List<DropList> getStatusList(String languageFlag) {
        return keyAccountAsaMapper.getStatusList(Integer.valueOf(languageFlag));
    }

    @Override
    public List<DropList> getDealerList(String languageFlag, String key1) {
        return keyAccountAsaMapper.getDealerList(languageFlag,key1);
    }

    @Override
    public List<RegionPo> getRegionList(String languageFlag) {
        return keyAccountAsaMapper.getRegionList(languageFlag);
    }

    @Override
    public List<DropList> getYearList(String languageFlag) {
        return keyAccountAsaMapper.getYearList(languageFlag);
    }

    @Override
    public KeyAccountMainPo getKeyAccountMain(String languageFlag,String key1) {
        KeyAccountMainPo keyAccountMainPo = new KeyAccountMainPo();
        List<CustomerCategoryPo> customerCategoryPoList = getCustomerCategoryList(languageFlag);
        for (CustomerCategoryPo cp:customerCategoryPoList) {
            cp.setCategotyList(getCategoryList(languageFlag,String.valueOf(cp.getId())));
        }
        keyAccountMainPo.setCustomerCategoryList(customerCategoryPoList);
        keyAccountMainPo.setStatusList(getStatusList(languageFlag));
        keyAccountMainPo.setRegionList(getRegionList(languageFlag));
        keyAccountMainPo.setDealerList(getDealerList(languageFlag,key1));
        keyAccountMainPo.setYearList(getYearList(languageFlag));
        return keyAccountMainPo;
    }

    @Override
    public CreateKeyAccountAsaPo initInsert(String languageFlag, Integer dealerId) {
        CreateKeyAccountAsaPo createKeyAccountAsaPo = new CreateKeyAccountAsaPo();
        List<CustomerCategoryPo> customerCategoryPoList = getCustomerCategoryList(languageFlag);
        for (CustomerCategoryPo c:customerCategoryPoList) {
            int panrentId = KeyAccountAsaEnum.getvalue(c.getId());
            List<CategotyPo> categoryList = getCategoryList(languageFlag,String.valueOf(panrentId));
            c.setCategotyList(categoryList);
        }
        createKeyAccountAsaPo.setStatus(Constants.ASA_STATUS_10);
        /** 写入dealer数据
        createKeyAccountAsaPo.setDealerId(dealerId);
        createKeyAccountAsaPo.setDealerComplay("");**/
        if(dealerId.equals(Constants.ACCOUNT_27077)){
            createKeyAccountAsaPo.setProvience("nationwide");
        }else {
            List<RegionPo> regionList = getRegionList(languageFlag);
            createKeyAccountAsaPo.setRegionId(regionList.get(0).getId());
            createKeyAccountAsaPo.setRegionName(regionList.get(0).getRegionName());
        }
        /**
         * createKeyAccountAsaPo.setUserName(userName);
         */
        /**前端直接写死的2%，3%，4%
         * createKeyAccountAsaPo.setBmwSupport();
         */

        return createKeyAccountAsaPo;
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE,rollbackFor = RuntimeException.class)
    public int doInsert(CreateKeyAccountAsaPo createKeyAccountAsaPo) throws Exception{
        keyAccountAsaMapper.doInsert(createKeyAccountAsaPo);
        keyAccountAsaMapper.insertHistory(createKeyAccountAsaPo);
        keyAccountAsaMapper.doApply(createKeyAccountAsaPo);
        return 1;
    }

    @Override
    public PageInfo getKeyAccountAsa(KeyAccountAsaSearchConditionVo kasv) {
        PageHelper.startPage(kasv.getPageNumber(),kasv.getPageSize());
        return new PageInfo(keyAccountAsaMapper.getKeyAccountAsa(kasv));
    }

    @Override
    public KeyAccountAsaDetailData doSelect(KeyAccountAsaDetailVo keyAccountAsaDetailVo) {
        KeyAccountAsaDetailData keyAccountAsaDetailData = keyAccountAsaMapper.doSelect(keyAccountAsaDetailVo);
        if(keyAccountAsaDetailData!=null){
            List<HistoryDataPo> historyDataList = keyAccountAsaMapper.getHistoryList(keyAccountAsaDetailVo);
            keyAccountAsaDetailData.setHistoryDataList(historyDataList);
        }

        return keyAccountAsaDetailData;
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE,rollbackFor = RuntimeException.class)
    public int doUpdate(CreateKeyAccountAsaPo createKeyAccountAsaPo)throws Exception {
        keyAccountAsaMapper.doUpdate(createKeyAccountAsaPo);
        keyAccountAsaMapper.insertHistory(createKeyAccountAsaPo);
        //保存文件没写
        keyAccountAsaMapper.doApply(createKeyAccountAsaPo);
        return 1;
    }

}
