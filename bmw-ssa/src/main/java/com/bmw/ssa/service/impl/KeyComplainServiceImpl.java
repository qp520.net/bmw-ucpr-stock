package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.entity.keyComplaint.KeyComplaintManagementSearchFromData;
import com.bmw.ssa.po.KeyAccountAsa.KeyAccountAsaSearchResultData;
import com.bmw.ssa.po.KeyComplaint.KeyComplaintManagementInfoData;
import com.bmw.ssa.po.KeyComplaint.SelectItem;
import com.bmw.ssa.repository.KeyComplaintMapper;
import com.bmw.ssa.service.KeyComplainService;
import com.bmw.ssa.vo.KeyAccountAsaSearchConditionVo;
import com.bmw.ssa.vo.keyComplain.KeyComplaintManagementParameterVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2019/9/4 0004.
 */
@Service
public class KeyComplainServiceImpl extends ServiceImpl<KeyComplaintMapper,SelectItem> implements KeyComplainService {

    @Autowired
    private KeyComplaintMapper keyComplaintMapper;
    @Override
    public KeyComplaintManagementSearchFromData getSearchFrom(String language) {
        KeyComplaintManagementSearchFromData searchFormDate = new KeyComplaintManagementSearchFromData();
        searchFormDate.setPriorityItem(keyComplaintMapper.getPriorityItem(language));
        searchFormDate.setComplaintSourceItem(keyComplaintMapper.getComplaintSourceItem(language));
        searchFormDate.setComplaintTypeItem(keyComplaintMapper.getComplaintTypeItem(language));
        searchFormDate.setStatusItem(keyComplaintMapper.getStatusItem(language));
        return searchFormDate;
    }

    @Override
    public PageInfo ssaComplaint(KeyComplaintManagementParameterVo kmpv) {
        PageHelper.startPage(kmpv.getPageNumber(),kmpv.getPageSize());
        return new PageInfo(keyComplaintMapper.ssaComplaint(kmpv));
    }
}
