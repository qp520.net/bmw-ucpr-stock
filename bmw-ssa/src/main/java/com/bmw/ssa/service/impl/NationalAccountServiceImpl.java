package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.po.nationalAccounts.NationalAccountsSearchDataPo;
import com.bmw.ssa.po.nationalAccounts.SubBranchDataPo;
import com.bmw.ssa.repository.NationalAccountMapper;
import com.bmw.ssa.service.NationalAccountService;
import com.bmw.ssa.vo.nationalAccount.CreateBranchConditionVo;
import com.bmw.ssa.vo.nationalAccount.NationalAccountSearchConditionVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 16074 on 2019/9/29.
 */
@Service
public class NationalAccountServiceImpl extends ServiceImpl<NationalAccountMapper,NationalAccountSearchConditionVo> implements NationalAccountService {
    @Autowired
    private NationalAccountMapper nationalAccountMapper;
    @Override
    public PageInfo getNationalAccounts(NationalAccountSearchConditionVo nscv) {
        PageHelper.startPage(nscv.getPageNum(),nscv.getPageSize());
        List<NationalAccountsSearchDataPo> list = nationalAccountMapper.getNationalAccounts(nscv);
        for (NationalAccountsSearchDataPo nacv:list) {
            CreateBranchConditionVo cbtv = new CreateBranchConditionVo();
            cbtv.setAccountid(nacv.getDmsCustomerNo());
            cbtv.setDealerid(nacv.getDealerId());
            cbtv.setLocale(String.valueOf(nscv.getEn_ch_flag()));
            List<SubBranchDataPo> branchList = searchAccountBranchList(cbtv);
            nacv.setBranchList(branchList);
        }
        return new PageInfo(list);
    }

    @Override
    public List<SubBranchDataPo> searchAccountBranchList(CreateBranchConditionVo createBranchConditionVo) {
        return nationalAccountMapper.searchAccountBranchList(createBranchConditionVo);
    }
}
