package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.entity.DropList;
import com.bmw.ssa.entity.KeyAuthorityAmaSearchData;
import com.bmw.ssa.po.InsuranceSearchDataPo;
import com.bmw.ssa.repository.SearchDataMapper;
import com.bmw.ssa.service.SearchDateService;
import com.bmw.ssa.utils.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Administrator on 2019/8/21 0021.
 */
@Service
public class SearchDateServiceImpl extends ServiceImpl<SearchDataMapper,DropList> implements SearchDateService {

    @Autowired
    private SearchDataMapper searchDataMapper;
    @Override
    public InsuranceSearchDataPo getAmaDetaDropDownList(String key1, String languageFlag) {
        List<InsuranceSearchDataPo> dataResult =searchDataMapper.getAmaDetaDropDownList("8","27077");
        InsuranceSearchDataPo dropDown = new InsuranceSearchDataPo();
        for (InsuranceSearchDataPo data: dataResult) {
            switch (Integer.parseInt(data.getListType())) {
                case 1: {
                    dropDown.setStatusList(data.getCourseList());
                    break;
                }
                case 2: {
                    dropDown.setRegionList(data.getCourseList());
                    break;
                }
                case 3: {
                    dropDown.setCustomerCategoryList(data.getCourseList());
                    dropDown.setCustomerCategoryDataeList(data.getCourseList());
                    break;
                }
                case 4: {
                    dropDown.setDealerList(data.getCourseList());
                    break;
                }
                case 5: {
                    dropDown.setFleetCustomerCategoryList(data.getCourseList());
                    break;
                }
                case 63: {
                    dropDown.setRemarkList57(data.getCourseList());
                    dropDown.setRemarkList(data.getCourseList());
                    break;
                }
                case 112: {
                    dropDown.setRemarkList94(data.getCourseList());
                    break;
                }
                case 113: {
                    dropDown.setRemarkList96(data.getCourseList());
                    break;
                }
                case 148: {
                    dropDown.setRemarkList60(data.getCourseList());
                    break;
                }
                case 238: {
                    dropDown.setResetList(data.getCourseList());
                    break;
                }
                case 239: {
                    dropDown.setRejectList(data.getCourseList());
                    break;
                }
                case 420: {
                    dropDown.setGiftNumList(data.getCourseList());
                    break;
                }
                case 422: {
                    dropDown.setIResetList(data.getCourseList());
                    break;
                }
                case 423: {
                    dropDown.setIRejectList(data.getCourseList());
                    break;
                }

            }
        }
        return dropDown;
    }

    @Override
    public InsuranceSearchDataPo getDropDownList(String key1, String languageFlag) {
        List<KeyAuthorityAmaSearchData> dataResult =searchDataMapper.getDropDownList(key1,languageFlag);
        InsuranceSearchDataPo dropDown = new InsuranceSearchDataPo();
        System.out.println(dataResult);
        for (KeyAuthorityAmaSearchData data: dataResult) {
            switch (Integer.parseInt(data.getListType())) {
                case 1: {
                    dropDown.setStatusList(data.getCourseList());
                    break;
                }
                case 2: {
                    dropDown.setRegionList(data.getCourseList());
                    break;
                }
                case 3: {
                    dropDown.setCustomerCategoryList(data.getCourseList());
                    dropDown.setCustomerCategoryDataeList(data.getCourseList());
                    break;
                }
                case 4: {
                    dropDown.setDealerList(data.getCourseList());
                    break;
                }
                case 5: {
                    dropDown.setFleetCustomerCategoryList(data.getCourseList());
                    break;
                }
                case 63: {
                    dropDown.setRemarkList57(data.getCourseList());
                    dropDown.setRemarkList(data.getCourseList());
                    break;
                }
                case 112: {
                    dropDown.setRemarkList94(data.getCourseList());
                    break;
                }
                case 113: {
                    dropDown.setRemarkList96(data.getCourseList());
                    break;
                }
                case 148: {
                    dropDown.setRemarkList60(data.getCourseList());
                    break;
                }
                case 238: {
                    dropDown.setResetList(data.getCourseList());
                    break;
                }
                case 239: {
                    dropDown.setRejectList(data.getCourseList());
                    break;
                }
                case 420: {
                    dropDown.setGiftNumList(data.getCourseList());
                    break;
                }
                case 422: {
                    dropDown.setIResetList(data.getCourseList());
                    break;
                }
                case 423: {
                    dropDown.setIRejectList(data.getCourseList());
                    break;
                }
            }
        }
        return dropDown;
    }
}
