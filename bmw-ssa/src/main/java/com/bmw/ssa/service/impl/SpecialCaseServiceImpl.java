package com.bmw.ssa.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.constants.Constants;
import com.bmw.ssa.entity.speciaCase.SpecialCaseData;
import com.bmw.ssa.excel.ExcelUtil;
import com.bmw.ssa.repository.SpecialCaseMapper;
import com.bmw.ssa.service.SpecialCaseService;
import com.bmw.ssa.vo.specialCase.ExportInfo;
import com.bmw.ssa.vo.specialCase.SpecialCaseVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/9/23 0023.
 */
@Service
@Slf4j
public class SpecialCaseServiceImpl extends ServiceImpl<SpecialCaseMapper,SpecialCaseVo> implements SpecialCaseService {
    @Autowired
    private SpecialCaseMapper specialCaseMapper;
    @Override
    @Transactional
    public Integer saveSpcialCase(SpecialCaseVo specialCaseVo, MultipartFile excel) {
        specialCaseVo =  getNewInitSCNO(specialCaseVo);
        specialCaseMapper.insertSepc(specialCaseVo);
        if(Constants.SPECIALCASE_VIN.equals(specialCaseVo.getSctype())){
            setListForSpecialVin(excel,specialCaseVo);
        }
        return 1;
    }

    /**
     * 获取Scno
     * @param data
     * @return
     */
    public SpecialCaseVo getNewInitSCNO(SpecialCaseVo data){
        SpecialCaseVo result = specialCaseMapper.getSpecialCaseNo();
        StringBuffer scno = new StringBuffer(result.getScno());
        while(scno.length()<5){
            scno.insert(0, "0");
        }
        scno.insert(0, result.getNowdate());
        scno.insert(0, "SC");
        data.setScno(scno.toString());
        data.setSccreatedate(result.getSccreatedate());
        return data;
    }

    public void setListForSpecialVin(MultipartFile file,SpecialCaseVo specialCaseVo){
        List<Object> vinData = ExcelUtil.readExcel(file,new ExportInfo());
        log.debug(JSON.toJSONString(vinData));
        List<ExportInfo> list = new ArrayList<>();
        assert vinData != null;
        for (Object aVinData : vinData) {
            ExportInfo ex = (ExportInfo) aVinData;
            ex.setScNo(specialCaseVo.getScno());
            list.add(ex);
        }
        specialCaseMapper.insertSepcVin(list);

    }

    @Override
    public List<ExportInfo> getSpecialCaseVinByNo(SpecialCaseVo specialCaseVo) {
        return specialCaseMapper.getSpecialCaseVinByNo(specialCaseVo);
    }

    @Override
    public PageInfo getSpecialCaseList(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<SpecialCaseVo> list = specialCaseMapper.getSpecialCaseList();
        return new PageInfo(list);
    }

    @Override
    @Transactional
    public Integer updateSpecialCaseByScNo(SpecialCaseVo specialCaseVo, MultipartFile excel) {
        specialCaseMapper.updateSpec(specialCaseVo);
        if(Constants.SPECIALCASE_VIN.equals(specialCaseVo.getSctype()) && excel !=null){
            specialCaseMapper.deleteScVin(specialCaseVo);
            setListForSpecialVin(excel,specialCaseVo);
        }
        return 1;
    }

    @Override
    public SpecialCaseVo getSpecialCaseByNo(SpecialCaseVo specialCaseVo) {
        return specialCaseMapper.getSpecialCaseByNo(specialCaseVo);
    }
}
