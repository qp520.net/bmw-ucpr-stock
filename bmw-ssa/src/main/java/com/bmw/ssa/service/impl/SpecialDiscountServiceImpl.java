package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ssa.constants.Constants;
import com.bmw.ssa.entity.speciaDiscount.SpecialDiscountApplicationData;
import com.bmw.ssa.entity.speciaDiscount.SsaSdaHistory;
import com.bmw.ssa.po.specialDiscount.AddInitPo;
import com.bmw.ssa.po.specialDiscount.SearchInitPo;
import com.bmw.ssa.po.specialDiscount.SelectItemPo;
import com.bmw.ssa.repository.SdaModifyMapper;
import com.bmw.ssa.repository.SdaTerminateMapper;
import com.bmw.ssa.repository.SpeciaDiscountMapper;
import com.bmw.ssa.service.SpecialDiscountService;
import com.bmw.ssa.vo.sdaTerminate.SDATerminateVo;
import com.bmw.ssa.vo.specialDiscount.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2019/9/9 0009.
 */
@Service
public class SpecialDiscountServiceImpl extends ServiceImpl<SpeciaDiscountMapper,SpecialDiscountApplicationVo> implements SpecialDiscountService {

    @Autowired
    private SpeciaDiscountMapper speciaDiscountMapper;
    @Autowired
    private SdaModifyMapper sdaModifyMapper;

    @Autowired
    private SdaTerminateMapper sdaTerminateMapper;

    @Override
    public PageInfo getSpecialDiscountApplicationData(SpecialDiscountApplicationVo specialDiscountApplicationVo) {
        PageHelper.startPage(specialDiscountApplicationVo.getPageNum(),specialDiscountApplicationVo.getPageSize());
        List<SpecialDiscountApplicationData> list = speciaDiscountMapper.getSpecialDiscountApplicationData(specialDiscountApplicationVo);
        PageInfo<SpecialDiscountApplicationData> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    public SearchInitPo getSearchInitData(SearchInitVo searchInitVo) {
        SearchInitPo searchInitPo = new SearchInitPo();
        List<SelectItemPo> customerDataList = speciaDiscountMapper.getCustomerData(searchInitVo);
        for (SelectItemPo s:customerDataList) {
            String cat = s.getKey1().substring(0,2);
        }
        List<SelectItemPo> list = speciaDiscountMapper.getSearchCategory(searchInitVo.getCategory());
        searchInitPo.setCustomerList(speciaDiscountMapper.getCustomerData(searchInitVo));
        searchInitPo.setCategoryList(speciaDiscountMapper.getSearchCategory(searchInitVo.getCategory()));
        searchInitPo.setDealertList(speciaDiscountMapper.getDealerList(searchInitVo));
        return searchInitPo;
    }

    @Override
    public AddInitPo getAddInitData(SearchInitVo searchInitVo) {
        AddInitPo init =new AddInitPo();
        init.setCategoryList(speciaDiscountMapper.getCategoryList(searchInitVo.getCategory()));
        init.setCustomerList(speciaDiscountMapper.getCustomerData(searchInitVo));
        init.setSeriesList(speciaDiscountMapper.getSeriesList());
        init.setVariantList(speciaDiscountMapper.getVariantList(searchInitVo.getModelCode()));
        return init;
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Integer insertSda(SsaSda ssaSda) throws Exception {
         speciaDiscountMapper.insertSsaSda(ssaSda);
        Integer sdaId = Integer.valueOf(ssaSda.getSDA_ID());
        SsaSdaHistory ssahistory = new SsaSdaHistory();
        ssahistory.setSDA_ID(String.valueOf(sdaId));
        ssahistory.setACTION_ID("1");
        ssahistory.setRESULT_ID("1");
        ssahistory.setUSER_NAME(ssaSda.getUSER_NAME());
        speciaDiscountMapper.insertSsaSdaHis(ssahistory);
        speciaDiscountMapper.updateSsaSda(ssaSda);
        return sdaId;
    }

    @Override
    public Integer updateSda(SsaSda ssaSda) throws Exception {
        int result = 0 ;
        speciaDiscountMapper.updateSsaSda(ssaSda);
        return result;
    }


    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void deleteSda(String sdaId) throws Exception {
        speciaDiscountMapper.deleteSsaSda(sdaId);
        speciaDiscountMapper.deleteSsaSdaHistory(sdaId);
    }

    @Override
    public SDAModifyData modifyGetSDA(String sdkNo) {
        SDAModifyData sdaModifyData = speciaDiscountMapper.getSDAByNOForModify(sdkNo);
        sdaModifyData.setVarientList(speciaDiscountMapper.getSDAModifyVariant(sdaModifyData.getModel()));
        return sdaModifyData;
    }

    @Override
    public void termSdaProc(ProcVo procVo) {
        speciaDiscountMapper.termSdaProc(procVo);
    }

    @Override
    @Transactional
    public Integer modifyOk(SDAModifyVo vo) throws Exception{
        int result =0;
        vo.setMtflag("N");
        speciaDiscountMapper.updateSDAMTflag(vo);
        sdaModifyMapper.insertSDAModify(vo);
        return result;
    }

    @Override
    @Transactional
    public Integer terminateApplication(SDATerminateVo vo) throws Exception{
        int result = 0;
        SDAModifyVo sdaModifyVo = new SDAModifyVo();
        sdaModifyVo.setSda_no(vo.getSda_no());
        sdaModifyVo.setMtflag("N");
        SDAHistoryForMT sdaHistoryForMT = new SDAHistoryForMT();
        sdaHistoryForMT.setSda_no(vo.getSda_no());
        sdaHistoryForMT.setAction_id("56");
        sdaHistoryForMT.setResult_text("SDA Terminate Apply");
        speciaDiscountMapper.updateSDAMTflag(sdaModifyVo);
        speciaDiscountMapper.insertSDAHistory(sdaHistoryForMT);
        sdaTerminateMapper.insertSDATerminate(vo);
        return result;
    }

    @Override
    public DiscountVo getDiscByCategory(DiscountVo discountVo) {
        String category = discountVo.getCategory();
        if(category.equals(Constants.CUSTOMER_CATEGORY_57)){
            DiscountVo vo = speciaDiscountMapper.getDISC_C_57_70_97(discountVo);
            discountVo.setDisc_c(checkResult(vo));
            DiscountVo vob = speciaDiscountMapper.getDISC_B_57_94_96(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            if(discountVo.getFinalPrice().equals("0")){
                discountVo.setFinalPrice(Constants.DISC_0);
            }
            if(discountVo.getStickerPrice().equals("0")){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_59)){
            DiscountVo voc = speciaDiscountMapper.getDISC_C_59_60(discountVo);
            discountVo.setDisc_c(checkResult(voc));
            DiscountVo vob = speciaDiscountMapper.getDISC_B_59_60_90(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            DiscountVo vod = speciaDiscountMapper.getDISC_D_59(discountVo);
            discountVo.setDisc_d(checkResult(vod));
            DiscountVo finaPriceVo = speciaDiscountMapper.getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(discountVo);
            discountVo.setFinalPrice(checkResult(finaPriceVo));
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_60)){
            DiscountVo voc = speciaDiscountMapper.getDISC_C_59_60(discountVo);
            discountVo.setDisc_c(checkResult(voc));
            DiscountVo vob = speciaDiscountMapper.getDISC_B_59_60_90(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            DiscountVo finalPriceVO = speciaDiscountMapper.getFINAL_PRICE_60(discountVo);
            discountVo.setFinalPrice(checkResult(finalPriceVO));
            if("0".equals(discountVo.getStickerPrice())){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
            if("0".equals(discountVo.getFinalPrice())){
                discountVo.setFinalPrice(Constants.DISC_0);
            }
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_61)){
            DiscountVo vod = speciaDiscountMapper.getDISC_D_61(discountVo);
            discountVo.setDisc_d(checkResult(vod));
            DiscountVo finalPrice = speciaDiscountMapper.getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(discountVo);
            discountVo.setFinalPrice(checkResult(finalPrice));
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_70)){
            DiscountVo voc = speciaDiscountMapper.getDISC_C_57_70_97(discountVo);
            discountVo.setDisc_c(checkResult(voc));
            DiscountVo vob = speciaDiscountMapper.getDISC_B_70(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            if(discountVo.getStickerPrice().equals("0")){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_80)){
            DiscountVo finalPriceVO = speciaDiscountMapper.getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(discountVo);
            discountVo.setFinalPrice(checkResult(finalPriceVO));
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_81)){
            DiscountVo finalPriceVo = speciaDiscountMapper.getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(discountVo);
            discountVo.setFinalPrice(checkResult(finalPriceVo));
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_90)){
            DiscountVo vob = speciaDiscountMapper.getDISC_B_59_60_90(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            DiscountVo finalPriceVo = speciaDiscountMapper.getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(discountVo);
            discountVo.setFinalPrice(checkResult(finalPriceVo));
            if("0".equals(discountVo.getFinalPrice())){
                discountVo.setFinalPrice(Constants.DISC_0);
            }
            if("0".equals(discountVo.getStickerPrice())){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_92)){
            DiscountVo vob = speciaDiscountMapper.getDISC_B_92(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            DiscountVo finalPriceVO = speciaDiscountMapper.getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(discountVo);
            discountVo.setFinalPrice(checkResult(finalPriceVO));
            if("0".equals(discountVo.getFinalPrice())){
                discountVo.setFinalPrice(Constants.DISC_0);
            }
            if("0".equals(discountVo.getStickerPrice())){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_93)){
            DiscountVo vob = speciaDiscountMapper.getDISC_B_93(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            DiscountVo finalPriceVo = speciaDiscountMapper.getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(discountVo);
            discountVo.setFinalPrice(checkResult(finalPriceVo));
            if("0".equals(discountVo.getStickerPrice())){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
            if("0".equals(discountVo.getFinalPrice())){
                discountVo.setFinalPrice(Constants.DISC_0);
            }
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_94)){
            DiscountVo voc = speciaDiscountMapper.getDISC_C_94_96(discountVo);
            discountVo.setDisc_c(checkResult(voc));
            DiscountVo vob = speciaDiscountMapper.getDISC_B_57_94_96(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            if(discountVo.getStickerPrice().equals("0")){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
            if(discountVo.getFinalPrice().equals("0")){
                discountVo.setFinalPrice(Constants.DISC_0);
            }
//            changeList();
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_95)){
            DiscountVo vob = speciaDiscountMapper.getDISC_B_95(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            DiscountVo finalPriceVo = speciaDiscountMapper.getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(discountVo);
            discountVo.setFinalPrice(checkResult(finalPriceVo));
            if(discountVo.getStickerPrice().equals("0")){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
            if(discountVo.getFinalPrice().equals("0")){
                discountVo.setFinalPrice(Constants.DISC_0);
            }
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_96)){
            DiscountVo voc = speciaDiscountMapper.getDISC_C_94_96(discountVo);
            discountVo.setDisc_c(checkResult(voc));
            DiscountVo vob = speciaDiscountMapper.getDISC_B_57_94_96(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            if(discountVo.getStickerPrice().equals("0")){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
            if(discountVo.getFinalPrice().equals("0")){
                discountVo.setFinalPrice(Constants.DISC_0);
            }
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_97)){
            DiscountVo voc = speciaDiscountMapper.getDISC_C_57_70_97(discountVo);
            discountVo.setDisc_c(checkResult(voc));
            if(discountVo.getStickerPrice().equals("0")){
                discountVo.setStickerPrice(Constants.DISC_0);
            }
            if(discountVo.getFinalPrice().equals("0")){
                discountVo.setFinalPrice(Constants.DISC_0);
            }
        }else if(category.equals(Constants.CUSTOMER_CATEGORY_99)){
            DiscountVo voc = speciaDiscountMapper.getDISC_C_99(discountVo);
            discountVo.setDisc_c(checkResult(voc));
            DiscountVo vob = speciaDiscountMapper.getDISC_B_99(discountVo);
            discountVo.setDisc_b(checkResult(vob));
            DiscountVo finalPriceVo = speciaDiscountMapper.getFINAL_PRICE_59_61_80_81_90_92_93_94_95_96_99(discountVo);
            discountVo.setFinalPrice(checkResult(finalPriceVo));
        }
        return discountVo;
    }

    public String checkResult(DiscountVo discountVo){
        if(discountVo != null){
            return discountVo.getResult();
        }else {
            return Constants.DISC_0;
        }
    }
}
