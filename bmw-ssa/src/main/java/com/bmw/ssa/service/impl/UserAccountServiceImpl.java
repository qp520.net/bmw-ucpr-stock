package com.bmw.ssa.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.core.web.exception.BmwException;
import com.bmw.ssa.constants.ErrorCode;
import com.bmw.ssa.entity.UserAccount;
import com.bmw.ssa.repository.UserAccountMapper;
import com.bmw.ssa.service.UserAccountService;
import com.bmw.ssa.vo.LevelVo;
import com.bmw.ssa.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2019/8/15 0015.
 */
@Service
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper,UserAccount> implements UserAccountService{
    @Autowired
    private UserAccountMapper userAccountMapper;

    @Override
    public UserAccount findUserByUserName(LoginVo loginVo){
        if(loginVo.getUser_name()==null&& loginVo.getPassword()==null){
            throw new BmwException(ErrorCode.ERROR_MSG_4000,ErrorCode.ERROR_CODE_4000);
        }
        return userAccountMapper.findUserByUserName(loginVo.getUser_name());
    }

    @Override
    public LevelVo selectKeyAccountAmaApprover(LevelVo levelVo) {
        return userAccountMapper.selectKeyAccountAmaApprover(levelVo);
    }
}
