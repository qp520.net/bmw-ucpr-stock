package com.bmw.ssa.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Project：BMW SSA Web Base
 * System：SSA
 * Sub System：Fleet Account
 * 日期工具类
 * @author  YangYi
 */
public class DateUtils {

	public static final String YYYYMMDD = "yyyy-MM-dd";

	/**
	 * 日期转换为字符串
	 * 
	 * @param date
	 * @param format
	 * @return
	 */
	public static String dateToString(Date date, String format) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat df = new SimpleDateFormat(format);
		String strDate = df.format(date);
		return strDate;
	}

	/**
	 * 字符串转换为日期
	 * 
	 * @param dateStr
	 * @param format
	 * @return
	 */
	public static Date stringToDate(String dateStr, String format) {
		if (dateStr == null || "".equals(dateStr)) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 取得系统当前时间字符串
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurrentDateStr(String format) {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(format);
		String time = df.format(date);
		return time;
	}
	
/**
 * @方法功能描述：获取N天前的日期时间
 * @特殊参数描述：
 * @创建人：QXO7129
 * @创建时间：Dec 17, 2016 10:44:29 AM
 */ 
	public static String getDateTimeByDays(int i){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 Calendar c = Calendar.getInstance();  
	        c.add(Calendar.DATE, - i);  
	        Date monday = c.getTime();
	        String preMonday = sdf.format(monday);
//		 System.out.println(preMonday);
		return preMonday; 
	}
	public static void main(String[] args) {
//		getDateTimeByDays(1);
//		System.out.println(weeHourString(new Date(), 0));
//		System.out.println(DateUtils.getDayBeginTime(7));
//		System.out.println(getDateTimeBetweenMonth(12));
		System.out.println(DateUtils.weeHourString(new Date(),0));
	}
/**
 * @方法功能描述：获取当前时间的天数
 * @特殊参数描述：
 * @创建人：QXO7129
 * @创建时间：Dec 17, 2016 11:38:43 AM
 */ 
public static int getMounthDateTimeByNow(){
	//获取当前时间  
    Calendar cal = Calendar.getInstance();  
    //下面可以设置月份，注：月份设置要减1，所以设置1月就是1-1，设置2月就是2-1，如此类推  
    cal.set(Calendar.MONTH, 1-1);  
    //调到上个月  
    cal.add(Calendar.MONTH, -1);  
    //得到一个月最最后一天日期(31/30/29/28)  
    int MaxDay=cal.getActualMaximum(Calendar.DAY_OF_MONTH);  
	return MaxDay;
}
/**
 * @方法功能描述：获取指定月份后的日期
 * @特殊参数描述：
 * @创建人：QXO7129
 * @创建时间：Dec 17, 2016 11:38:59 AM
 */ 
public static String getDateTimeBetweenMonth(int monthes){
	Date date = new Date();//当前日期
	SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Calendar calendar = Calendar.getInstance();//日历对象
	calendar.setTime(date);//设置当前日期
	calendar.add(Calendar.MONTH, -monthes);//月份减一
	return f.format(calendar.getTime());
	
} 
	/**
	 * 获取当年的最后一天
	 * 
	 * @return
	 */
	public static Date getCurrentYearLastDate() {
		Calendar currCal = Calendar.getInstance();
		int currentYear = currCal.get(Calendar.YEAR);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, currentYear);
		calendar.roll(Calendar.DAY_OF_YEAR, -1);
		Date currYearLast = calendar.getTime();
		return currYearLast;
	}
	
	/**
	 * @方法功能描述：获取i天前的开始时间
	 * @特殊参数描述：
	 * @创建人：QXO7129
	 * @创建时间：Jan 11, 2017 10:47:24 AM
	 */ 
	public static String getDayBeginTime(int i){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 Calendar calendar = Calendar.getInstance();
		    calendar.add(Calendar.DATE, - i); 
		    calendar.set(Calendar.HOUR_OF_DAY, 0);
		    calendar.set(Calendar.MINUTE, 0);
		    calendar.set(Calendar.SECOND, 0);
		    Date start = calendar.getTime();
//		    System.out.println(sdf.format(start));
		    return sdf.format(start);
	}
	
	/**
	 *  获取过去的多长时间
	 *  time=Calendar.**,num=-*
	 *  过去的几个小时、日期
	 * @return
	 */

	public static String getUpperTimeString(int time, int num) {
		SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		Date date = new Date(System.currentTimeMillis());
		calendar.setTime(date);
		calendar.add(time, num);
		date = calendar.getTime();
		return f.format(date);

	}
	
	/**
	 *  flag 0获取当天凌晨时间
	 *  flag 1获取今天最末时间
	 *  flag 2获取昨天开始时间
	 *  flag 其它值获取当前系统时间
	 * @return
	 */
	
	public static String weeHourString(Date date, int flag) {
		SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		long millisecond = hour * 60 * 60 * 1000 + minute * 60 * 1000 + second
				* 1000;
		if (flag == 0) {
			cal.setTimeInMillis(cal.getTimeInMillis() - millisecond);
			return f.format(cal.getTime());
		} else if (flag == 1) {
			cal.setTimeInMillis(cal.getTimeInMillis() - millisecond);
			cal.setTimeInMillis(cal.getTimeInMillis() + 23 * 60 * 60 * 1000
					+ 59 * 60 * 1000 + 59 * 1000);
		} else if (flag == 2) {
			long millisecond2 = 24 * 60 * 60 * 1000;
			cal.setTimeInMillis(cal.getTimeInMillis() - millisecond
					- millisecond2);
		}
		return f.format(cal.getTime());
	}
	
	
	/**
	 *  获取周一日起
	 *  n=0获取当周周一日期
	 *  大于0的正整数，是几就向下推几周
	 *  小于0的正整数，是几就向上推几周
	 * @return
	 */
	public static String getNowWeekMonday(int flag) {
		SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Calendar currentDate = Calendar.getInstance();
	    currentDate.setFirstDayOfWeek(Calendar.MONDAY);
	    currentDate.set(Calendar.HOUR_OF_DAY, 0);
	    currentDate.set(Calendar.MINUTE, 0);
	    currentDate.set(Calendar.SECOND, 0);
	    currentDate.set(Calendar.MILLISECOND, 0);
	    currentDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
	    Date date=currentDate.getTime();
	    if(flag == 0){
	    	 return f.format(date.getTime());
	    }else if (flag == 1) {
	    	return f.format(date.getTime() - 7 * 24 * 60 * 60 * 1000);
		}
		return null;
	}
	
	 
	/**
	 *  获取最近月份的第*天
	 *  n=0获取上月最后一天
	 *  大于0的正整数，是几就从1号向下向下推几天
	 *  小于0的正整数，是几就从上月最后一天向上推几天
	 * @return
	 */
	public static String getMondayForstDay(int n) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();    
		c.set(Calendar.HOUR_OF_DAY, 0);
	    c.set(Calendar.MINUTE, 0);
	    c.set(Calendar.SECOND, 0);
	    c.set(Calendar.MILLISECOND, 0);
		c.add(Calendar.MONTH, 0);
		//设置为1号,当前日期既为本月第一天 
		c.set(Calendar.DAY_OF_MONTH,n);
		String first = format.format(c.getTime());
		return first;
	}
	
	
	
	   /**
	     * 获取前半年的开始时间
	     * @return
	     */
    
    public  static String getHalfYearStartTimeString(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int currentMonth = c.get(Calendar.MONTH) + 1;
        Date now = null;
        try {
            if (currentMonth >= 1 && currentMonth <= 6){
                c.set(Calendar.MONTH, 0);
            }else if (currentMonth >= 7 && currentMonth <= 12){
                c.set(Calendar.MONTH, 6);
            }
            c.set(Calendar.DATE, 1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return longSdf.format(now);
        
    }
    
    
    /**
     * 获取后半年的结束时间
     * @return
     */
    
    public static  String getHalfYearEndTimeString(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int currentMonth = c.get(Calendar.MONTH) ;
        Date now = null;
        try {
            if (currentMonth >= 0 && currentMonth <= 6){
                c.set(Calendar.MONTH, 5);
                c.set(Calendar.DATE, 30);
            }else if (currentMonth >= 7 && currentMonth <= 12){
                c.set(Calendar.MONTH, 11);
                c.set(Calendar.DATE, 31);
            }
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return longSdf.format(now);
    }
    
    /**
     * 当前年的开始时间，即2016-01-01 00:00:00
     *int 0当前年的开始时间   6后半年的开始时间
     * @return
     */
    
    public static  String getCurrentYearStartTimeString(int m) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = null;
        try {
            c.set(Calendar.MONTH, m);
            c.set(Calendar.DATE, 1);
            now = shortSdf.parse(shortSdf.format(c.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return f.format(now);
    } 
    /**
     * @方法功能描述：时间就近取整:<=30分向前取整,>30分向后取整
     * @特殊参数描述：
     * @创建人：QXO7129
     * @创建时间：Dec 6, 2016 9:22:32 AM
     */ 
    	public static String getInitialTime(String time){
    	String hour="00";//小时
    	String minutes="00";//分钟
    	String outTime="00:00";
    	StringTokenizer st = new StringTokenizer(time, ":");
    	        List<String> inTime = new ArrayList<String>(); 
    	        while (st.hasMoreElements()) { 
    	        inTime.add(st.nextToken()); 
    	        }
    	        hour=inTime.get(0).toString();
    	        minutes=inTime.get(1).toString();
    	if(Integer.parseInt(minutes)>=30){
//    	 hour=(Integer.parseInt(hour)+1)+"";
    	 minutes=":30";
    	}   else {
    		 minutes=":00";
		}     
    	outTime=hour+minutes;
    	SimpleDateFormat sdf=new SimpleDateFormat("HH:mm");

    	try {
    	outTime=sdf.format(sdf.parse(outTime));
    	} catch (ParseException e) {
    	e.printStackTrace();
    	}
    	return outTime;
    	}
    	/**
		 * 
		 * @方法功能描述:时间间隔是否满一年
		 * @特殊参数描述：
		 * @创建人：QXO7127
		 * @创建时间：Mar 16, 2017 2:06:37 PM
		 * @修改人：QXO7127
		 * @修改时间：Mar 16, 2017 2:06:37 PM
		 * @修改备注：
		 */ 

    	public static boolean isOneYear(Date start, Date end) {
    		if (start==null||end==null) {
    			return false;
			}else {
			
    			Calendar startday = Calendar.getInstance();
    			Calendar endday = Calendar.getInstance();
    			startday.setTime(start);
    			endday.setTime(end);
    			if (startday.after(endday)) {
    				return false;
    			}
    			long sl = startday.getTimeInMillis();
    			long el = endday.getTimeInMillis();
    			long days = ((el - sl) / (1000 * 60 * 60 * 24));
    			if (days <= 366) {
    				return false;
    			} else {
    				return true;
    			}
    			}
    	}

	/**
	 * 
	 * @方法功能描述:时间间隔是否满固定的时间间隔
	 * @特殊参数描述：interval/时间间隔
	 * @创建人：QXO7127
	 * @创建时间：Mar 16, 2017 2:06:37 PM
	 * @修改人：QXO7127
	 * @修改时间：Mar 16, 2017 2:06:37 PM
	 * @修改备注：
	 */
	public static boolean isInterval(Date start, Date end, int interval) {
		if (start == null || end == null) {
			return false;
		} else {

			Calendar startday = Calendar.getInstance();
			Calendar endday = Calendar.getInstance();
			startday.setTime(start);
			endday.setTime(end);
			if (startday.after(endday)) {
				return false;
			}
			long sl = startday.getTimeInMillis();
			long el = endday.getTimeInMillis();
			long days = ((el - sl) / (1000 * 60 * 60 * 24));
			if (days < interval) {
				return false;
			} else {
				return true;
			}
		}
	}
}
