package com.bmw.ssa.utils;

/**
 * Created by Administrator on 2019/8/30 0030.
 */
public enum KeyAccountAsaEnum {
    get57Value(57,63),
    get94Value(94,112),
    get96Value(96,113);


    private Integer key;
    private Integer value;



    KeyAccountAsaEnum(int key, int value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
    public static int getvalue(int key) {
        for (KeyAccountAsaEnum k : KeyAccountAsaEnum.values()) {
            if (k.getKey()== key) {
                return k.getValue();
            }          }
        return 0;
    }

}
