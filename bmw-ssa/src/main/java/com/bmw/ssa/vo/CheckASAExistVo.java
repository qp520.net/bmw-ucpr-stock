package com.bmw.ssa.vo;


import lombok.Data;

/**
 * @类描述：判断机构协议是否存在传入VO
 * @创建人： QXO7127
 * @创建时间：Dec 6, 2016 11:22:38 AM
 * @修改人： QXO7127
 * @修改时间：Dec 6, 2016 11:22:38 AM
 * @修改备注：
 */
@Data
public class CheckASAExistVo {

	private String kasaId;

	private String dealerId;

	private String languageFlag="8";
	
	private String typ;
	
	private String year;
	
	private String name;
	
	private String renewx;


}
