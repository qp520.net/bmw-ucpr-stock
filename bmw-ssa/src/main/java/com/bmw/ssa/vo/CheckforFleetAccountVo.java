package com.bmw.ssa.vo;

import com.bmw.core.database.po.BasePO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Administrator on 2019/8/16 0016.
 */
@Data
@ApiModel(value = "检查是否注册的参数")
public class CheckforFleetAccountVo {
    @ApiModelProperty(value = "公司中文名",notes = "",example = "abc")
    private String companyCN;

    @ApiModelProperty(value = "国际化标识",example = "1")
    private int languageFlag=8;
    @ApiModelProperty(value = "经销商类型")
    private int parameter_L = 1;
}
