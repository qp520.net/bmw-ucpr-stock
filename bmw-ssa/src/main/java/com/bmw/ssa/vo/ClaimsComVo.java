package com.bmw.ssa.vo;
import lombok.Data;

import java.util.Date;

/**
 * Project：BMW SSA Web Base System：SSA Sub System：fleetAccount xxxxx(描述)
 * 
 * @author lijiwei
 * 
 */
@Data
public class ClaimsComVo {
	private String is_required;
	private String idNo;
	private String bonus;
	private String bonusRMB;
	private String licenses;

	public String getIs_required() {
		return is_required;
	}

	public void setIs_required(String is_required) {
		this.is_required = is_required;
	}

	// search area:Search
	private String companyCn;
	private String seriesNo;
	private String status;
	private String fleetAccNo;
	private String dmsCustNo;
	private String nsaBba;
	private String invoiceName;
	private String vin;
	private String taYear;
	private String vin7;

	// search area:Advanced
	private String category;
	private String asaType;
	private String dealer;
	private String region;
	private String claimsPerAsaDrop;
	private String claimsPerAsaText;
	private boolean dealerChange;
	private boolean pmAmountExist;

	// search area:Dates
	private Date retailDateFrom;
	private Date retailDateTo;
	private Date dealerapplyFrom;
	private Date dealerapplyTo;
	private Date claimsApprovalFrom;
	private Date claimsApprovalTo;
	private String claimVRDate;

	// dropDown用
	private String key;
	private String value;

	// 其他
	private String customerId;
	private String language;
	private String userId;
	private String department;

	// [Process Claimes]页面用
	private String stickerPrice;
	private String plusAmount;
	private String minusAmount;
	private String bmwSubs;
	private String active;
	private String docSeq;
	private String docId;
	private String docR;
	private int butFlag;
	private String businessType;
	private String orderKey;
	private String specialCaseNo;
	private String userRoll;
	private String asaCode;

	private String nextSeq;
	private String userGroupName;
	
	private int firstRecord;
	private int lastRecord;

}
