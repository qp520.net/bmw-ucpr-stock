package com.bmw.ssa.vo;

import com.bmw.core.database.po.BasePO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * Created by Administrator on 2019/8/20 0020.
 */
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
public class InsuranceSearchConditionVo extends BasePO {
    /** 机构全称 */
    @ApiModelProperty(value = "机构全称")
    private String companyCN;

    /** 客户姓名 */
    @ApiModelProperty(value = "客户姓名")
    private String nameCN;
    @ApiModelProperty(value = "idNo",required = true)
    private String idNo;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private String status;

    /** 经销商 */
    @ApiModelProperty(value = "经销商")
    private String dealer;

    /** 区域 */
    @ApiModelProperty(value = "区域")
    private String region;
    /** 创建日期 From */
    @ApiModelProperty(value = "创建日期 from")
    private Date creationDateFrom;

    /** 创建日期 To */
    @ApiModelProperty(value = "创建日期 to")
    private Date creationDateTo;

    /** 激活日期 From */
    @ApiModelProperty(value = "激活日期from")
    private Date activationDateFrom;

    /** 激活日期 To */
    @ApiModelProperty(value = "激活日期To")
    private Date activationDateTo;

    /** 申请日期 From */
    @ApiModelProperty(value = "申请日期from")
    private Date applicationDateFrom;

    /** 申请日期 To */
    @ApiModelProperty(value = "申请日期To")
    private Date applicationDateTo;
    /** 顾客ID */
    private String customer;
    /** 国际化标识 */
    @ApiModelProperty(value = "国际化标识",required = true)
    private String language;

    /** 查询标识*审批*/
    private String approval;
    /** 审批时传入审批人账号 */
    private String userName;
    /** 查询标识*待办事宜*/
    private String toDoAction;
    @ApiModelProperty(value = "第几页",example = "1")
    private int pageNumber =1;
    @ApiModelProperty(value = "条数",example = "10")
    private int pageSize=10;


}
