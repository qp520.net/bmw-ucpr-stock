package com.bmw.ssa.vo;

import lombok.Data;

/**
 * @Project: BMW SSA Web Base
 * @System: SSA
 * @SubSystem: Key Account ASA
 * @Description: 机构协议申请详细VO
 * @author: andh
 */
@Data
public class KeyAccountAsaDetailVo{

	private String kasaId;

	private String dealerId;

	private String dealerName;

	private String languageFlag ="8";

	private String ogKasaId;
}
