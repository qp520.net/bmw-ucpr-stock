package com.bmw.ssa.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Project: BMW SSA Web Base
 * @System: SSA
 * @SubSystem: Key Account ASA
 * @Description: 机构协议申请检索条件VO
 * @author: andh
 */
@Data
@ApiModel
public class KeyAccountAsaSearchConditionVo {
	/** 机构全称 */
	@ApiModelProperty(value = "机构全称")
	private String institutionCns;
	/** 机构全称-大写 */
	@ApiModelProperty(value = "机构全程大写")
	private String institutionCnsUpper;

	/** 客户分类 */
	@ApiModelProperty(value = "客户分类")
	private String customerCategory;

	/** 状态 */
	@ApiModelProperty(value = "状态")
	private String status;

	/** 经销商 */
	@ApiModelProperty(value = "经销商")
	private String dealer;

	/** 区域 */
	@ApiModelProperty(value = "区域")
	private String region;

	/** 创建日期 From */
	@ApiModelProperty(value = "创建时间From")
	private Date creationDateFrom;

	/** 创建日期 To */
	@ApiModelProperty(value = "创建时间To")
	private Date creationDateTo;

	/** 激活日期 From */
	@ApiModelProperty(value = "激活时间From")
	private Date activationDateFrom;

	/** 激活日期 To */
	@ApiModelProperty(value = "激活时间TO")
	private Date activationDateTo;

	/** 申请日期 From */
	@ApiModelProperty(value = "申请时间From")
	private Date applicationDateFrom;

	/** 申请日期 To */
	@ApiModelProperty(value = "申请时间TO")
	private Date applicationDateTo;

	/** 顾客ID */
	@ApiModelProperty(value = "顾客ID")
    private String customer;
    
    /** 国际化标识 */
	@ApiModelProperty(value = "国际化")
    private String language ="8";
    
    private String renew;
    
    private String year;
	@ApiModelProperty(value = "页号")
	private int pageNumber =1;
	@ApiModelProperty(value = "条数")
	private int pageSize=10;



}
