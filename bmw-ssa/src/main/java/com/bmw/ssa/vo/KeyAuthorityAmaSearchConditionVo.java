package com.bmw.ssa.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @类描述：机构市场活动报告 搜索条件
 * @创建人： QXO7127
 * @创建时间：Apr 19, 2017 5:36:23 PM
 * @修改人： QXO7127
 * @修改时间：Apr 19, 2017 5:36:23 PM
 * @修改备注：
 */ 
@Data
public class KeyAuthorityAmaSearchConditionVo {
	/** 机构全称 */
	private String institutionCns;

	/** 客户分类 */
	private String customerCategory;

	/** 状态 */
	private String status;

	/** 经销商 */
	private String dealer;

	/** 区域 */
	private String region;
	
	/** 创建日期 From */
	private Date creationDateFrom;

	/** 创建日期 To */
	private Date creationDateTo;

	/** 激活日期 From */
	private Date activationDateFrom;

	/** 激活日期 To */
	private Date activationDateTo;

	/** 申请日期 From */
	private Date applicationDateFrom;

	/** 申请日期 To */
	private Date applicationDateTo;

	/** 顾客ID */
    private String customer;
    
    /** 国际化标识 */
    private String language;
    /** 查询标识 */
    private String approval;
    /** 审批时传入审批人账号 */
    private String userName;
	private int pageNumber =1;
	private int pageSize=10;

	
}
