package com.bmw.ssa.vo;

import lombok.Data;

/**
 * Created by Administrator on 2019/8/21 0021.
 */
@Data
public class LevelVo {
    private int currentLevel;
    private int allLevel;
    private String referenceId;

    private String cat;
    private String cot;
    private String dealerId;
    private String variantCode;
    private int claimLimitMin;
    private int claimLimitMax;
    private String supplier;
    private String auditId;
    private String anaCode;
    private String status;
    private String accountId;
    private String asaId;

    private String appRole;
}
