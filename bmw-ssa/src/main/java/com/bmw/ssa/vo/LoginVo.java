package com.bmw.ssa.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by Administrator on 2019/8/15 0015.
 */
@ApiModel
@Data
public class LoginVo {
    @ApiModelProperty(value = "用户名",dataType = "String")
    private String user_name;
    @ApiModelProperty(value = "密码",dataType = "String")
    private String password;
}
