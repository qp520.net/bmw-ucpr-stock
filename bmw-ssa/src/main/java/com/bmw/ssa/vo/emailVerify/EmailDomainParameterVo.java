/**
 * 
 */
package com.bmw.ssa.vo.emailVerify;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* Project：BMW SSA Web Base
* System：SSA
* Sub System：fleetAccount
* E-Mail parameter
* @author neusoft
* 
*/
@Data
@ApiModel
public class EmailDomainParameterVo {
	
	// QXO7127 May 4, 2018 10:59:28 AM  页面标签
	@ApiModelProperty(hidden = true)
	private String keyWord;
	/**
	 * Dealer ID
	 */
	@ApiModelProperty(value = "经销商id")
	private String dealerId;
	
	/**
	 * Account ID
	 */
	@ApiModelProperty(value = "公司编号")
	private String accountId;
	
	/**
	 * Status
	 */
	@ApiModelProperty(value = "状态")
	private String status;
	
	/**
	 * language
	 */
	@ApiModelProperty(value = "国际化标识，不填则默认为8")
	private int language=8;
	
	/**
	 * domainId
	 */
	@ApiModelProperty(value = "domainId")
    private String domainId;
	
    /**
	 * domainName
	 */
	@ApiModelProperty(value = "domainName")
	private String domainName;
	
	/**
	 * customerId
	 */
	@ApiModelProperty(value = "顾客（车主）编号")
	private String customerId;
	
	/**
	 * asaId
	 */
	@ApiModelProperty(value = "asaid")
	private String asaId;

}
