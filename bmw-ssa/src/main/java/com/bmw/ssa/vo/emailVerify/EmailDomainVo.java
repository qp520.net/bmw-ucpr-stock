/**
 * 
 */
package com.bmw.ssa.vo.emailVerify;

/**
* Project：BMW SSA Web Base
* System：SSA
* Sub System：fleetAccount
* E-Mail Vo
* @author neusoft
* 
*/
public class EmailDomainVo  {
	
	/** 
	 * rowKey 
	 */
	private String rowKey;
	/** 
	 * domainId 
	 */
	private String domainId;
	
	/** 
	 * domainName 
	 */
	private String domainName;
	
	/** 
	 * asaNo
	 */
	private String asaNo;
	
	/** 
	 * fileOuloadInfo
	 */
	private String fileOuloadInfo;
	
	/**
	 * Dealer ID
	 */
	private String dealerId;
	
	/**
	 * Account ID
	 */
	private String accountId;
	
	/**
	 * create user
	 */
	private String createUser;
	
	/**
	 * asaId
	 */
	private String asaId;
	
	/**
	 * customerId
	 */
	private String customerId;

	
	// QXO7127 Apr 11, 2018 10:09:42 AM 放大镜是否显示
	private boolean viewShow;
	// QXO7127 Apr 11, 2018 10:09:42 AM 下载是否显示
	private boolean downShow;
	// QXO7127 Apr 11, 2018 10:09:42 AM 上传是否显示
	private boolean doUpShow;
	// QXO7127 Apr 11, 2018 10:12:10 AM 文件值
	private byte[] docContent;
	// QXO7127 Apr 11, 2018 10:12:25 AM 文件名称
	private String docFilename;
	// QXO7127 Apr 11, 2018 10:12:34 AM 文件类型
	private String docMimeType;
	
	public String getDomainId() {
		return domainId;
	}

	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getAsaNo() {
		return asaNo;
	}

	public void setAsaNo(String asaNo) {
		this.asaNo = asaNo;
	}

	public String getFileOuloadInfo() {
		return fileOuloadInfo;
	}

	public void setFileOuloadInfo(String fileOuloadInfo) {
		this.fileOuloadInfo = fileOuloadInfo;
	}
	
	/**
	 * @return the dealerId
	 */
	public String getDealerId() {
		return dealerId;
	}

	/**
	 * @param dealerId the dealerId to set
	 */
	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getRowKey() {
		return rowKey;
	}

	public void setRowKey(String rowKey) {
		this.rowKey = rowKey;
	}

	public String getAsaId() {
		return asaId;
	}

	public void setAsaId(String asaId) {
		this.asaId = asaId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public boolean isViewShow() {
		return viewShow;
	}

	public void setViewShow(boolean viewShow) {
		this.viewShow = viewShow;
	}

	public boolean isDownShow() {
		return downShow;
	}

	public void setDownShow(boolean downShow) {
		this.downShow = downShow;
	}

	public boolean isDoUpShow() {
		return doUpShow;
	}

	public void setDoUpShow(boolean doUpShow) {
		this.doUpShow = doUpShow;
	}

	public byte[] getDocContent() {
		return docContent;
	}

	public void setDocContent(byte[] docContent) {
		this.docContent = docContent;
	}

	public String getDocFilename() {
		return docFilename;
	}

	public void setDocFilename(String docFilename) {
		this.docFilename = docFilename;
	}

	public String getDocMimeType() {
		return docMimeType;
	}

	public void setDocMimeType(String docMimeType) {
		this.docMimeType = docMimeType;
	}
	
	
}
