package com.bmw.ssa.vo.emailVerify;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
* Project：BMW SSA Web Base
* System：SSA
* Sub System：fleetAccount
* E-Mail parameter
* @author neusoft
* 
*/
@ApiModel
public class EmailExistVerifyParameterVo {
	/**
	 * 邮箱地址
	 */
	@ApiModelProperty(value = "邮箱")
	private String mail;
	@ApiModelProperty(value = "身份证")
	private String idNo;
	/**
	 * 邮箱地址(@xxx.com)
	 */
	@ApiModelProperty(value = "")
	private String suffix;

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the suffix
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * @param suffix the suffix to set
	 */
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
}
