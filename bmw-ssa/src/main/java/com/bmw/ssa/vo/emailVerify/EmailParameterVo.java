/**
 * 
 */
package com.bmw.ssa.vo.emailVerify;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author UASD-030
 *
 */
@ApiModel
public class EmailParameterVo {
	/** language */
	@ApiModelProperty(value = "国际化标志")
	protected String language;
	
	/** customer Id */
	@ApiModelProperty(value = "当前登录用户id")
	protected String customerId;
	
	/** username */
	@ApiModelProperty(value = "用户名")
	protected String userName;

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
