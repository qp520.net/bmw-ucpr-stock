/**
 * 
 */
package com.bmw.ssa.vo.emailVerify;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
* Project：BMW SSA Web Base
* System：SSA
* Sub System：fleetAccount
* E-Mail parameter
* @author neusoft
* 
*/
@Data
@ApiModel
public class EmployeeParameterVo extends EmailParameterVo  {
	/**
	 * Dealer Account ID
	 */
	@ApiModelProperty
	private String dealerAccountId;

	@ApiModelProperty(value = "身份证号")
	private String idNo;
	/**
	 * Customer Name
	 */
	@ApiModelProperty(value = "客户姓名")
	private String customerName;
	
	/**
	 * Mail Address
	 */
	@ApiModelProperty(value = "邮箱")
	private String mailAddress;
	
	/**
	 * Verify Date (From)
	 */

	private Date verifyDateFrom;
	
	/**
	 * Verify Date (To)
	 */

	private Date verifyDateTo;
	
	/**
	 * Verify Date (From)
	 */
	@ApiModelProperty(value = "验证日期 From")
	private String verifyStrFrom;
	
	/**
	 * Verify Date (To)
	 */
	@ApiModelProperty(value = "验证日期 To")
	private String verifyStrTo;
	
	/**
	 * SQL execute class
	 * A: 员工姓名里输入 fleet only时(全检索)
	 * B: 当从大客户账户搜索一览中选中一条记录时
	 * C: 当只输入客户姓名，邮箱等条件时
	 */

	private String handleClass;

	/**
	 * @return the dealerAccountId
	 */

	@ApiModelProperty(value = "页号",example = "1")
	private int pageNum=1;
	@ApiModelProperty(value = "条数",example = "10")
	private int pageSize=10;

	@ApiModelProperty(value = "经销商")
	private String dealerId;

	@ApiModelProperty(value = "用户id")
	private String accountId;

}
