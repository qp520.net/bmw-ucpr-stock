package com.bmw.ssa.vo.emailVerify;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* Project：BMW SSA Web Base
* System：SSA
* Sub System：fleetAccount
* E-Mail vo
* @author neusoft
* 
*/
@Data
@ApiModel
public class FleetAccountsInfoVo{
	/** Dealer Account ID */
	@ApiModelProperty(value = "")
	private String dealerAccountId;
	
	/** Status */
	@ApiModelProperty(value = "状态")
	private String status;
	
	/** Status Name */
	@ApiModelProperty(value = "",hidden = true)
	private String statusName;
	
	/** Dealer ID */
	@ApiModelProperty(value = "经销商id")
	private String dealerId;
	
	/** Dealer Name */
	@ApiModelProperty(value = "经销商名称")
	private String dealerName;
	
	/** Customer Category */
	@ApiModelProperty(value = "客户类别")
	private String customerCategory;
	
	/** Company Name */
	@ApiModelProperty(value = "公司全程")
	private String companyCn;
	
	/** Fleet Account No */
	@ApiModelProperty(value = "大客户编号")
	private String fleetAccountNo;

}
