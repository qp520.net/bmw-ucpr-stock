/**
 * 
 */
package com.bmw.ssa.vo.emailVerify;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* Project：BMW SSA Web Base
* System：SSA
* Sub System：fleetAccount
* E-Mail parameter
* @author neusoft
* 
*/
@Data
@ApiModel
public class FleetAccountsParameterVo {
	/**
	 * Company Name
	 */
	@ApiModelProperty(value = "公司名称")
	private String companyName;
	
	/**
	 * Fleet Account No
	 */
	@ApiModelProperty(value = "大客户编号")
	private String fleetAccountNo;

	@ApiModelProperty(value = "页号")
	private int pageNum =1;
	@ApiModelProperty(value = "条数")
	private int pageSize=10;
	@ApiModelProperty(value = "国际化标识")
	private String language="8";


}
