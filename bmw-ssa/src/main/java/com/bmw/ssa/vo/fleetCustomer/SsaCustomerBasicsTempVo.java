package com.bmw.ssa.vo.fleetCustomer;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * Created by Administrator on 2019/9/24 0024.
 */
@Data
@ApiModel
public class SsaCustomerBasicsTempVo extends BaseRowModel{
    // QXO7127 Jul 27, 2017 3:28:24 PM  bba编号
    @ApiModelProperty(value = "bba编号")
    @ExcelProperty(index = 0,value = "经销商编号（CBU)")
    private String cbuDea;
    // QXO7127 Jul 27, 2017 3:35:52 PM  职位
    @ApiModelProperty(value = "职位")
    @ExcelProperty(index = 1,value = "经销商职位")
    private String userPosition;
    @ApiModelProperty(value = "姓名")
    @ExcelProperty(index = 2,value = "姓名")
    private String userName;
    // QXO7127 Jul 27, 2017 3:35:30 PM  固话
    @ApiModelProperty(value = "座机")
    @ExcelProperty(index = 3,value = "座机")
    private String userMobile;
    // QXO7127 Jul 27, 2017 3:35:37 PM  手机
    @ApiModelProperty(value = "手机")
    @ExcelProperty(index = 4,value = "手机")
    private String userPhone;
    @ApiModelProperty(value = "邮箱")
    @ExcelProperty(index = 5,value = "邮箱")
    private String userMail;
    // QXO7127 Jul 27, 2017 3:35:43 PM  加入时间 (String)
    @ApiModelProperty(value = "加入时间")
    @ExcelProperty(index = 6,value = "加入宝马大客户时间")
    private String accessionTime;
}
