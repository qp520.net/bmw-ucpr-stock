package com.bmw.ssa.vo.fleetCustomer;

import com.bmw.ssa.entity.DropList;
import com.bmw.ssa.po.KeyAccountAsa.RegionPo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
/**
 * @类描述：客户基础信息-左侧搜索条件Vo
 * @创建人： QXO7127
 * @创建时间：Jul 27, 2017 3:23:18 PM
 * @修改人： QXO7127
 * @修改时间：Jul 27, 2017 3:23:18 PM
 * @修改备注：
 */
@ApiModel
@Data
public class SsaCustomerMainSearchVo{
    /** 经销商List */
    @ApiModelProperty(value = "经销商List")
    private List<DropList> dealerList =  new ArrayList<DropList>();
    /** 区域List */
    @ApiModelProperty(value = "区域List")
    private List<RegionPo> regionList =  new ArrayList<RegionPo>();
    /** 职位List */
    @ApiModelProperty(value = "职位List")
    private List<DropList> positionList =  new ArrayList<DropList>();
    /** 年份List */
    @ApiModelProperty(value = "年份List")
    private List<DropList> yearList =  new ArrayList<DropList>();
    /** 经销商 */
    @ApiModelProperty(value = "经销商")
    private String dealer ;
    /** 区域 */
    @ApiModelProperty(value = "区域")
    private String region;
    /** 职位 */
    @ApiModelProperty(value = "职位")
    private String position;
    /** 年份 */
    @ApiModelProperty(value = "年份")
    private String year;
    /** 语言类型 */
    @ApiModelProperty(value = "语言类型")
    private String languageFlag ="8";
    /** 输入经销商List */
    @ApiModelProperty(value = "输入经销商List")
    private List<DropList> dealerNoList = new ArrayList<DropList>();
    /** 间隔年份 */
    @ApiModelProperty(value = "间隔年份")
    private String intervalYear;

    @ApiModelProperty(value = "页号")
    private int pageNum = 1;
    @ApiModelProperty(value = "条数")
    private int pageSize = 10;



}