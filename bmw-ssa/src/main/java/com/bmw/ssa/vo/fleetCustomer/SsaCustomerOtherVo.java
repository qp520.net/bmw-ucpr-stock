package com.bmw.ssa.vo.fleetCustomer;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by Administrator on 2019/9/24 0024.
 */
@Data
@ApiModel
public class SsaCustomerOtherVo extends BaseRowModel{
    @ApiModelProperty(value = "经销商编码")
    @ExcelProperty(index = 0,value = "经销商编码")
    private String cbuDea;
    @ApiModelProperty(value = "年份")
    @ExcelProperty(index = 1,value = "年份")
    private String year;
    @ApiModelProperty(value = "经销商职位")
    @ExcelProperty(index = 2,value = "经销商职位")
    private String userPosition;
    // QXO7127 Jul 27, 2017 3:26:50 PM  资质认证
    @ApiModelProperty(value = "大客户经理认证")
    @ExcelProperty(index = 3,value = "大客户经理认证")
    private String aptitude;

    private int type =1;

    @ApiModelProperty(value = "页号")
    private int pageNum= 1;
    @ApiModelProperty(value = "条数")
    private int pageSize = 10;
}
