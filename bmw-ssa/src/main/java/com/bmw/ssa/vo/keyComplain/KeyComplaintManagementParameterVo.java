package com.bmw.ssa.vo.keyComplain;

//import java.util.Date;


import lombok.Data;

/**
* Project：BMW SSA Web Base
* System：SSA
* Sub System：keyAccount
* 客户投诉/咨询 parameterVo
* @author liangqs
* 
*/
@Data
public class KeyComplaintManagementParameterVo{

	/** ID */
	private String id;

	/** 语言 */
	private String language;

	/** USERTYPE */
	private String userType;

	/** DEALERID */
	private String dealerid;
	
	/** CORPORATION */
	private String corporation;
	
	/** CORPORATION NAME */
	private String corporationName;
	
	/** FOLLOWUP_DEALER */
	private String followupDealer;
	
	/** COMPLAINT_ID */
	private String complaintId;

	/** User-number ~c */
	private String userNumber;

	/** Username ~u */
	private	String userName;

	/** STATUS */
	private	String status;

	/** SOLUTION */
	private String solution;
	
	/** Priority */
	private String priority;
	
	/** Complaint Source */
	private String complaintSource;
	
	/** Complaint Source */
	private String complaintType;
	
	/** Start Date From */
	private String startDateFrom;

	/** Start Date To */
	private String startDateTo;
	
	/** Due Date From */
	private String dueDateFrom;

	/** Due Date To */
	private String dueDateTo;
	
	/** Actual Date From */
	private String actualDateFrom;

	/** Actual Date To */
	private String actualDateTo;
	
	/** Create Date From */
	private String createDateFrom;

	/** Create Date To */
	private String createDateTo;
	
	/** Last Action Date From */
	private String lastActionDateFrom;

	/** Last Action Date To */
	private String lastActionDateTo;

	private int pageNumber =1;
	private int pageSize=10;
	

}
