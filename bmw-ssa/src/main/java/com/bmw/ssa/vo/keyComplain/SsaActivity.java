package com.bmw.ssa.vo.keyComplain;

import lombok.Data;

import java.util.Date;
@Data
public class SsaActivity {
	private	String	ACT_ID	;
	private	String	CREATEDBY	;
	private	Date	PLANDATE	;
	private	Date	CREATED_DATE	;
	private	String	ACTIVITY_TYPE	;
	private	String	CONTACT_TYPE	;
	private	Date	CLOSE_DATE	;
	private	String	CLOSED_BY	;
	private	String	DEALERID	;
	private	String	ACCOUNTID	;
	private	String	CUSTOMERID	;
	private	String	ASSIGN_TO	;
	private	String	TEXT1	;
	private	String	TEXT2	;
	private	String	CONTACT_FOR	;
	private	String	IMPORTANT	;
	private	String	CONTACT_METHOD	;
	private	String	CHANGED_BY	;
	private	Date	CHANGED_DATE	;
	private	String	ACTID	;
	private	String	DELETED	;
	private	String	VARIANT_CODE	;
	private	Date	PLANED_DATE	;
	private	String	REFERENCE	;
	private	String	FOLLOW_UP_DEALER1	;
	private	String	ACTOR1	;
	private	String	FOLLOW_UP_DEALER2	;
	private	String	ACTOR2	;
	private	String	FOLLOW_UP_DEALER3	;
	private	String	ACTOR3	;
	private	String	FOLLOW_UP_DEALER4	;
	private	String	ACTOR4	;
	private	String	PLANED_TIME	;
	private	Date	ACTOR_DATE	;
	private	String	ACTOR_TIME	;
	private	String	CONTACT_PERSON	;
	private	String	STATUS	;
	private	String	DESCRIPTION	;
	private	String	ACT_TYPE	;
	private	String	FOLLOWUP_ACTID	;
	private	String	INVATATION_ID	;
	private	String	COMPLAINT_ID	;
	private	Date	LAST_ACTION_DATE	;
	private	String	MAIN_ACT	;
	private	String	SOURCE	;
	private	String	EVENT_TYPE	;
	private	String	TARGET_GROUP	;
	private	String	MARKET_TYPE	;
	private	String	NA	;


}
