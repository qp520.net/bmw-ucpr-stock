package com.bmw.ssa.vo.keyComplain;

import lombok.Data;

import java.util.Date;

@Data
public class SsaComplaint {
	private	String	ID	;
	private	String	CORPORATION	;
	private	String	CONTACT	;
	private	String	PRIORITY	;
	private	String	COMPLAINT_TYPE	;
	private	String	COMPLAINT_SOURCE	;
	private	String	COMPLAINT_DEALER	;
	private	Date	START_DATE	;
	private	Date	DUE_DATE	;
	private	Date	ACTUAL_DATE	;
	private	String	FOLLOWUP_DEALER	;
	private	String	ACTOR	;
	private	String	STATUS	;
	private	String	DESCRIPTION	;
	private	String	SOLUTION	;
	private	Date	CREATED_DATE	;
	private	String	CREATED_USER	;
	private	Date	LASTACTION_DATE	;
	private	String	DEALERID	;
	private	String	USERTYPE	;
	private	String	VIN	;
	private	String	COMP_TOPIC	;

}
