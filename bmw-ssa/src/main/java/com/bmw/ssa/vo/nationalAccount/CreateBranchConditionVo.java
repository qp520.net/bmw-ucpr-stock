package com.bmw.ssa.vo.nationalAccount;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * Project：BMW SSA Web Base
 * System：SSA
 * Sub System：Admin
 * 大客户-分公司sql参数vo
 * @author wangyuchen
 * 
 */
@ApiModel
@Data
public class CreateBranchConditionVo{
	
	private String dealerid;
	private String accountid;
	private String locale;

}
