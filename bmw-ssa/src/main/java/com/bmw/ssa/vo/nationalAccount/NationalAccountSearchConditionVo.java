package com.bmw.ssa.vo.nationalAccount;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Project: BMW SSA Web Base
 * @System: SSA
 * @SubSystem: National Account
 * @Description: 全国协议检索条件
 * @author: andh
 */
@ApiModel
public class NationalAccountSearchConditionVo {

	/** 登录的customerId */
	@ApiModelProperty(value = "登录的customerId")
	private String customerid;

	/** 国际化标记 */
	@ApiModelProperty(value = "国际化标记")
	private int en_ch_flag;
	@ApiModelProperty(value = "登录用户的用户名")
	private String user_name;
	@ApiModelProperty(value = "页号")
	private int pageNum=1;
	@ApiModelProperty(value = "条数")
	private int pageSize=10;

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the customerid
	 */
	public String getCustomerid() {
		return customerid;
	}

	/**
	 * @param customerid
	 *            the customerid to set
	 */
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}

	/**
	 * @return the en_ch_flag
	 */
	public int getEn_ch_flag() {
		return en_ch_flag;
	}

	/**
	 * @param en_ch_flag
	 *            the en_ch_flag to set
	 */
	public void setEn_ch_flag(int en_ch_flag) {
		this.en_ch_flag = en_ch_flag;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

}
