package com.bmw.ssa.vo.sdaTerminate;

import lombok.Data;

@Data
public class SDATerminateVo {

	private String sda_no;
	private String dealer_id;
	private String create_year;
	private String last_action_date;
	private String next_approver_role;
	private String next_approver_seq;
	private String status;
	
	private String accountid;
	private String sda_id;
	
	private String mtflag;
	
	private String last_action_user;
	
	// QXO7127 May 23, 2017 11:30:58 AM  终止文件
	private String fileType;
	private String fileName;
	private byte[] fileDate;
	// QXO7127 May 23, 2017 11:31:24 AM  终止原因
	private String reason;
	
	

	
}
