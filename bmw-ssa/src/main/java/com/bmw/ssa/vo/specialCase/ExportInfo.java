package com.bmw.ssa.vo.specialCase;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

/**
 * Created by Administrator on 2019/9/23 0023.
 */
@Data
public class ExportInfo extends BaseRowModel{
    @ExcelProperty(index = 0,value = "VIN")
    private String vinNo;
    private String scNo;
}
