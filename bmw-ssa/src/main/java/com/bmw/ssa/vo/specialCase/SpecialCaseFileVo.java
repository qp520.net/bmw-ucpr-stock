package com.bmw.ssa.vo.specialCase;

public class SpecialCaseFileVo {
	private String fileName1;
	private String dealer1;
	private String approver1;
	private	byte[] attach1;
	 
	private String fileName2;
	private String dealer2;
	private String approver2;
	private	byte[] attach2;
	
	private String fileName3;
	private String dealer3;
	private String approver3;
	private	byte[] attach3;
	
	private String fileName4;
	private String dealer4;
	private String approver4;
	private	byte[] attach4;
	
	private String fileName5;
	private String dealer5;
	private String approver5;
	private	byte[] attach5;
	
	private String scno;

	public String getFileName1() {
		return fileName1;
	}

	public void setFileName1(String fileName1) {
		this.fileName1 = fileName1;
	}

	public String getDealer1() {
		return dealer1;
	}

	public void setDealer1(String dealer1) {
		this.dealer1 = dealer1;
	}

	public String getApprover1() {
		return approver1;
	}

	public void setApprover1(String approver1) {
		this.approver1 = approver1;
	}

	public byte[] getAttach1() {
		return attach1;
	}

	public void setAttach1(byte[] attach1) {
		this.attach1 = attach1;
	}

	public String getFileName2() {
		return fileName2;
	}

	public void setFileName2(String fileName2) {
		this.fileName2 = fileName2;
	}

	public String getDealer2() {
		return dealer2;
	}

	public void setDealer2(String dealer2) {
		this.dealer2 = dealer2;
	}

	public String getApprover2() {
		return approver2;
	}

	public void setApprover2(String approver2) {
		this.approver2 = approver2;
	}

	public byte[] getAttach2() {
		return attach2;
	}

	public void setAttach2(byte[] attach2) {
		this.attach2 = attach2;
	}

	public String getFileName3() {
		return fileName3;
	}

	public void setFileName3(String fileName3) {
		this.fileName3 = fileName3;
	}

	public String getDealer3() {
		return dealer3;
	}

	public void setDealer3(String dealer3) {
		this.dealer3 = dealer3;
	}

	public String getApprover3() {
		return approver3;
	}

	public void setApprover3(String approver3) {
		this.approver3 = approver3;
	}

	public byte[] getAttach3() {
		return attach3;
	}

	public void setAttach3(byte[] attach3) {
		this.attach3 = attach3;
	}

	public String getFileName4() {
		return fileName4;
	}

	public void setFileName4(String fileName4) {
		this.fileName4 = fileName4;
	}

	public String getDealer4() {
		return dealer4;
	}

	public void setDealer4(String dealer4) {
		this.dealer4 = dealer4;
	}

	public String getApprover4() {
		return approver4;
	}

	public void setApprover4(String approver4) {
		this.approver4 = approver4;
	}

	public byte[] getAttach4() {
		return attach4;
	}

	public void setAttach4(byte[] attach4) {
		this.attach4 = attach4;
	}

	public String getFileName5() {
		return fileName5;
	}

	public void setFileName5(String fileName5) {
		this.fileName5 = fileName5;
	}

	public String getDealer5() {
		return dealer5;
	}

	public void setDealer5(String dealer5) {
		this.dealer5 = dealer5;
	}

	 

	public String getApprover5() {
		return approver5;
	}

	public void setApprover5(String approver5) {
		this.approver5 = approver5;
	}

	public byte[] getAttach5() {
		return attach5;
	}

	public void setAttach5(byte[] attach5) {
		this.attach5 = attach5;
	}

	public String getScno() {
		return scno;
	}

	public void setScno(String scno) {
		this.scno = scno;
	}
	
	
	 
}
