package com.bmw.ssa.vo.specialCase;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;
@ApiModel
public class SpecialCaseVo {
	@ApiModelProperty(value = "特殊审批项编号")
	private String scno;
	@ApiModelProperty(value = "状态")
	private String scstatus;
	@ApiModelProperty(value = "类型：VIN OR ASA")
	private String sctype;
	@ApiModelProperty(value = "审批项名称")
	private String scname;
	@ApiModelProperty(value = "",hidden = true)
	private String sccreatedate;
	@ApiModelProperty(value = "",hidden = true)
	private String sclastdate;
	@ApiModelProperty(value = "asaNo")
	private String asano;
	@ApiModelProperty(value = "给经销商备注")
	private String sccomment1;
	@ApiModelProperty(value = "给审批者备注")
	private String sccomment2;
	@ApiModelProperty(value = "备注")
	private String sccomment3;
	@ApiModelProperty(value = "vinNo")
	private String vinNo;
	@ApiModelProperty(value = "")
	private String    filaname1 ;
	@ApiModelProperty(value = "")
	private String   dealer1 ;
	@ApiModelProperty(value = "")
	private String    approver1 ;
	@ApiModelProperty(value = "")
	private String    filaname2 ;
	@ApiModelProperty(value = "")
	private String   dealer2 ;
	@ApiModelProperty(value = "")
	private String    approver2 ;
	@ApiModelProperty(value = "")
	private String    filaname3 ;
	@ApiModelProperty(value = "")
	private String   dealer3 ;
	@ApiModelProperty(value = "")
	private String    approver3 ;
	@ApiModelProperty(value = "")
	private String    filaname4 ;
	@ApiModelProperty(value = "")
	private String   dealer4 ;
	@ApiModelProperty(value = "")
	private String    approver4 ;
	@ApiModelProperty(value = "")
	private String    filaname5 ;
	@ApiModelProperty(value = "")
	private String   dealer5 ;
	@ApiModelProperty(value = "")
	private String    approver5 ;
	@ApiModelProperty(value = "")
	private String nowdate;
	@ApiModelProperty(value = "")
	private String update_user;
	@ApiModelProperty(value = "页号")
	private int pageNum;
	@ApiModelProperty(value = "条数")
	private int pageSize;


	public String getVinNo() {
		return vinNo;
	}

	public void setVinNo(String vinNo) {
		this.vinNo = vinNo;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getScno() {
		return scno;
	}
	public void setScno(String scno) {
		this.scno = scno;
	}
	public String getScstatus() {
		return scstatus;
	}
	public void setScstatus(String scstatus) {
		this.scstatus = scstatus;
	}
	public String getSctype() {
		return sctype;
	}
	public void setSctype(String sctype) {
		this.sctype = sctype;
	}
	public String getScname() {
		return scname;
	}
	public void setScname(String scname) {
		this.scname = scname;
	}
	public String getSccreatedate() {
		return sccreatedate;
	}
	public void setSccreatedate(String sccreatedate) {
		this.sccreatedate = sccreatedate;
	}
	public String getSclastdate() {
		return sclastdate;
	}
	public void setSclastdate(String sclastdate) {
		this.sclastdate = sclastdate;
	}
	public String getAsano() {
		return asano;
	}
	public void setAsano(String asano) {
		this.asano = asano;
	}
	public String getSccomment1() {
		return sccomment1;
	}
	public void setSccomment1(String sccomment1) {
		this.sccomment1 = sccomment1;
	}
	public String getSccomment2() {
		return sccomment2;
	}
	public void setSccomment2(String sccomment2) {
		this.sccomment2 = sccomment2;
	}
	public String getSccomment3() {
		return sccomment3;
	}
	public void setSccomment3(String sccomment3) {
		this.sccomment3 = sccomment3;
	}
	public String getVinno() {
		return vinNo;
	}
	public void setVinno(String vinNo) {
		this.vinNo = vinNo;
	}
	public String getFilaname1() {
		return filaname1;
	}
	public void setFilaname1(String filaname1) {
		this.filaname1 = filaname1;
	}
	public String getDealer1() {
		return dealer1;
	}
	public void setDealer1(String dealer1) {
		this.dealer1 = dealer1;
	}
	public String getApprover1() {
		return approver1;
	}
	public void setApprover1(String approver1) {
		this.approver1 = approver1;
	}
	public String getFilaname2() {
		return filaname2;
	}
	public void setFilaname2(String filaname2) {
		this.filaname2 = filaname2;
	}
	public String getDealer2() {
		return dealer2;
	}
	public void setDealer2(String dealer2) {
		this.dealer2 = dealer2;
	}
	public String getApprover2() {
		return approver2;
	}
	public void setApprover2(String approver2) {
		this.approver2 = approver2;
	}
	public String getFilaname3() {
		return filaname3;
	}
	public void setFilaname3(String filaname3) {
		this.filaname3 = filaname3;
	}
	public String getDealer3() {
		return dealer3;
	}
	public void setDealer3(String dealer3) {
		this.dealer3 = dealer3;
	}
	public String getApprover3() {
		return approver3;
	}
	public void setApprover3(String approver3) {
		this.approver3 = approver3;
	}
	public String getFilaname4() {
		return filaname4;
	}
	public void setFilaname4(String filaname4) {
		this.filaname4 = filaname4;
	}
	public String getDealer4() {
		return dealer4;
	}
	public void setDealer4(String dealer4) {
		this.dealer4 = dealer4;
	}
	public String getApprover4() {
		return approver4;
	}
	public void setApprover4(String approver4) {
		this.approver4 = approver4;
	}
	public String getFilaname5() {
		return filaname5;
	}
	public void setFilaname5(String filaname5) {
		this.filaname5 = filaname5;
	}
	public String getDealer5() {
		return dealer5;
	}
	public void setDealer5(String dealer5) {
		this.dealer5 = dealer5;
	}
	public String getApprover5() {
		return approver5;
	}
	public void setApprover5(String approver5) {
		this.approver5 = approver5;
	}
	public String getNowdate() {
		return nowdate;
	}
	public void setNowdate(String nowdate) {
		this.nowdate = nowdate;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	
}
