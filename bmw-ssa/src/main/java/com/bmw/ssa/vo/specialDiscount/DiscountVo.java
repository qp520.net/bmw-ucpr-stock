package com.bmw.ssa.vo.specialDiscount;

public class DiscountVo{

	private String result;
	
	
	private String stickerPrice;
	private String finalPrice;
	
	private String sdaId;
	private String category;
	private String perform;
	private String variantCode;
	
	private String disc_b;
	private String disc_c;
	private String disc_d;
	
	private String dealerid;
	private String accountid;
	
	private String promotion;
	
	private String ownerBy;
	
	// begin shenzk 20160413
	private String companyCn;
	
	private String remarkX;
	// end shenzk 20160413
	
	public String getDealerid() {
		return dealerid;
	}
	public void setDealerid(String dealerid) {
		this.dealerid = dealerid;
	}
	public String getAccountid() {
		return accountid;
	}
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}
	public String getDisc_b() {
		if (disc_b == null || disc_b.length() == 0) {
			disc_b = "0";
		}
		return disc_b;
	}
	public void setDisc_b(String disc_b) {
		this.disc_b = disc_b;
	}
	public String getDisc_c() {
		if (disc_c == null || disc_c.length() == 0) {
			disc_c = "0";
		}
		return disc_c;
	}
	public void setDisc_c(String disc_c) {
		this.disc_c = disc_c;
	}
	public String getDisc_d() {
		if (disc_d == null || disc_d.length() == 0) {
			disc_d = "0";
		}
		return disc_d;
	}
	public void setDisc_d(String disc_d) {
		this.disc_d = disc_d;
	}
	public String getStickerPrice() {
		if (stickerPrice == null || stickerPrice.length() == 0) {
			stickerPrice = "0";
		}
		return stickerPrice;
	}
	public void setStickerPrice(String stickerPrice) {
		this.stickerPrice = stickerPrice;
	}
	public String getFinalPrice() {
		if (finalPrice == null || finalPrice.length() == 0) {
			finalPrice = "0";
		}
		return finalPrice;
	}
	public void setFinalPrice(String finalPrice) {
		this.finalPrice = finalPrice;
	}
	public String getSdaId() {
		return sdaId;
	}
	public void setSdaId(String sdaId) {
		this.sdaId = sdaId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPerform() {
		return perform;
	}
	public void setPerform(String perform) {
		this.perform = perform;
	}
	public String getVariantCode() {
		if (variantCode == null) {
			variantCode = "";
		}
		return variantCode;
	}
	public void setVariantCode(String variantCode) {
		this.variantCode = variantCode;
	}
	public String getResult() {
		if (result == null || result.length() == 0) {
			result = "0";
		}
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getPromotion() {
		if (promotion == null || promotion.length() == 0) {
			promotion = "0";
		}
		return promotion;
	}
	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}
	public String getOwnerBy() {
		if (ownerBy == null) {
			ownerBy = "";
		}
		return ownerBy;
	}
	public void setOwnerBy(String ownerBy) {
		this.ownerBy = ownerBy;
	}
	public String getCompanyCn() {
		return companyCn;
	}
	public void setCompanyCn(String companyCn) {
		this.companyCn = companyCn;
	}
	public String getRemarkX() {
		return remarkX;
	}
	public void setRemarkX(String remarkX) {
		this.remarkX = remarkX;
	}
	
	
}
