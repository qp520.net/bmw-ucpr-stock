package com.bmw.ssa.vo.specialDiscount;

import lombok.Data;

@Data
public class ProcVo {

	private String userName;
	private String sdaId;
	private String buttonStatus;

	
}
