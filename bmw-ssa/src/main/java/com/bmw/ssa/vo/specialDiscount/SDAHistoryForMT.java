package com.bmw.ssa.vo.specialDiscount;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * get ssa_sda for modify
 *
 */
@Data
public class SDAHistoryForMT implements Serializable {
	 /**
	 * 
	 */
	private static final long serialVersionUID = -8876802235414847104L;
	private String user_name;
	private String action_id;
	private String sda_id;
	private String result_text;
	private String result_id;
	private String sda_no;
	
	

	 
}
