package com.bmw.ssa.vo.specialDiscount;

import java.io.Serializable;

/**
 * 
 * get ssa_sda for modify
 *
 */
public class SDAInfoForModifyData implements Serializable {
	private static final long serialVersionUID = -8782792802973005106L;

	private String model;
	private String variant;
	private String sdaNo;
	private String fleetDiscount;
	private String name;
	private String idno;
	private String stickerPrice;
	private String finalPrice;
	private String discount;
	private String last_action_user;
	//cr
	private String customer_category;
	private String car_reform;
	private String disc_c;  
	private String disc_d;
	private String accountid;
	private String sdaid;
	private String dealer_id;
	private String company_cn;
	private String remark;

}
