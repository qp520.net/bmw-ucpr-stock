package com.bmw.ssa.vo.specialDiscount;

import lombok.Data;

@Data
public class SDAInfoForModifyVo {

	private String model;
	private String variant;
	private String sdaNo;
	private String fleetDiscount;
	private String name;
	private String idno;
	private String stickerPrice;
	private String finalPrice;
	private String discount;
	
	private String seq;
	private String audit_id;
	private String dealer_id;
	private String last_action_user;
	//cr
		private String customer_category;
		private String car_reform;
		private String disc_c;  
		private String disc_d;
		private String accountid;
		private String sdaid;
	private String company_cn;
	private String remark;
	
}
