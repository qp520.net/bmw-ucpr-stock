package com.bmw.ssa.vo.specialDiscount;


import com.bmw.ssa.po.KeyComplaint.SelectItem;

import java.io.Serializable;
import java.util.List;

public class SDAModifyData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sda_no;
	private String vehicle_variant_b;
	private String vehicle_variant_e;
	private String fleet_discount_b;
	private String fleet_discount_e;
	private String cust_name_b;
	private String cust_name_e;
	private String cust_id_b;
	private String cust_id_e;
	private String sticker_price_b;
	private String sticker_price_e;
	private String final_price_b;
	private String final_price_e;
	private String total_discount_b;
	private String total_discount_e;
	private String reason;
	private String status;
	private String model;
	private byte[] certificate;
	private List<SelectItem> varientList;
	private String file_name;
	private String dealer_id;
	private String create_year;
	private String next_approver_seq;
	private String last_user_name;
	private String last_action_user;
	private String customer_category;
	private String car_reform;
	private String disc_c;  
	private String disc_d;
	private String accountid;
	private String dealerID;
	private String dealerName;
	private String region;
	private String remark;
	
	//xuding add: 配置下拉框vehicle_variant_b 对应的值label
	private String vehicle_variant_blabel;
	private String vehicle_variant_elabel;
	//xuding add end

	public String getVehicle_variant_blabel() {
		return vehicle_variant_blabel;
	}
	public void setVehicle_variant_blabel(String vehicle_variant_blabel) {
		this.vehicle_variant_blabel = vehicle_variant_blabel;
	}
	public String getVehicle_variant_elabel() {
		return vehicle_variant_elabel;
	}
	public void setVehicle_variant_elabel(String vehicle_variant_elabel) {
		this.vehicle_variant_elabel = vehicle_variant_elabel;
	}
	public String getDealerID() {
		return dealerID;
	}
	public void setDealerID(String dealerID) {
		this.dealerID = dealerID;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	// begin shenzk 20160413
	private String companyCn;
	// end shenzk 20160413

	public String getCustomer_category() {
		return customer_category;
	}
	public void setCustomer_category(String customer_category) {
		this.customer_category = customer_category;
	}
	public String getCar_reform() {
		return car_reform;
	}
	public void setCar_reform(String car_reform) {
		this.car_reform = car_reform;
	}
	public String getDisc_c() {
		return disc_c;
	}
	public void setDisc_c(String disc_c) {
		this.disc_c = disc_c;
	}
	public String getDisc_d() {
		return disc_d;
	}
	public void setDisc_d(String disc_d) {
		this.disc_d = disc_d;
	}
	public String getAccountid() {
		return accountid;
	}
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}
	private String sdaid;

	public String getSdaid() {
		return sdaid;
	}
	public void setSdaid(String sdaid) {
		this.sdaid = sdaid;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getLast_action_user() {
		return last_action_user;
	}
	public void setLast_action_user(String last_action_user) {
		this.last_action_user = last_action_user;
	}
	public String getDealer_id() {
		return dealer_id;
	}
	public void setDealer_id(String dealer_id) {
		this.dealer_id = dealer_id;
	}
	public String getCreate_year() {
		return create_year;
	}
	public void setCreate_year(String create_year) {
		this.create_year = create_year;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getSda_no() {
		return sda_no;
	}
	public void setSda_no(String sda_no) {
		this.sda_no = sda_no;
	}
	public String getVehicle_variant_b() {
		return vehicle_variant_b;
	}
	public void setVehicle_variant_b(String vehicle_variant_b) {
		this.vehicle_variant_b = vehicle_variant_b;
	}
	public String getVehicle_variant_e() {
		return vehicle_variant_e;
	}
	public void setVehicle_variant_e(String vehicle_variant_e) {
		this.vehicle_variant_e = vehicle_variant_e;
	}
	public String getFleet_discount_b() {
		return fleet_discount_b;
	}
	public void setFleet_discount_b(String fleet_discount_b) {
		this.fleet_discount_b = fleet_discount_b;
	}
	public String getFleet_discount_e() {
		return fleet_discount_e;
	}
	public void setFleet_discount_e(String fleet_discount_e) {
		this.fleet_discount_e = fleet_discount_e;
	}
	public String getCust_name_b() {
		return cust_name_b;
	}
	public void setCust_name_b(String cust_name_b) {
		this.cust_name_b = cust_name_b;
	}
	public String getCust_name_e() {
		return cust_name_e;
	}
	public void setCust_name_e(String cust_name_e) {
		this.cust_name_e = cust_name_e;
	}
	public String getCust_id_b() {
		return cust_id_b;
	}
	public void setCust_id_b(String cust_id_b) {
		this.cust_id_b = cust_id_b;
	}
	public String getCust_id_e() {
		return cust_id_e;
	}
	public void setCust_id_e(String cust_id_e) {
		this.cust_id_e = cust_id_e;
	}
	public String getSticker_price_b() {
		return sticker_price_b;
	}
	public void setSticker_price_b(String sticker_price_b) {
		this.sticker_price_b = sticker_price_b;
	}
	public String getSticker_price_e() {
		return sticker_price_e;
	}
	public void setSticker_price_e(String sticker_price_e) {
		this.sticker_price_e = sticker_price_e;
	}
	public String getFinal_price_b() {
		return final_price_b;
	}
	public void setFinal_price_b(String final_price_b) {
		this.final_price_b = final_price_b;
	}
	public String getFinal_price_e() {
		return final_price_e;
	}
	public void setFinal_price_e(String final_price_e) {
		this.final_price_e = final_price_e;
	}
	public String getTotal_discount_b() {
		return total_discount_b;
	}
	public void setTotal_discount_b(String total_discount_b) {
		this.total_discount_b = total_discount_b;
	}
	public String getTotal_discount_e() {
		return total_discount_e;
	}
	public void setTotal_discount_e(String total_discount_e) {
		this.total_discount_e = total_discount_e;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public byte[] getCertificate() {
		return certificate;
	}
	public void setCertificate(byte[] certificate) {
		this.certificate = certificate;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public List<SelectItem> getVarientList() {
		return varientList;
	}
	public void setVarientList(List<SelectItem> varientList) {
		this.varientList = varientList;
	}
	public String getNext_approver_seq() {
		return next_approver_seq;
	}
	public void setNext_approver_seq(String next_approver_seq) {
		this.next_approver_seq = next_approver_seq;
	}
	public String getLast_user_name() {
		return last_user_name;
	}
	public void setLast_user_name(String last_user_name) {
		this.last_user_name = last_user_name;
	}
	public String getCompanyCn() {
		return companyCn;
	}
	public void setCompanyCn(String companyCn) {
		this.companyCn = companyCn;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
