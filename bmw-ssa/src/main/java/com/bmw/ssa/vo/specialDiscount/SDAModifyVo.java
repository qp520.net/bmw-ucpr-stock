package com.bmw.ssa.vo.specialDiscount;

import lombok.Data;

@Data
public class SDAModifyVo {

	private String sda_no;
	private String vehicle_variant_b;
	private String vehicle_variant_e;
	private String fleet_discount_b;
	private String fleet_discount_e;
	private String cust_name_b;
	private String cust_name_e;
	private String cust_id_b;
	private String cust_id_e;
	private String sticker_price_b;
	private String sticker_price_e;
	private String final_price_b;
	private String final_price_e;
	private String total_discount_b;
	private String total_discount_e;
	private String reason;
	private String status;
	private byte[] certificate;
	private String model;
	private String file_name;
	private String dealer_id;
	private String create_year;
	private String user_name;
	private String next_approver_seq;
	private String last_user_name;
	private String mtflag;
	
	private String last_action_user;
	private String sdaid;
	private String language;
	

}
