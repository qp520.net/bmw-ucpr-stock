package com.bmw.ssa.vo.specialDiscount;

import lombok.Data;

@Data
public class SdaDocVo{

	private String displayId;
	private String displayName;
	private String required;
	private String sequence;
	private String IS_REQUIRED;

	
}
