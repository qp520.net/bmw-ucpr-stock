package com.bmw.ssa.vo.specialDiscount;

import com.bmw.ssa.entity.speciaDiscount.SsaSdaHistory;
import lombok.Data;

import java.sql.Date;
@Data
public class SdaHistoryVo extends SsaSdaHistory {

	private String language;
	private String title1;
	private String title2;
	private Date changeDate;

	
	
}
