package com.bmw.ssa.vo.specialDiscount;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by Administrator on 2019/9/9 0009.
 */
@Data
@ApiModel
public class SearchInitVo {
    @ApiModelProperty(value = "国际化")
    private String language;
    @ApiModelProperty(value = "经销商")
    private Integer customerid;
    @ApiModelProperty(value = "客户姓名")
    private String userName;
    @ApiModelProperty(value = "客户类别")
    private String	category;
    @ApiModelProperty(value = "角色")
    private String userRole;
    @ApiModelProperty(value = "")
    private String modelCode;
}
