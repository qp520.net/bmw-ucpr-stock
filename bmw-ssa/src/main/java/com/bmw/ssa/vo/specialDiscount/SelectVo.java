package com.bmw.ssa.vo.specialDiscount;

import lombok.Data;

@Data
public class SelectVo {
	private String language;
	private String key1;
	private String value1;
	private String seriesSec;
	private String othersKey;
	
	private String catgoryFlg;
	
	private String customerid;


}
