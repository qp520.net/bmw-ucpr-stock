package com.bmw.ssa.vo.specialDiscount;

import lombok.Data;

@Data
public class SpecialDiscountApplicationVo  {
	private String dealerId;
	private String accountId;
	private String status;
	private String exclamationMark;
	private String asterisk;
	private String sdaNo;
	private String dmsCustomerNo;
	private String customerCategory;
	private String initiator;
	private String nameCN;
	private String nameEN;
	private String companyCN;
	private String companyEN;
	private String applicantion;
	private String sdaCompanyDepartment;
	private String sdaApplicantTitle;
	private String sdaMobileApplicant;
	private String title;
	private String phone;
	private String vehicleOwnedBy;
	private String sdaContactReference;
	private String sdaPhoneContactRef;
	private String model;
	private String variant;
	private String description;
	private String extCol;
	private String intCol;
	private String expDelivery;
	private String suggestedDealer;
	private String claimDealerID;
	private String dealerName;
	private String stickerPrice;
	private String promotion;
	private String discC;
	private String discB;
	private String discD;
	private String vin;
	private String retailDate;
	private String remark;
	private String applicationDate;
	private String createDate;
	private String lastActionDate;
	private String customerId;
	private String user;
	private String language;
	
    // 2016/04/07 update by wbo_neu
    private String accountAppliedFrom ; 
    private String accountAppliedTo;
    private String lastActionDateFrom;
    private String lastActionDateTo;
    // 2016/04/07 update by wbo_neu
	
	private String title1;
	private String title2;
	private String title3;
	
	private int firstRecord;
	private int lastRecord;
	private String sort;
	private String sortADSC;

	private int pageNum =1;
	private int pageSize =10;

}
