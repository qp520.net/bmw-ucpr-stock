package com.bmw.ssa.vo.specialDiscount;

import lombok.Data;

@Data
public class SsaLookup {
	private	String	CATEGORY	;
	private	String	NUM_VAL	;
	private	String	CHAR_VAL	;
	private	String	TEXT_EN	;
	private	String	TEXT_CN	;
	private	String	ACTIVE	;

}
