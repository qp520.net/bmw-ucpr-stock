package com.bmw.ssa.vo.specialDiscount;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
@Data
@ApiModel
public class SsaSda  {
	private	String	SDA_ID	;
	@ApiModelProperty(value = "车辆配置")
	private	String	VARIANT_CODE	;
	@ApiModelProperty(value = "外饰颜色")
	private	String	EXT_COL	;
	@ApiModelProperty(value = "内饰颜色")
	private	String	INT_COL	;
	@ApiModelProperty(value = "预计交付日期")
	private	Date	EXP_DELIVERY	;
	@ApiModelProperty(value = "建议零售价(人民币)")
	private	String	STICKER_PRICE	;
	@ApiModelProperty(value = "总折扣%")
	private	String	DISC_C	;
	@ApiModelProperty(value = "BMW机构销售%")
	private	String	DISC_B	;
	@ApiModelProperty(value = "")
	private	String	DISC_D	;
	@ApiModelProperty(value = "类别")
	private	String	REMARK	;
	private String  REMARK_VALUE;
	private	String	STATUS	;
	private	String	VIN_7	;
	private	String	TITLE	;
	@ApiModelProperty(value = "英文名")
	private	String	NAME_EN	;
	@ApiModelProperty(value = "中文名")
	private	String	NAME_CN	;
	@ApiModelProperty(value = "手机号")
	private	String	PHONE	;
	@ApiModelProperty(value = "机构(英文)")
	private	String	COMPANY_EN	;
	@ApiModelProperty(value = "机构中文")
	private	String	COMPANY_CN	;
	private	Date	RETAIL_DATE	;
	@ApiModelProperty(value = "经销商编号")
	private	String	CLAIM_DEALERID	;
	@ApiModelProperty(value = "DMS客户编号")
	private	String	CLAIM_CUSTOMERID	;
	private	String	DEALERID	;
	private	String	ACCOUNTID	;
	@ApiModelProperty(value = "申请人")
	private	String	APPLICANT	;
	private	String	SUGGESTED_DEALER	;
	@ApiModelProperty(value = "客户类别")
	private	String	CUSTOMER_CATEGORY	;
	private String CUSTOMER_CATEGORY_VALUE;
	@ApiModelProperty(value = "型号")
	private	String	SERIES	;
	@ApiModelProperty(value = "最终价格(人民币)")
	private	String	FINAL_PRICE	;
	private	String	BMW_SUBS	;
	private	String	PROMOTION	;
	@ApiModelProperty(value = "公司/部门")
	private	String	COM_DEP_APPLICANT	;
	@ApiModelProperty(value = "职位")
	private	String	TITLE_APPLICANT	;
	@ApiModelProperty(value = "申请人手机号")
	private	String	MOBILE_APPLICANT	;
	@ApiModelProperty(value = "申请人邮箱")
	private	String	EMAIL_APPLICANT	;
	private	String	OWNED_BY	;
	@ApiModelProperty(value = "身份证号")
	private	String	ID_NO	;
	@ApiModelProperty(value = "邮箱")
	private	String	EMAIL	;
	@ApiModelProperty(value = "员工号")
	private	String	EMPLOYEEID_ACP	;
	@ApiModelProperty(value = "姓名(中/英)")
	private	String	NAME_CONTACT_REF	;
	private	String	TITLE_CONTACT_REF	;
	@ApiModelProperty(value = "与客户关系")
	private	String	RELATIONSHIP_CONTACT_REF	;
	@ApiModelProperty(value = "联系方式(联系人/推荐人)")
	private	String	PHONE_CONTACT_REF	;
	@ApiModelProperty(value = "公司")
	private	String	COMPANY_CONTACT_REF	;
	@ApiModelProperty(value = "申请日期")
	private	Date	APPLICATION_DATE	;
	private	String	NEXT_APPROVER_SEQ	;
	private	String	AUDIT_CODE	;
	private	String	AUDIT_ID	;
	private	String	AUDIT_RESULT	;
	private	String	AUDIT_RESET_CODE	;
	private	String	AUDIT_REJECT_CODE	;
	private	String	AUDIT_RESET_CODES	;
	private	String	AUDIT_REJECT_CODES	;
	private	String	AUDIT_TEXT	;
	private	String	NEXT_APPROVER_ROLE	;
	private	String	DOC1T	;
	private	String	DOC2T	;
	private	String	DOC3T	;
	private	String	DOC4T	;
	private	String	DOC5T	;
	private	String	DOC6T	;
	private	Date	APPROVAL_DATE	;
	private	String	REGION	;
	private	String	SDA_NO	;
	private	Date	LAST_ACTION_DATE	;
	private	String	CREATED_DEALER	;
	private	String	CREATED_BY	;
	private	Date	CREATED_DATE	;
	private	String	INITIATOR	;
	@ApiModelProperty(value = "SDA编号")
	private	String	SDA_NO_NEW	;
	private	String	LAST_USER_NAME	;
	private	String	APPRO_COMMENT	;
	private	String	UNIVERSITY	;
	private	String	PAYMENT_METHOD	;
	private	String	COUNTRY_STUDIED	;
	private	String	GRADUATION_YEAR	;
	private	String	CAR_REFORM	;
	private	String	LICENSE_QUOTAS	;
	private	String	JOB_INDUSTRY	;
	private	String	CUSTOMER_SOURCE	;
	private	String	DOC7T	;
	private	String	NODELETE	;
	private	String	RESET_TIME	;

	private	String	USER_NAME	;

	private String LANG;
	private String VARIANT_CODE_VALUE;
	@ApiModelProperty(value = "状态")
	private String dXSTAT;
	private String TITLE_VALUE;
	private String SERIES_VALUE;
	private String OWNED_BY_VALUE;
	private String SUGGESTED_DEALER_VALUE;
	private String PAYMENT_METHOD_VALUE;
	
	private String seq ;
	
	private String SDAIDVIEW;
//	// 0 是新增，1是 申请
//	private int buttonFlag;

}
