package com.bmw.ssa.vo.specialDiscount;

import lombok.Data;

@Data
public class UserinfoVo {

	private String username;
	private String customerid;
	private String language;
	
	private String asaId;

}
