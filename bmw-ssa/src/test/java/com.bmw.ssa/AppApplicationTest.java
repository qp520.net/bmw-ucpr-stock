package com.bmw.ssa;

import com.bmw.ssa.constants.Constants;
import com.bmw.ssa.service.InsuranceService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * 公积金验证测试类
 * Created by Administrator on 2019/8/21 0021.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AppApplicationTest {

    @Autowired
    private InsuranceService insuranceService;

    @Autowired
    private MockMvc mvc;

    /**
     * countextLoads
     * 筛选条件的数据接口
     *
     * @return void
     * -----------------------------------------------------------------------------------------------------------------
     */
    @Test
    public void contextLoads() throws Exception {
//        String json = "{\"pageNumber\":\"1\",\"pageSize\":\"10\"}";
        mvc.perform(MockMvcRequestBuilders.get("/insuranceController/getSearchData")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
//                .content(json.getBytes())
                        .param("department", "1")
                        .param("locale", "8")
                        .param("customerid", "2")
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    /**
     * 获取数据列表测试
     * getListTest
     *
     * @return void
     * -----------------------------------------------------------------------------------------------------------------
     */
    @Test
    public void getListTest() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("language", "8");

        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/getInsuranceList")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json.getBytes()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    /**
     * testAddInsurance
     * 测试新增接口
     *
     * @return void
     * -----------------------------------------------------------------------------------------------------------------
     */
    @Test
    public void testAddInsurance() throws Exception {
        JSONObject jsonparamter = new JSONObject();
        jsonparamter.put("dealerId", "2");
        jsonparamter.put("insNo", "3");
        jsonparamter.put("activationDate", "2019-08-28");
        jsonparamter.put("applicantName", "TEST2");
        jsonparamter.put("applicantMobile", "18987568362");
        jsonparamter.put("companyCN", "TEST_TEST-2");
        jsonparamter.put("nameCN", "TEST");
        jsonparamter.put("idNo", "1290987");
        jsonparamter.put("nextApproverRole", "1");
        jsonparamter.put("nextApproverSeq", "1");
        jsonparamter.put("auditCode", "1");
        jsonparamter.put("auditId", "1");
        jsonparamter.put("auditResetCode", "1");
        jsonparamter.put("auditRejectCode", "1");
        jsonparamter.put("auditText", "1");
        jsonparamter.put("applicationDate", "2019-08-28");
        jsonparamter.put("userName", "weasaw");
        jsonparamter.put("resetTime", "10000");
        String json = jsonparamter.toString();
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/doInsetInsurance")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    /**
     * testEdit
     * 测试编辑接口
     *
     * @return void
     * -----------------------------------------------------------------------------------------------------------------
     */
    @Test
    public void testEdit() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("language", "8");
        jsonObject.put("idNo", "123243");
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/getInsuranceDetail")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * testDetailInDownload
     *
     * @return void
     * -----------------------------------------------------------------------------------------------------------------
     */
    public void testDetailInDownload() throws Exception {

        String json = "";
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/getSearchData")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json.getBytes()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * testDetailInUpload
     *
     * @return void
     * -----------------------------------------------------------------------------------------------------------------
     */
    @Test
    public void testDetailInUpload() throws Exception {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("applicantName", "测试更新");
        jsonObject.put("applicantMobile", "测试更新手机号");
        jsonObject.put("companyCN", "公司名");
        jsonObject.put("nameCN", "用户名");
        jsonObject.put("idNo", "121312");
        jsonObject.put("userName", "附近开了房间都是");
        jsonObject.put("auditText", "备注");
        jsonObject.put("insId", "2");


        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.put("/insuranceController/doUpdateInsurance")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json.getBytes()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * testCreateIns
     * 测试写入历史纪录
     *
     * @return void
     * -----------------------------------------------------------------------------------------------------------------
     */
    @Test
    public void testCreateIns() throws Exception {
        JSONObject json = new JSONObject();
        json.put("userName", "TEST2");
        json.put("insId", 1);
        String paramJson = json.toString();
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/addInsuranceHistory")
                .content(paramJson)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    /**
     * readInsHistory
     * 获取历史数据list
     *
     * @return void
     * -----------------------------------------------------------------------------------------------------------------
     */
    @Test
    public void readInsHistory() throws Exception {
        String userName = "test";
        JSONObject json = new JSONObject();
        json.put("locale", "1");
        json.put("insId", "1");
        String resultJson = json.toString();

        mvc.perform(MockMvcRequestBuilders.get("/insuranceController/getInsuranceHistoryData")
//                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
//                .content(resultJson.getBytes())
                        .param("locale", "1")
                        .param("insId", "1")
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
        log.debug("===========END=======================");
    }

    /**
     * 查询审批列表的测试
     * @throws Exception
     */
    @Test
    public void getInsuranceAppList() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userName","axg3794");
        jsonObject.put("language",Constants.GET_INSURANCE_SEARCH_CN_8);
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/getInsuranceAppList")
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print());
    }

    /**
     * 测试审批申请
     * 1.申请，2审批
     * @throws Exception
     */
    @Test
    public void  testApply() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userName","axg3794");
        jsonObject.put("insId","3");
        jsonObject.put("operationType", Constants.OPERTION_TYPE_1);
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/doApplyInsurance")
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * 公积金申请测试拒绝
     *
     * @throws Exception
     */
    @Test
    public void testDoReject() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("auditRejectCode","2");
        jsonObject.put("auditText","测试拒绝申请");
        jsonObject.put("insId","3");
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/doRejectInsurance")
                .content(json.getBytes())
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * 公积金申请测试退回
     * @throws Exception
     */
    @Test
    public void testReset() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("auditResetCode","3");
        jsonObject.put("auditText","公积金申请测试退回");
        jsonObject.put("insId","3");
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/doResetInsurance")
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     *  校验身份证号是否已经申请过了
     * @throws Exception
     * retun: 审核的状态
     */
    @Test
    public void  testCheckKinsExistWeb() throws Exception{
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("insId","1");
        jsonObject.put("nameCN","asasas");
        jsonObject.put("dealerId","2");
        jsonObject.put("idNo","1290987");
        jsonObject.put("languageFlag",Constants.GET_INSURANCE_SEARCH_CN_8);
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/insuranceController/checkKinsExistWeb")
                .content(json.getBytes())
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}
