package com.bmw.ssa;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by Administrator on 2019/8/23 0023.
 */
public class BaseTest {
    public BaseTest() {
    }

    protected <T> ResponseEntity saveResponse(T t) {
        return ResponsUtils.created(t);
    }

    protected <T> ResponseEntity updateResponse(T t) {
        return ResponsUtils.success(t);
    }

    protected ResponseEntity deleteResponse() {
        return ResponsUtils.delete();
    }

    protected static <T> ResponseEntity selectResponse(T t) {
        return ResponsUtils.success(t);
    }

    protected <T> ResponseEntity listResponse(T t) {
        return ResponsUtils.success(t);
    }

    protected <T> ResponseEntity pageResponse(T t) {
        return ResponsUtils.success(t);
    }

    protected ResponseEntity errorResponse(String errorMessage) {
        return ResponsUtils.failure(Integer.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()), errorMessage);
    }
}
