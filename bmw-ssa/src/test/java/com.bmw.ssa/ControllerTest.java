package com.bmw.ssa;

import com.bmw.core.web.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Administrator on 2019/8/23 0023.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest extends BaseTest {
    public static void main(String[] args) {
        System.out.println(getData());
    }


    public static ResponseEntity getData(){
        ResponseEntity responseEntity = selectResponse(null);
        return responseEntity;

    }
}
