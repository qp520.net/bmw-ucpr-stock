package com.bmw.ssa;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Condition;

/**
 * Created by Administrator on 2019/8/22 0022.
 */
public class LambdaTest {
    public static void main(String[] args) {
//        List<String> item = new ArrayList<String>();
//        item.add("A");
//        item.add("B");
//        item.add("C");
//        item.add("D");
//        item.forEach(a->{
//            System.out.println(a);
//        });
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.stream().filter(s->s==5).mapToInt(i->i+1);
        list.forEach(s->{
            System.out.println(s);
        });

    }




}
