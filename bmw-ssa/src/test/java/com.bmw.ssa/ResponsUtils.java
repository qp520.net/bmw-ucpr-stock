package com.bmw.ssa;

import com.bmw.core.web.exception.BmwException;
import com.bmw.core.web.vo.ResponseVO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by Administrator on 2019/8/23 0023.
 */
public class ResponsUtils {
    public ResponsUtils() {
    }

    public static ResponseEntity success(Object body) {
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(Integer.valueOf(HttpStatus.OK.value()));
        responseVO.setMessage("请求成功");
        responseVO.setBody(body);
        return ResponseEntity.ok(responseVO);
    }

    public static ResponseEntity created(Object body) {
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(Integer.valueOf(HttpStatus.CREATED.value()));
        responseVO.setMessage("请求成功");
        responseVO.setBody(body);
        return ResponseEntity.ok(responseVO);
    }

    public static ResponseEntity delete() {
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(Integer.valueOf(HttpStatus.NO_CONTENT.value()));
        responseVO.setMessage("删除成功");
        return ResponseEntity.ok(responseVO);
    }

    public static ResponseEntity badRequest(String message) {
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(Integer.valueOf(HttpStatus.BAD_REQUEST.value()));
        responseVO.setMessage(message);
        return ResponseEntity.ok(responseVO);
    }

    public static ResponseEntity notFound() {
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(Integer.valueOf(HttpStatus.NOT_FOUND.value()));
        responseVO.setMessage("没有找到");
        return ResponseEntity.ok(responseVO);
    }

    public static ResponseEntity failure(BmwException ex) {
        if(ex.getCode() == null) {
            ex.setCode(Integer.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        }

        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(ex.getCode());
        responseVO.setMessage(ex.getMessage());
        return ResponseEntity.ok(responseVO);
    }

    public static ResponseEntity failure(Exception ex) {
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(Integer.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
        responseVO.setMessage(ex.getMessage());
        return ResponseEntity.ok(responseVO);
    }

    public static ResponseEntity failure(Integer code, String message) {
        ResponseVO responseVO = new ResponseVO();
        responseVO.setCode(code);
        responseVO.setMessage(message);
        return ResponseEntity.ok(responseVO);
    }

    public static ResponseEntity failure() {
        return failure(Integer.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()), "服务内部错误");
    }

}
