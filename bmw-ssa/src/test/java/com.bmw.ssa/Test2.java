package com.bmw.ssa;

import java.util.*;

/**
 * Created by Administrator on 2019/8/27 0027.
 */
public class Test2 {


    public static <T> List<List<T>> groupDataByCondition(List<T> srcList, Comparator<? super T> comparator) {
        List<List<T>> resultList = new ArrayList<>();
        for (int i = 0; i < srcList.size(); i++) {
            boolean isFindInCurrentGroups = false;

            //1.在现有组中查找
            for (int groupIndex = 0; groupIndex < resultList.size(); groupIndex++) {
                if (resultList.get(groupIndex).size() == 0 ||
                        comparator.compare(resultList.get(groupIndex).get(0), srcList.get(i)) == 0) {
                    //没有直接添加或者与第j组第一个匹配
                    resultList.get(groupIndex).add(srcList.get(i));
                    isFindInCurrentGroups = true;
                    break;
                }
            }
            //2.在现有组中没查找到
            if (!isFindInCurrentGroups) {
                List<T> newGroupList = new ArrayList<>();
                newGroupList.add(srcList.get(i));
                resultList.add(newGroupList);
            }
        }

        return resultList;
    }

}
