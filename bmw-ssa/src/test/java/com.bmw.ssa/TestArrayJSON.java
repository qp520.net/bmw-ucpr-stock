package com.bmw.ssa;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.bmw.ssa.po.KeyComplaint.SelectItem;
import com.bmw.ssa.po.specialDiscount.SelectItemPo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Administrator on 2019/9/10 0010.
 */
public class TestArrayJSON {
    public static void main(String[] args) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < 10; i++) {
            JSONObject js = new JSONObject();
            js.put("key1",i);
            jsonArray.add(js);
        }
        String json = jsonArray.toString();
        List<SelectItemPo> list = JSON.parseObject(json,new TypeReference<ArrayList<SelectItemPo>>(){});
        List<SelectItemPo> newList = list.stream().filter(s->Integer.valueOf(s.getNumKey())>3)
                .sorted(Comparator.comparing(SelectItemPo::getNumKey).reversed())
                .collect(Collectors.toList());
        List<SelectItemPo> list1 = list.stream()
                .filter(s->Integer.valueOf(s.getNumKey())>4)
                .sorted(((o1, o2) -> {
                    if(Integer.valueOf(o1.getNumKey())>3){
                        return Integer.valueOf(o1.getNumKey())-Integer.valueOf(o2.getNumKey());
                    }else {
                        return o1.getKey1().compareTo(o2.getKey1());
                    }
                }))
                .collect(Collectors.toList());
        list.sort(Comparator.comparing(o->o.getKey1()));
        Collections.sort(list, new Comparator<SelectItemPo>() {
            @Override
            public int compare(SelectItemPo o1, SelectItemPo o2) {
                return 0;
            }
        });
        System.out.println(list);
    }
}
