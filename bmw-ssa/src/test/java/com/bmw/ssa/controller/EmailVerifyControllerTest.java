package com.bmw.ssa.controller;

import com.alibaba.fastjson.JSON;
import com.bmw.ssa.constants.Constants;
import com.bmw.ssa.vo.emailVerify.EmailDomainParameterVo;
import com.bmw.ssa.vo.emailVerify.EmployeeParameterVo;
import com.bmw.ssa.vo.emailVerify.FleetAccountsParameterVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2019/9/19 0019.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class EmailVerifyControllerTest {
    @Autowired
    private MockMvc mvc;

    /**
     * 获取大客户数据列表
     * @throws Exception
     */
    @Test
    public void testSearchFleetAccounts() throws Exception {
        FleetAccountsParameterVo parameterVo = new FleetAccountsParameterVo();
        parameterVo.setFleetAccountNo("C0011502");
        parameterVo.setLanguage(Constants.GET_INSURANCE_SEARCH_CN_8);
        String json  = JSON.toJSONString(parameterVo);
        mvc.perform(MockMvcRequestBuilders.post("/emailVerify/searchFleetAccounts")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json.getBytes())
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * 获取邮箱验证列表
     * @throws Exception
     */

    @Test
    public void testSearchEmployeeEmail() throws Exception {
        EmployeeParameterVo parameterVo = new EmployeeParameterVo();
        parameterVo.setDealerId("27007");
        parameterVo.setAccountId("000003");
        parameterVo.setPageNum(8);
        parameterVo.setLanguage(Constants.GET_INSURANCE_SEARCH_CN_8);
        String json = JSON.toJSONString(parameterVo);
        mvc.perform(MockMvcRequestBuilders.post("/emailVerify/searchEmployeeEmail")
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    /**
     * 获取邮箱后缀
     * @throws Exception
     */
    @Test
    public void testGetEmailDoMainList() throws Exception {
        EmailDomainParameterVo parameterVo = new EmailDomainParameterVo();
        parameterVo.setDealerId("27077");
        parameterVo.setAccountId("000003");
        parameterVo.setStatus("60");
        String json = JSON.toJSONString(parameterVo);
        mvc.perform(MockMvcRequestBuilders.post("/emailVerify/getEmailDoMain")
        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json.getBytes())
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}