package com.bmw.ssa.controller;

import com.alibaba.fastjson.JSON;
import com.bmw.ssa.po.fleetCustomer.CustomerSearchPo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerMainSearchVo;
import com.bmw.ssa.vo.fleetCustomer.SsaCustomerOtherVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.File;
import java.io.FileInputStream;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2019/9/25 0025.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FleetCustomerControllerTest {
    @Autowired
    MockMvc mvc;

    @Test
    public void testSearchList() throws Exception {
        SsaCustomerMainSearchVo ssaCustomerMainSearchVo = new SsaCustomerMainSearchVo();
        String json = JSON.toJSONString(ssaCustomerMainSearchVo);
        mvc.perform(MockMvcRequestBuilders.get("/fleetCustomer/searchList")
        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testImportCustomerList() throws Exception {
   /*     SsaCustomerOtherVo ssaCustomerOtherVo = new SsaCustomerOtherVo();
        ssaCustomerOtherVo.setType(1);
        String json = JSON.toJSONString(ssaCustomerOtherVo);
        mvc.perform(MockMvcRequestBuilders.fileUpload("/fleetCustomer/importCustomerList")
                .file(new MockMultipartFile("excel", "customerBasicsTemplate (1).xlsx", "multipart/form-data", new FileInputStream(new File("D:/var/customerBasicsTemplate (1).xlsx"))))
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)

        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());*/
        SsaCustomerOtherVo ssaCustomerOtherVo = new SsaCustomerOtherVo();
        ssaCustomerOtherVo.setType(2);
        String json = JSON.toJSONString(ssaCustomerOtherVo);
        mvc.perform(MockMvcRequestBuilders.fileUpload("/fleetCustomer/importCustomerList")
                .file(new MockMultipartFile("excel", "customerOtherTemplate.xlsx", "multipart/form-data", new FileInputStream(new File("D:/var/customerOtherTemplate.xlsx"))))
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)

        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());


    }

    @Test
    public void testDoUpdate() throws Exception {
        CustomerSearchPo customerSearchPo = new CustomerSearchPo();
        customerSearchPo.setAccessionTime("2019-08-25 23:59:59");
        customerSearchPo.setUserName("test");
        customerSearchPo.setUserPhone("12345");
        customerSearchPo.setUserMobile("012");
        customerSearchPo.setUserMail("123@123");
        customerSearchPo.setCbuDea("33624");
        customerSearchPo.setUserPosition("大客户经理");
        String json = JSON.toJSONString(customerSearchPo);
        mvc.perform(MockMvcRequestBuilders.post("/fleetCustomer/doUpdate")
        .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void testSearchOtherList() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/fleetCustomer/searchOtherList")
                .param("userPosition","大客户经理")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)

        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testExportCustomerList() throws Exception {


    }

    @Test
    public void testExportOtherList() throws Exception {

    }

    @Test
    public void getInitSearchDataTest() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/fleetCustomer/getInitSearchData")
                .param("languageFlag","8")
                .param("key","27077")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)

        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}