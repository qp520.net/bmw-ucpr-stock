package com.bmw.ssa.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bmw.ssa.po.KeyAccountAsa.CreateKeyAccountAsaPo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2019/9/2 0002.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class KeyAccountAsaControllerTest {

    @Autowired
    private MockMvc mvc;

    /**
     * 测试初始化新增接口
     */
    @Test
    public void testGetCustomerCategory() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("languageFlag", "8");
        jsonObject.put("dealerId", "27077");
        Map<String, Object> params = new HashMap<>();
//        params.put("languageFlag","8");
        params.put("dealerId", "27077");
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.get("/keyAccountAsa/getKeyAccountInnitData")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
//                .param("languageFlag","8")
                        .param("dealerId", "27077")
//                .content(json)
                        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testGetRemarkList() throws Exception {


    }

    @Test
    public void testGetKeyAccountInnitData() throws Exception {

    }

    /**
     * 测试详情接口
     *
     * @throws Exception
     */
    @Test
    public void testGetKeyAccountAsaDetail() throws Exception {
        JSONObject jsonObject = new JSONObject();
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.get("/keyAccountAsa/getKeyAccountAsaDetail")
                        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
//                .content(json)
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .param("languageFlag", "8")
                        .param("dealerId", "27077")
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    /**
     * 创建申请
     *
     * @throws Exception
     */
    @Test
    public void testDoInsert() throws Exception {

        CreateKeyAccountAsaPo createKeyAccountAsaPo = new CreateKeyAccountAsaPo();
        createKeyAccountAsaPo.setRenew("0");
        createKeyAccountAsaPo.setDealerId(27077);
        createKeyAccountAsaPo.setCustomerCategory("94");
        createKeyAccountAsaPo.setUserName("axg3794");
        createKeyAccountAsaPo.setApplicantName("测试");
        createKeyAccountAsaPo.setApplicantMobile("13700090000");
        createKeyAccountAsaPo.setApplicantMail("123@wew.com");
        createKeyAccountAsaPo.setRemark("4");
        createKeyAccountAsaPo.setCompanyCn("哈哈公司");
        createKeyAccountAsaPo.setCompanyAddress("纽约");
        createKeyAccountAsaPo.setProvience("安徽");
        createKeyAccountAsaPo.setCity("芜湖");
        createKeyAccountAsaPo.setPostCode("10021");
        createKeyAccountAsaPo.setRegionName("北京");
        createKeyAccountAsaPo.setCharger("如意");
        createKeyAccountAsaPo.setTitle("老板");
        createKeyAccountAsaPo.setChargerContact("18900090010");
        createKeyAccountAsaPo.setBmwSupport("2");
        createKeyAccountAsaPo.setValidYear("2019");
        createKeyAccountAsaPo.setOperationType(0);
        createKeyAccountAsaPo.setAuditText("abcdwaeler");
        String json = JSON.toJSONString(createKeyAccountAsaPo);
        mvc.perform(MockMvcRequestBuilders.post("/keyAccountAsa/doInsert")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());


    }

    /**
     * 获得机构申请列表
     *
     * @throws Exception
     */
    @Test
    public void testGetKeyAccountAsa() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("language", "8");
        jsonObject.put("customer", "27077");
        jsonObject.put("pageNumber", 2);
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/keyAccountAsa/getKeyAccountAsa")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    /**
     * 测试更新
     *
     * @throws Exception
     */
    @Test
    public void testDoUpdate() throws Exception {
        CreateKeyAccountAsaPo createKeyAccountAsaPo = new CreateKeyAccountAsaPo();
        createKeyAccountAsaPo.setRenew("0");
        createKeyAccountAsaPo.setDealerId(27077);
        createKeyAccountAsaPo.setCustomerCategory("94");
        createKeyAccountAsaPo.setUserName("axg3794");
        createKeyAccountAsaPo.setApplicantName("测试");
        createKeyAccountAsaPo.setApplicantMobile("123");
        createKeyAccountAsaPo.setApplicantMail("123@wew.com");
        createKeyAccountAsaPo.setRemark("4");
        createKeyAccountAsaPo.setCompanyCn("哈哈公司");
        createKeyAccountAsaPo.setCompanyAddress("纽约");
        createKeyAccountAsaPo.setProvience("安徽");
        createKeyAccountAsaPo.setCity("芜湖");
        createKeyAccountAsaPo.setPostCode("10021");
        createKeyAccountAsaPo.setRegionName("北京");
        createKeyAccountAsaPo.setCharger("如意");
        createKeyAccountAsaPo.setTitle("老板");
        createKeyAccountAsaPo.setChargerContact("18900090010");
        createKeyAccountAsaPo.setBmwSupport("2");
        createKeyAccountAsaPo.setValidYear("2019");
        createKeyAccountAsaPo.setOperationType(0);
        createKeyAccountAsaPo.setKasaId(33);
        createKeyAccountAsaPo.setAuditText("abcdwaeler");

        String json = JSON.toJSONString(createKeyAccountAsaPo);
        mvc.perform(MockMvcRequestBuilders.post("/keyAccountAsa/doUpdate")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print());
    }

    /**
     * 测试详情
     *
     * @throws Exception
     */
    @Test
    public void testDoSelect() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("languageFlag", "8");
        jsonObject.put("kasaId", "33");
        String json = jsonObject.toString();
        mvc.perform(MockMvcRequestBuilders.post("/keyAccountAsa/doSelect")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}