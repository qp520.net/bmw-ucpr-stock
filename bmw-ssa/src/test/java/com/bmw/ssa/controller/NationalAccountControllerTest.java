package com.bmw.ssa.controller;

import com.alibaba.fastjson.JSON;
import com.bmw.ssa.vo.nationalAccount.NationalAccountSearchConditionVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.Assert.*;

/**
 * Created by 16074 on 2019/9/29.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class NationalAccountControllerTest {

    @Autowired
    MockMvc mvc;
    @Test
    public void testSearchData() throws Exception {
        NationalAccountSearchConditionVo nationalAccountSearchConditionVo = new NationalAccountSearchConditionVo();
        nationalAccountSearchConditionVo.setCustomerid("27077");
        nationalAccountSearchConditionVo.setEn_ch_flag(8);
        nationalAccountSearchConditionVo.setUser_name("axg3794");
        String json = JSON.toJSONString(nationalAccountSearchConditionVo);
        mvc.perform(MockMvcRequestBuilders.post("/nationalAccount/searchData")
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}