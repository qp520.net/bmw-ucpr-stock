package com.bmw.ssa.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bmw.ssa.po.specialDiscount.SelectItemPo;
import com.bmw.ssa.vo.specialDiscount.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2019/9/9 0009.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SpeciaDiscountControllerTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void testGetSpecialDiscountApplicationData() throws Exception {
        SpecialDiscountApplicationVo spv = new SpecialDiscountApplicationVo();
        spv.setLanguage("8");
        spv.setCustomerId("1");
        spv.setAccountId("1");
        spv.setDealerId("1");
        String json = JSON.toJSONString(spv);
        mvc.perform(MockMvcRequestBuilders.get("/speciaDiscount/getSpecialDiscountApplicationData")
//                .content(json)
                .param("language","8")
                .param("customerId","27077")
                .param("accountId","100008")
                .param("dealerId","00000")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(MockMvcResultHandlers.print());

    }

    @Test
    public void testGetSearchInitData() throws Exception{
        SearchInitVo searchInitVo = new SearchInitVo();
        searchInitVo.setLanguage("8");
        searchInitVo.setCustomerid(27077);
        searchInitVo.setUserName("axg3794");
        searchInitVo.setCategory("20");
        String json = JSON.toJSONString(searchInitVo);
        mvc.perform(MockMvcRequestBuilders.post("/speciaDiscount/getSearchInitData")
        .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void initAdd()throws Exception{
        SearchInitVo searchInitVo = new SearchInitVo();
        searchInitVo.setLanguage("8");
        searchInitVo.setCustomerid(27077);
        searchInitVo.setUserName("axg3794");
        searchInitVo.setModelCode("");
        String json = JSON.toJSONString(searchInitVo);
        mvc.perform(MockMvcRequestBuilders.post("/speciaDiscount/initAddData")
                .content(json.getBytes())
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
    @Test
    public void addTest() throws Exception{
        SsaSda ssaSda = new SsaSda();
        ssaSda.setDEALERID("1");
        ssaSda.setACCOUNTID("123");
        ssaSda.setAPPLICANT("adc");
        LocalDateTime localDateTime = LocalDateTime.now();
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        System.out.println(dateTimeFormatter.format(localDateTime));
        ssaSda.setAPPLICATION_DATE(new Date());
        ssaSda.setAPPRO_COMMENT("1212");
        ssaSda.setAUDIT_CODE("ASD12");
        ssaSda.setAPPROVAL_DATE(new Date());
        ssaSda.setAUDIT_REJECT_CODE("12");
        ssaSda.setAUDIT_ID("12");
        ssaSda.setAUDIT_RESET_CODE("12");
        ssaSda.setAUDIT_RESULT("12");
        ssaSda.setAUDIT_TEXT("qw");
        ssaSda.setBMW_SUBS("12");
        ssaSda.setCUSTOMER_CATEGORY("1");
        ssaSda.setCREATED_DEALER("1");
        ssaSda.setCREATED_BY("2");
        ssaSda.setCREATED_DATE(new Date());
        ssaSda.setNAME_CONTACT_REF("12");
        ssaSda.setTITLE_CONTACT_REF("31");
        ssaSda.setRELATIONSHIP_CONTACT_REF("12");
        ssaSda.setPHONE_CONTACT_REF("12");
        ssaSda.setCOMPANY_CONTACT_REF("12");
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(ssaSda);
        System.out.println(json);
        mvc.perform(MockMvcRequestBuilders.post("/speciaDiscount/addSda")
        .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        )

                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    public void updateTest() throws Exception{

    }

    @Test
    public void deleteTest() throws Exception{
        mvc.perform(MockMvcRequestBuilders.get("/speciaDiscount/deleteSdaData").param("sdaId","46"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }


    public void nodifyTest() throws Exception{
        JSONObject jsonObject = new JSONObject();
        SDAModifyVo vo = new SDAModifyVo();
        vo.setSda_no("2014-8891");
        vo.setModel("");
        String json  = jsonObject.toJSONString();
        mvc.perform(MockMvcRequestBuilders.post("/speciaDiscount/modify").content(json.getBytes()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    public void addInit()throws Exception{

    }

    @Test
    public void getDisc() throws Exception{
        DiscountVo discountVo = new DiscountVo();
        discountVo.setAccountid("27077");
        discountVo.setDealerid("27077");
        discountVo.setCategory("99");
        discountVo.setRemarkX("");
        discountVo.setPerform("1");
        discountVo.setVariantCode("UC51");
        discountVo.setCompanyCn("");
        String json  = JSON.toJSONString(discountVo);
        mvc.perform(MockMvcRequestBuilders.post("/speciaDiscount/getDisc")
        .content(json.getBytes())
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}