package com.bmw.ssa.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bmw.ssa.constants.Constants;
import com.bmw.ssa.vo.specialCase.SpecialCaseVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.File;
import java.io.FileInputStream;

import static org.junit.Assert.*;

/**
 * Created by Administrator on 2019/9/23 0023.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SpecialCaseControllerTest {
    @Autowired
    MockMvc mvc;

    @Test
    public void testInsertSepc() throws Exception {
        SpecialCaseVo specialCaseVo = new SpecialCaseVo();
        specialCaseVo.setSctype(Constants.SPECIALCASE_VIN);
        specialCaseVo.setScname("测送");
        specialCaseVo.setScstatus("01");
        specialCaseVo.setSccomment1("121");
        specialCaseVo.setSccomment2("aads");
        specialCaseVo.setSccomment3("sfdsf");
        specialCaseVo.setAsano("");
        String json = JSON.toJSONString(specialCaseVo);
        File file = new File("D:\\var\\vinDatas.csv");
        mvc.perform(MockMvcRequestBuilders.fileUpload("/specialCase/insertSepc").file(new MockMultipartFile("excel", "test1.xlsx", "multipart/form-data", new FileInputStream(new File("D:/var/test1.xlsx"))))
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void editTest()throws Exception{
        SpecialCaseVo specialCaseVo = new SpecialCaseVo();
        specialCaseVo.setSctype(Constants.SPECIALCASE_ASA);
        specialCaseVo.setScname("测送哈哈");
        specialCaseVo.setScstatus("01");
        specialCaseVo.setSccomment1("121");
        specialCaseVo.setSccomment2("aads");
        specialCaseVo.setSccomment3("sfdsf");
        specialCaseVo.setScno("SC201900126");
        String json = JSON.toJSONString(specialCaseVo);
        mvc.perform(MockMvcRequestBuilders.post("/specialCase/edit")
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void detailTest() throws Exception{
        SpecialCaseVo specialCaseVo = new SpecialCaseVo();
        specialCaseVo.setScno("SC201900009");
        String json = JSON.toJSONString(specialCaseVo);
        mvc.perform(MockMvcRequestBuilders.post("/specialCase/detail")
                .content(json.getBytes())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void pageListTest()throws Exception{
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageNum",1);
        jsonObject.put("pageSize",10);
        String json = JSON.toJSONString(jsonObject);
        mvc.perform(MockMvcRequestBuilders.get("/specialCase/getList")
                .param("pageNum","1")
                .param("pageSize","10")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}