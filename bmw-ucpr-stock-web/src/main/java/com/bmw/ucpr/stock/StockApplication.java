package com.bmw.ucpr.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock
 *
 * @author Bruce Maa
 * @since 2019-07-11.17:33
 */
@SpringBootApplication
@EnableFeignClients("com.bmw")
public class StockApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockApplication.class, args);
    }
}
