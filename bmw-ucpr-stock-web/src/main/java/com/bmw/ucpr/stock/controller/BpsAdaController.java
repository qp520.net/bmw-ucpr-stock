package com.bmw.ucpr.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ucpr.stock.entity.BpsAdaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAdaSearchVo;
import com.bmw.ucpr.stock.entity.BpsAdaVo;
import com.bmw.ucpr.stock.service.BpsAdaService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/bpsAda")
public class BpsAdaController extends BaseController {
	
	private final BpsAdaService bpsAdaService;
	
	private BpsAdaSearchVo searchCondition = new BpsAdaSearchVo();
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAdaList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaPage(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               				@RequestParam(required = false, defaultValue = "10") Long pageSize,
                               				BpsAdaSearchVo searchCondition) {
        
    	IPage<BpsAdaVo> bpsAdaPage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsAdaService.getBpsAdaPage(bpsAdaPage, searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/supplierList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getSupplierList(BpsAdaSearchVo searchCondition) {
    	
        return selectResponse(bpsAdaService.getSupplierList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/OCUTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getOCUTypeList(BpsAdaSearchVo searchCondition) {
    	
        return selectResponse(bpsAdaService.getOCUTypeList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/ecodeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getEcodeList(BpsAdaSearchVo searchCondition) {
    	
        return selectResponse(bpsAdaService.getEcodeList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAdatDetail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdatDetail(BpsAdaSearchVo searchCondition) {
		
		/*searchCondition.setLanguage(userBean.getLocale());
		searchCondition.setUcvin(selectBpsSieVo.getUcvin());
		searchCondition.setCustomerid(userBean.getCustomerid());
		String vinValid = bpsAdaService.getBpsAdaVinValid(searchCondition);
		if ("1".equals(vinValid)) {
			return errorResponse("该车为平行进口车或者不是宝马正确车架号，不能作为认证二手车进行库存维护和申请");
		} else {
			String applyValid = bpsAdaService.getBpsAdaApplyValid(searchCondition);
			if ("Y".equals(applyValid)) {
				return errorResponse("目标");
			} else {
				String dateValid = bpsAdaService.getBpsAdaApplyDateValid(searchCondition);
				if ("N".equals(dateValid)) {
					return selectResponse(bpsAdaService.getBpsAdaDetail(searchCondition));
				}
			}
			return errorResponse("失败");
		}	*/	
		return selectResponse(bpsAdaService.getBpsAdaDetail(searchCondition));
    }
	
	/*@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAdaHistory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaPageHistory(BpsAdaSearchVo searchCondition) { 
		
        return selectResponse(bpsAdaService.getBpsAdaHistory(searchCondition));
    }*/
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAdaPageHistory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaPageHistory(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               				@RequestParam(required = false, defaultValue = "10") Long pageSize,
                               				BpsAdaSearchVo searchCondition) {
        
    	IPage<BpsAdaDetailVo> bpsAdaPageHistory = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsAdaService.getBpsAdaPageHistory(bpsAdaPageHistory, searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initSourceList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initSourceList(BpsAdaSearchVo searchCondition) { 
		searchCondition.setCategory("42");
        return selectResponse(bpsAdaService.getLookUpList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initPlateTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initPlateTypeList(BpsAdaSearchVo searchCondition) { 
		searchCondition.setCategory("32");
		return selectResponse(bpsAdaService.getLookUpList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initCarTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initCarTypeList(BpsAdaSearchVo searchCondition) { 
		searchCondition.setCategory("35");
		return selectResponse(bpsAdaService.getLookUpList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initUsedCarConditionList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initUsedCarConditionList(BpsAdaSearchVo searchCondition) { 
		searchCondition.setCategory("30");
		return selectResponse(bpsAdaService.getLookUpList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initSaleTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initSaleTypeList(BpsAdaSearchVo searchCondition) { 
		searchCondition.setCategory("31");
		return selectResponse(bpsAdaService.getLookUpList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initCoveredByNcWarrtList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initCoveredByNcWarrtList(BpsAdaSearchVo searchCondition) { 
		searchCondition.setCategory("33");
		return selectResponse(bpsAdaService.getLookUpList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initWarr3partWarrorWarrList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initWarr3partWarrorWarrList(BpsAdaSearchVo searchCondition) { 
		searchCondition.setCategory("34");
		return selectResponse(bpsAdaService.getLookUpList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAdaFile", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaFile(BpsAdaSearchVo searchCondition) { 
		
        return selectResponse(bpsAdaService.getBpsAdaFile(searchCondition));
    }
	//TODO
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/BpsAdaApplyValid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaApplyValid(BpsAdaSearchVo bpsAdaSearchVo) { 
		
        return selectResponse(bpsAdaService.getBpsAdaApplyValid(bpsAdaSearchVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAdaApplyDateValid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaApplyDateValid(BpsAdaSearchVo bpsAdaSearchVo) { 
		
        return selectResponse(bpsAdaService.getBpsAdaApplyDateValid(bpsAdaSearchVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAdaVinValid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaVinValid(BpsAdaSearchVo bpsAdaSearchVo) { 
		
        return selectResponse(bpsAdaService.getBpsAdaVinValid(bpsAdaSearchVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/needextendedwarr", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getneedextendedwarr(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getneedextendedwarr(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/warrucwarrenddate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getwarrucwarrenddate(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getwarrucwarrenddate(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/warrendmileage", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getwarrendmileage(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getwarrendmileage(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/ncwarrdays", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getncwarrdays(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getncwarrdays(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/totalstockcost", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity gettotalstockcost(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.gettotalstockcost(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockgrossmargin1", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getstockgrossmargin1(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getstockgrossmargin1(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockgrossmargin2", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getstockgrossmargin2(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getstockgrossmargin2(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockgrossmarginrate1", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getstockgrossmarginrate1(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getstockgrossmarginrate1(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockgrossmarginrate2", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getstockgrossmarginrate2(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getstockgrossmarginrate2(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockcommission", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getstockcommission(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getstockcommission(bpsAdaDetailVo));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockcommissionrate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getstockcommissionrate(BpsAdaDetailVo bpsAdaDetailVo) { 
		
        return selectResponse(bpsAdaService.getstockcommissionrate(bpsAdaDetailVo));
    }
	
	@PostMapping(value = "/updateBpsAda", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void updateBpsAda(@RequestBody BpsAdaDetailVo bpsAdaDetailVo) {
		bpsAdaService.updateBpsAda(bpsAdaDetailVo);
		if(!bpsAdaDetailVo.getStatus().equals("05")) {
			//bpsAdaDetailVo.setUser(userBean.getUserName());
			bpsAdaService.submitBpsAda(bpsAdaDetailVo);
		}
		
	}
	
	@PostMapping(value = "/submitBpsAda", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void submitBpsAda(@RequestBody BpsAdaDetailVo bpsAdaDetailVo) {	
		
		/*double checkmargin = Double.valueOf(bpsAdaService.getstockgrossmarginrate1(bpsAdaDetailVo));
		if (checkmargin > 15 || checkmargin < -5) {
			return;
		}*/
		bpsAdaDetailVo.setUser("aa");
		bpsAdaService.updateBpsAda(bpsAdaDetailVo);
		bpsAdaService.submitBpsAda(bpsAdaDetailVo);
	}
	
	//凭证调用
	@PostMapping(value = "/updateBpsAdaCode", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void updateBpsAdaCode(@RequestBody BpsAdaDetailVo bpsAdaDetailVo) {
		bpsAdaService.updateBpsAdaCode(bpsAdaDetailVo);;
	}
	//凭证调用
	@PostMapping(value = "/submitBpsAdaCode", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void submitBpsAdaCode(@RequestBody BpsAdaDetailVo bpsAdaDetailVo) {
		bpsAdaService.submitBpsAdaCode(bpsAdaDetailVo);
	}

}

