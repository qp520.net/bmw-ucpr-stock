package com.bmw.ucpr.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ucpr.stock.entity.BpsAmaADetailVo;
import com.bmw.ucpr.stock.entity.BpsAmaASearchVo;
import com.bmw.ucpr.stock.entity.BpsAmaAVo;
import com.bmw.ucpr.stock.entity.BpsAmaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAmaSearchVo;
import com.bmw.ucpr.stock.entity.BpsAmaVo;
import com.bmw.ucpr.stock.service.BpsAmaAService;
import com.bmw.ucpr.stock.service.BpsAmaService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/bpsAma")
public class BpsAmaController extends BaseController {
	
	private final BpsAmaService bpsAmaService;
	
	private final BpsAmaAService bpsAmaAService;
	
	private BpsAmaSearchVo searchCondition = new BpsAmaSearchVo();
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaPage", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaPage(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsAmaSearchVo condition) {
        
    	IPage<BpsAmaVo> bpsAmaPage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsAmaService.getBpsAmaPage(bpsAmaPage, condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/supplierList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getSupplierList(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getSupplierList(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/OCUTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getOCUTypeList(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getOCUTypeList(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/ecodeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getEcodeList(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getEcodeList(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaDetail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaDetail(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getBpsAmaDetail(condition));
    }
	
	/*@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaHistory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaHistory(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getBpsAmaHistory(condition));
    }*/
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaHistoryPage", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaHistoryPage(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsAmaSearchVo condition) {
        
    	IPage<BpsAmaDetailVo> bpsAmaHistoryPage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsAmaService.getBpsAmaHistoryPage(bpsAmaHistoryPage, condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaFile", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaFile(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getBpsAmaFile(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaDocD", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaDocD(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getBpsAmaDocD(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaCanApply", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaCanApply(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getBpsAmaCanApply(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaApplyDate", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaApplyDate(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getBpsAmaApplyDate(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaApplyValid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaApplyValid(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getBpsAmaApplyValid(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaApplyAuditValid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaApplyAuditValid(BpsAmaSearchVo condition) {
        
        return selectResponse(bpsAmaService.getBpsAmaApplyAuditValid(condition));
    }
	//退回原因
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initReasonList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initReasonList(BpsAmaSearchVo condition) {
		condition.setCategory("13");
        return selectResponse(bpsAmaService.getLookUpList(condition));
    }
	
	@PostMapping(value = "/submitBpsAma", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void submitBpsAma(@RequestBody BpsAmaDetailVo bpsAmaDetailVo) {
		
		/*searchCondition.setUcvin(bpsAmaDetailVo.getUcvin());
		String applyvalid = bpsAmaService.getBpsAmaApplyValid(searchCondition);
		if (!applyvalid.equals("0")) {
			return;
		} else if (applyvalid.equals("0")) {
			String auditvalid = bpsAmaService.getBpsAmaApplyAuditValid(searchCondition);
			if (!auditvalid.equals("0")) {
				return;
			} else if (auditvalid.equals("0")){
				//bpsAmaDetailVo.setUser(userBean.getUserName());
				bpsAmaService.submitBpsAma(bpsAmaDetailVo);
			}
		}*/		
		bpsAmaService.submitBpsAma(bpsAmaDetailVo);
	}
	
	@PostMapping(value = "/submitBpsAmaBatch", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void submitBpsAmaBatch(@RequestBody BpsAmaDetailVo bpsAmaDetailVo) {
		bpsAmaService.submitBpsAmaBatch(bpsAmaDetailVo);
	}
	
	@PostMapping(value = "/submitBpsAmaCode", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void submitBpsAmaCode(@RequestBody BpsAmaDetailVo bpsAmaDetailVo) {
		bpsAmaService.submitBpsAmaCode(bpsAmaDetailVo);
	}
	
	@PostMapping(value = "/doReset", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void doReset(@RequestBody BpsAmaDetailVo bpsAmaDetailVo) {
		bpsAmaDetailVo.setReason1("2");
		bpsAmaService.updateBpsAma(bpsAmaDetailVo);
		bpsAmaService.submitBpsAma(bpsAmaDetailVo);
	}
	
	
	

	//amaa TODO
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaAPage", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaAPage(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               			 @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               			 BpsAmaASearchVo condition) {
        
    	IPage<BpsAmaAVo> bpsAmaAPage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsAmaAService.getBpsAmaAPage(bpsAmaAPage, condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/policyList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getPolicyList(BpsAmaASearchVo condition) {
        
        return selectResponse(bpsAmaAService.getPolicyList(condition));
    }
	//TODO
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaADetail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaADetail(BpsAmaASearchVo condition) {
        
        return selectResponse(bpsAmaAService.getBpsAmaADetail(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/BpsAmaAHistory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaAHistory(BpsAmaASearchVo condition) {
        
        return selectResponse(bpsAmaAService.getBpsAmaAHistory(condition));
    }
	//TODO
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsAmaApplyResetValid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAmaApplyResetValid(BpsAmaASearchVo condition) {
        
        return selectResponse(bpsAmaAService.getBpsAmaApplyResetValid(condition));
    }
	
	@PostMapping(value = "/updateBpsAmaA", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void updateBpsAmaA(@RequestBody BpsAmaADetailVo bpsAmaADetailVo) {
		bpsAmaAService.updateBpsAmaA(bpsAmaADetailVo);
	}
	
	@PostMapping(value = "/submitBpsAmaA", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void submitBpsAmaA(@RequestBody BpsAmaADetailVo bpsAmaADetailVo) {
		bpsAmaAService.submitBpsAmaA(bpsAmaADetailVo);
	}
	
}
