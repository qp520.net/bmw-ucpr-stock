package com.bmw.ucpr.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ucpr.stock.entity.BpsAdaDetailVo;
import com.bmw.ucpr.stock.entity.BpsEcaDetailVo;
import com.bmw.ucpr.stock.entity.BpsEcaSearchVo;
import com.bmw.ucpr.stock.entity.BpsEcaVo;
import com.bmw.ucpr.stock.service.BpsEcaService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/bpsEca")
public class BpsEcaController extends BaseController {
	
	private final BpsEcaService bpsEcaService;
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsEcaPage", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsEcaPage(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               				@RequestParam(required = false, defaultValue = "10") Long pageSize,
                               				BpsEcaSearchVo condition) {
        
    	IPage<BpsEcaVo> bpsEcaPage = new Page<>(pageNumber, pageSize);
    	condition.setUsername("axg3794");
        return pageResponse(bpsEcaService.getBpsEcaPage(bpsEcaPage, condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsEcaDetail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsEcaDetail(BpsEcaSearchVo condition) { 
		
        return selectResponse(bpsEcaService.getBpsEcaDetail(condition));
    }
	
	/*@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsEcaHistory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsEcaHistory(BpsEcaSearchVo condition) { 
		
        return selectResponse(bpsEcaService.getBpsEcaHistory(condition));
    }*/
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsEcaHistoryPage", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsEcaHistoryPage(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               				@RequestParam(required = false, defaultValue = "10") Long pageSize,
                               				BpsEcaSearchVo condition) {
        
    	IPage<BpsEcaDetailVo> bpsEcaHistoryPage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsEcaService.getBpsEcaHistoryPage(bpsEcaHistoryPage, condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/bpsEcaFile", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsEcaFile(BpsEcaSearchVo condition) { 
		
        return selectResponse(bpsEcaService.getBpsEcaFile(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initFileValidList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initFileValidList(BpsEcaSearchVo condition) { 
		condition.setCategory("11");
        return selectResponse(bpsEcaService.getLookUpList(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initResetReasonList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initResetReasonList(BpsEcaSearchVo condition) { 
		condition.setCategory("13");
        return selectResponse(bpsEcaService.getLookUpList(condition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/initRejectReasonList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initRejectReasonList(BpsEcaSearchVo condition) { 
		condition.setCategory("14");
        return selectResponse(bpsEcaService.getLookUpList(condition));
    }
	
	@PostMapping(value = "/updateBpsEca", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void updateBpsEca(@RequestBody BpsEcaDetailVo bpsEcaDetailVo) {
		bpsEcaService.updateBpsEca(bpsEcaDetailVo);
	}
	
	//审批通过
	@PostMapping(value = "/submitEca", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void submitEca(@RequestBody BpsEcaDetailVo bpsEcaDetailVo) {
		//bpsEcaDetailVo.setUser("");
		if(bpsEcaDetailVo.getVstatus().equals("10")) {
			bpsEcaService.submitBpsEcaBatch(bpsEcaDetailVo);
		}
		if(bpsEcaDetailVo.getVstatus().equals("15")) {
			bpsEcaService.submitBpsEca(bpsEcaDetailVo);
		}
		if(bpsEcaDetailVo.getVstatus().equals("00")) {
			bpsEcaService.submitStockEca(bpsEcaDetailVo);
		}
	}
	
	//审批退回
	@PostMapping(value = "/doResetEca", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void doResetEca(@RequestBody BpsEcaDetailVo bpsEcaDetailVo) {
		
		/*if(bpsEcaDetailVo.getComments().equals("11") || bpsEcaDetailVo.getComments() == null) {
			return "请输入备注内容";
		}*/
		bpsEcaDetailVo.setReason1("2");
		bpsEcaService.updateBpsEca(bpsEcaDetailVo);
		//bpsEcaDetailVo.setUser("");
		if(bpsEcaDetailVo.getVstatus().equals("10")) {
			bpsEcaService.submitBpsEcaBatch(bpsEcaDetailVo);
		}
		if(bpsEcaDetailVo.getVstatus().equals("15")) {
			bpsEcaService.submitBpsEca(bpsEcaDetailVo);
		}
		if(bpsEcaDetailVo.getVstatus().equals("00")) {
			bpsEcaService.submitStockEca(bpsEcaDetailVo);
		}
		
	}
	
	//审批拒绝
	@PostMapping(value = "/doRejectEca", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void doRejectEca(@RequestBody BpsEcaDetailVo bpsEcaDetailVo) {
		
		/*if(bpsEcaDetailVo.getComments().equals("3") || bpsEcaDetailVo.getComments() == null) {
			return "请输入备注内容";
		}*/
		bpsEcaDetailVo.setReason1("3");
		bpsEcaService.updateBpsEca(bpsEcaDetailVo);
		//bpsEcaDetailVo.setUser("");
		if(bpsEcaDetailVo.getVstatus().equals("10")) {
			bpsEcaService.submitBpsEcaBatch(bpsEcaDetailVo);
		}
		if(bpsEcaDetailVo.getVstatus().equals("15")) {
			bpsEcaService.submitBpsEca(bpsEcaDetailVo);
		}
		if(bpsEcaDetailVo.getVstatus().equals("00")) {
			bpsEcaService.submitStockEca(bpsEcaDetailVo);
		}
	}
	
	@PostMapping(value = "/submitBpsEcaBatch", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void submitBpsEcaBatch(@RequestBody BpsEcaDetailVo bpsEcaDetailVo) {
		bpsEcaService.submitBpsEcaBatch(bpsEcaDetailVo);
	}
	
	@PostMapping(value = "/submitBpsEcaCode", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void submitBpsEcaCode(@RequestBody BpsEcaDetailVo bpsEcaDetailVo) {
		bpsEcaService.submitBpsEcaCode(bpsEcaDetailVo);
	}

}
