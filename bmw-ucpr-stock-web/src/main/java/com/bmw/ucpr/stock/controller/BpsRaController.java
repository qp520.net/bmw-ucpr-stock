package com.bmw.ucpr.stock.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ucpr.stock.entity.BpsRaDetailVo;
import com.bmw.ucpr.stock.entity.BpsRaSearchVo;
import com.bmw.ucpr.stock.service.BpsRaService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月7日 上午11:19:15 

* 类说明 

*/
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/bpsRa")
public class BpsRaController extends BaseController{
	
	private final BpsRaService bpsraService;
	
	/**
	 * @author zlk
	 * selectitem  查询---Supplier
	 * 
	 */
	
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getSupplierList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getSupplierList(BpsRaSearchVo bpsraSearchVo) {
        
        return selectResponse(bpsraService.getSupplierList(bpsraSearchVo));
    }
    
	/**
	 * @author zlk
	 * selectitem  查询---Status
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getStatusList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getStatusList(BpsRaSearchVo bpsraSearchVo) {
        
        return selectResponse(bpsraService.getStatusList(bpsraSearchVo));
    }
	
    
	/**
	 * @author zlk
	 * selectitem  查询---CbuNo
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getCbuNoList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getCbuNoList(BpsRaSearchVo bpsraSearchVo) {
    
    	
    	return selectResponse(bpsraService.getCbuNoList(bpsraSearchVo));
    }
    
    
	/**
	 * @author zlk
	 * selectitem  查询---CkdNo
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getCkdNoList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getCkdNoList(BpsRaSearchVo bpsraSearchVo) {
        
        return selectResponse(bpsraService.getCkdNoList(bpsraSearchVo));
    
}
    
	/**
	 * @author zlk
	 * selectitem  查询---ecode
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getEcodeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getEcodeList(BpsRaSearchVo bpsraSearchVo) {
        
        return selectResponse(bpsraService.getEcodeList(bpsraSearchVo));
    
}
    
	
	 /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param demo       查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity page(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsRaSearchVo bpsRaSearchVo) {
        
    	if (bpsRaSearchVo.getRf()!=null&&bpsRaSearchVo.getRt()==null) {
			Date fromHandoverDate = bpsRaSearchVo.getRf();
			Date currentDate = stringToDate(getCurrentDateStr("yyyy-MM-dd 00:00:00"), "yyyy-MM-dd HH:mm:ss");
			Date addOneYear = addOneYear(fromHandoverDate);
			boolean isTrue = currentDate.after(addOneYear);
		
		}
		if (bpsRaSearchVo.getRf()!=null&&bpsRaSearchVo.getRt()!=null) {
			Date fromHandoverDate = bpsRaSearchVo.getRf();
			Date toHandoverDate = bpsRaSearchVo.getRt();
			Date addOneYear = addOneYear(fromHandoverDate);
			boolean isTrue = toHandoverDate.after(addOneYear);
		
		}
    	
    	
    	IPage<?> bpsRaPage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsraService.getBpsRaPage(bpsRaPage, bpsRaSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsRaDetail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getgetBpsRaDetail(BpsRaSearchVo bpsRaSearchVo) {
    	
        return selectResponse(bpsraService.getBpsRaDetail(bpsRaSearchVo));
    }
    
    
    @PostMapping(value = "/submitBpsRa", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void submitBpsRa(@RequestBody BpsRaDetailVo bpsRaDetailVo) {
    	bpsRaDetailVo.setNcvin(bpsRaDetailVo.getVstatus());

		
		//bpsRaService.submitBpsRa(getBpsRaDetail);
    	bpsraService.submitBpsRa(bpsRaDetailVo);

    }
    /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param bpsraSearchVo       查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsRaHistory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsSieList(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsRaSearchVo bpsraSearchVo) {
        
    	IPage<?> siePage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsraService.getBpsRaHistory(siePage, bpsraSearchVo));
    }
    
	/**
	 * 取得系统当前时间字符串
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurrentDateStr(String format) {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(format);
		String time = df.format(date);
		return time;
	}
	
    /**
     * 为date1加一年
     * @param date1
     * @return
     */
    public static Date addOneYear(Date date1){
	    Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date1);
		rightNow.add(Calendar.YEAR,1);//日期加1年
		Date dt1=rightNow.getTime();
		return dt1;
    }
    
	/**
	 * 字符串转换为日期
	 * 
	 * @param dateStr
	 * @param format
	 * @return
	 */
	public static Date stringToDate(String dateStr, String format) {
		if (dateStr == null || "".equals(dateStr)) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		java.util.Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
}
