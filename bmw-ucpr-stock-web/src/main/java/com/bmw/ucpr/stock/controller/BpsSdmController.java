package com.bmw.ucpr.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ucpr.stock.entity.BpsSieDetailVo;
import com.bmw.ucpr.stock.entity.BpsSieSearchVo;
import com.bmw.ucpr.stock.entity.StockApproveVo;
import com.bmw.ucpr.stock.entity.StockConditionEntity;
import com.bmw.ucpr.stock.service.BpsSdmService;
import com.bmw.ucpr.stock.service.BpsSieService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.controller
 *
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/bpssdm")
public class BpsSdmController extends BaseController {

    private final BpsSdmService bpsSdmService;
    
    private final BpsSieService bpsSieService;
    
    /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param demo       查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getStockList(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               StockConditionEntity stockConditionEntity) {
        
    	IPage<?> demoPage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsSdmService.getStockList(demoPage, stockConditionEntity));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getSupplierList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getSupplierList(BpsSieSearchVo bpsSieSearchVo) {
        
        return selectResponse(bpsSieService.getSupplierList(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getEcodeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getEcodeList(BpsSieSearchVo bpsSieSearchVo) {
        
        return selectResponse(bpsSieService.getEcodeList(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getDelComment", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getDelComment(StockConditionEntity stockConditionEntity) {
    	
        return selectResponse(bpsSdmService.getDelComment(stockConditionEntity));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getReasonList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getReasonList(BpsSieSearchVo bpsSieSearchVo) {
    	bpsSieSearchVo.setCategory("49");
        
        return selectResponse(bpsSieService.getLookUpList(bpsSieSearchVo));
    }

    @PostMapping(value = "/doApproveApplication", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void doApproveApplication(@RequestBody StockApproveVo stockApproveVo) {
        
    	bpsSdmService.approveProcedure(stockApproveVo);
		bpsSdmService.updateApprove(stockApproveVo);
		bpsSdmService.updateHistory(stockApproveVo);
    }
    
}
