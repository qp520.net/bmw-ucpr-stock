package com.bmw.ucpr.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ucpr.stock.entity.BpsSieDetailVo;
import com.bmw.ucpr.stock.entity.BpsSieSearchVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.service.BpsSieService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.controller
 *
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/bpssie")
public class BpsSieController extends BaseController {

    private final BpsSieService bpsSieService;
    
    /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param demo       查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity page(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsSieSearchVo bpsSieSearchVo) {
        
    	IPage<SelectItem> demoPage = new Page<SelectItem>(pageNumber, pageSize);
        
        return pageResponse(bpsSieService.getSupplierPage(demoPage, bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity select(BpsSieSearchVo bpsSieSearchVo) {
        
        return selectResponse(bpsSieService.getSupplierList(bpsSieSearchVo));
    }
    
    /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param demo       查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsSieList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsSieList(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsSieSearchVo bpsSieSearchVo) {
        
    	IPage<?> siePage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsSieService.getBpsSiePage(siePage, bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getSupplierList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getSupplierList(BpsSieSearchVo bpsSieSearchVo) {
        
        return selectResponse(bpsSieService.getSupplierList(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getOCUTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getOCUTypeList(BpsSieSearchVo bpsSieSearchVo) {
        
        return selectResponse(bpsSieService.getOCUTypeList(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getEcodeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getEcodeList(BpsSieSearchVo bpsSieSearchVo) {
        
        return selectResponse(bpsSieService.getEcodeList(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getCarTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getCarTypeList(BpsSieSearchVo bpsSieSearchVo) {
    	bpsSieSearchVo.setCategory("35");
        
        return selectResponse(bpsSieService.getLookUpList(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getPlateTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getPlateTypeList(BpsSieSearchVo bpsSieSearchVo) {
    	bpsSieSearchVo.setCategory("32");
        
        return selectResponse(bpsSieService.getLookUpList(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/initSaleTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity initSaleTypeList(BpsSieSearchVo bpsSieSearchVo) {
    	
        return selectResponse(bpsSieService.initSaleTypeListByOcu(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getUsedCarConditionList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getUsedCarConditionList(BpsSieSearchVo bpsSieSearchVo) {
    	bpsSieSearchVo.setCategory("30");
        
        return selectResponse(bpsSieService.getLookUpList(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getSaleTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getSaleTypeList(BpsSieSearchVo bpsSieSearchVo) {
    	bpsSieSearchVo.setCategory("31");
        
        return selectResponse(bpsSieService.getLookUpList(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsAdaHistory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaHistory(BpsSieSearchVo bpsSieSearchVo) {
    	
        return selectResponse(bpsSieService.getBpsAdaHistory(bpsSieSearchVo));
    }
    
    /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param demo       查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsAdaHistoryList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaHistoryList(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsSieSearchVo bpsSieSearchVo) {
        
    	IPage<?> siePage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(bpsSieService.getBpsAdaHistoryPage(siePage, bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsAdaFile", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsAdaFile(BpsSieSearchVo bpsSieSearchVo) {
    	
        return selectResponse(bpsSieService.getBpsAdaFile(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsSieDetail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsSieDetail(BpsSieSearchVo bpsSieSearchVo) {
    	
        return selectResponse(bpsSieService.getBpsSieDetail(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsSieDetailSec", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsSieDetailSec(BpsSieSearchVo bpsSieSearchVo) {
    	
        return selectResponse(bpsSieService.getBpsSieDetailSec(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsSieVinValid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsSieVinValid(BpsSieSearchVo bpsSieSearchVo) {
    	
        return selectResponse(bpsSieService.getBpsSieVinValid(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsSieValid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsSieValid(BpsSieSearchVo bpsSieSearchVo) {
    	
        return selectResponse(bpsSieService.getBpsSieValid(bpsSieSearchVo));
    }
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsSieDateValid", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsSieDateValid(BpsSieSearchVo bpsSieSearchVo) {
    	
        return selectResponse(bpsSieService.getBpsSieDateValid(bpsSieSearchVo));
    }
    
    @PostMapping(value = "/updateOcutype", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateOcutype(@RequestBody BpsSieDetailVo bpsSieDetailVo) {
        bpsSieService.updateOcutype(bpsSieDetailVo);
    }
    
    @PostMapping(value = "/updateBpsSie", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateBpsSie(@RequestBody BpsSieDetailVo bpsSieDetailVo) {
        bpsSieService.updateBpsSie(bpsSieDetailVo);
    }

    @PostMapping(value = "/submitBpsSie", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void submitBpsSie(@RequestBody BpsSieDetailVo bpsSieDetailVo) {
        bpsSieService.updateBpsSie(bpsSieDetailVo);
        bpsSieService.updateBpsSieCode(bpsSieDetailVo);
		bpsSieService.submitBpsSie(bpsSieDetailVo);

    }
    
}
