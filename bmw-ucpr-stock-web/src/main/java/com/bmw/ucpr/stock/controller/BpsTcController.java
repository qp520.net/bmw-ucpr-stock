package com.bmw.ucpr.stock.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.text.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ucpr.stock.entity.BpsTcDetailVo;
import com.bmw.ucpr.stock.entity.BpsTcSearchVo;
import com.bmw.ucpr.stock.service.BpsTcService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/** 

* @author 作者 Your-Name: zhoulukai

* @version 创建时间：2019年8月5日 上午11:00:40 

* 类说明 

*/

@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/bpsTc")
public class BpsTcController extends BaseController{

	private final BpsTcService bpsTcService;
	/**
	 * @author zlk
	 * selectitem  查询---Supplier
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getSupplierList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getSupplierList(BpsTcSearchVo psTcsearchVo) {
    	
        return selectResponse(bpsTcService.getSupplierList(psTcsearchVo));
    }
    
    
	/**
	 * @author zlk
	 * selectitem  查询---Status
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getStatusList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getStatusList(BpsTcSearchVo bpsTcsearchVo) {
        
        return selectResponse(bpsTcService.getStatusList(bpsTcsearchVo));
    }
	
    
	/**
	 * @author zlk
	 * selectitem  查询---CbuNo
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getCbuNoList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getCbuNoList(BpsTcSearchVo bpsTcsearchVo) {
        
        return selectResponse(bpsTcService.getCbuNoList(bpsTcsearchVo));
    }
    
    
	/**
	 * @author zlk
	 * selectitem  查询---CkdNo
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getCkdNoList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getCkdNoList(BpsTcSearchVo bpsTcsearchVo) {
        
        return selectResponse(bpsTcService.getCkdNoList(bpsTcsearchVo));
}
    
	/**
	 * @author zlk
	 * selectitem  查询---ecode
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getEcodeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getEcodeList(BpsTcSearchVo bpsTcsearchVo) {
    	
        return selectResponse(bpsTcService.getEcodeList(bpsTcsearchVo));
}
    
    
    /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param BpsTcSearchVo        查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsTcpage(
    		                   @RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsTcSearchVo BpsTcSearchVo) {
    	
    	if (BpsTcSearchVo.getHf() != null && BpsTcSearchVo.getHt() == null) {
			Date fromHandoverDate = BpsTcSearchVo.getHf();
			Date currentDate = stringToDate(
					getCurrentDateStr("YYYY-mm-dd 00:00:00"),
					"YYYY-mm-dd HH:ii:ss");
			Date addOneYear = addOneYear(fromHandoverDate);
			boolean isTrue = currentDate.after(addOneYear);

		}
		if (BpsTcSearchVo.getHf() != null && BpsTcSearchVo.getHt() != null) {
			Date fromHandoverDate = BpsTcSearchVo.getHf();
			Date toHandoverDate = BpsTcSearchVo.getHt();
			Date addOneYear = addOneYear(fromHandoverDate);
			boolean isTrue = toHandoverDate.after(addOneYear);
		
		}


		List<String> vinList = new ArrayList<String>();

		if (BpsTcSearchVo.getUcvins() != null && BpsTcSearchVo.getUcvins().length()>0) {
			String ucvins = BpsTcSearchVo.getUcvins();
			String[] ucStrings = ucvins.split("\r\n");
			for (int i = 0; i < ucStrings.length; i++) {
				vinList.add(ucStrings[i]);
			}
		}

		BpsTcSearchVo.setUcvinlistwrite(vinList);
    	IPage<?> bpstcPage = new Page<>(pageNumber, pageSize);
        return pageResponse(bpsTcService.getBpsTcList(bpstcPage,BpsTcSearchVo));
    }
    
   
    /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param BpsTcSearchVo       查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
    @GetMapping(value = "/getBpsTchistory",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsTchistory(
    		                   @RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsTcSearchVo bpsTcsearchVo) {
    	
    	IPage<?> bpstcage = new Page<>(pageNumber, pageSize);
        return pageResponse(bpsTcService.getBpsTcHistory(bpstcage,bpsTcsearchVo));
    } 
    

    
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsTcFile", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsTcFile(BpsTcSearchVo bpsTcSearchVo) {
    	
        return selectResponse(bpsTcService.getBpsTcFile(bpsTcSearchVo));
    }
	
	

    /**
     * @author zhoulukai--查询车辆信息详情详情
     *
     */
     @SuppressWarnings("rawtypes")
	 @GetMapping(value = "/getBpsTcDetail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsTcDetail(BpsTcSearchVo bpstcsearchvo) {
    	
        return selectResponse(bpsTcService.getBpsTcDetail(bpstcsearchvo));
    }
     
	
	
     /**
      * @author zhoulukai--车辆终止操作
      *
      */
     @PostMapping(value = "/submitbpsTc", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
     public void submitBpsSie(@RequestBody BpsTcDetailVo bpstcDetailVo) {
    	 bpsTcService.updateBpsTc(bpstcDetailVo);
    	 
    	 

 		if (null == bpstcDetailVo.getPolicyid()
 				|| "".equals(bpstcDetailVo.getPolicyid())) {
 			bpstcDetailVo.setPolicyid("");
 			bpsTcService.updateBpsTcCode(bpstcDetailVo);
 		} else {
 			 bpsTcService.submitBpsTc(bpstcDetailVo);
 		}
         
           

     }
	
 	/**
 	 * 字符串转换为日期
 	 * 
 	 * @param dateStr
 	 * @param format
 	 * @return
 	 */
 	public static Date stringToDate(String dateStr, String format) {
 		if (dateStr == null || "".equals(dateStr)) {
 			return null;
 		}
 		SimpleDateFormat sdf = new SimpleDateFormat(format);
 		java.util.Date date = null;
 		try {
 			date = sdf.parse(dateStr);
 		} catch (ParseException e) {
 			e.printStackTrace();
 		}
 		return date;
 	}
     
     
	/**
	 * 取得系统当前时间字符串
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurrentDateStr(String format) {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(format);
		String time = df.format(date);
		return time;
	}
	
    /**
     * 为date1加一年
     * @param date1
     * @return
     */
    public static Date addOneYear(Date date1){
	    Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date1);
		rightNow.add(Calendar.YEAR,1);//日期加1年
		Date dt1=rightNow.getTime();
		return dt1;
    }
    
}
