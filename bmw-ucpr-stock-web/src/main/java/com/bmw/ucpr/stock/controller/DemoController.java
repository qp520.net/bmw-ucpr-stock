package com.bmw.ucpr.stock.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.todo.api.TodoListApi;
import com.bmw.todo.dto.ToDoListDO;
import com.bmw.ucpr.stock.entity.Demo;
import com.bmw.ucpr.stock.service.DemoService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.controller
 *
 * @author Bruce Maa
 * @since 2019-07-10.16:06
 */
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/demo")
public class DemoController extends BaseController {

    private final DemoService demoService;
    
    private final TodoListApi todoListApi;
    
    @PostMapping("/test")
    public void test(@RequestBody ToDoListDO toDoList) {
    	Long value = todoListApi.saveTodo(toDoList);
    	System.out.println(value);
    }

    /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param demo       查询条件封装对象
     * @return  分页查询结果
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity page(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               Demo demo) {
        IPage<Demo> demoPage = new Page<>(pageNumber, pageSize);
        return pageResponse(demoService.page(demoPage));
    }

    /**
     * 单个查询
     * @param demoId 主键
     * @return  单个对象
     */
    @GetMapping(value = "/{demoId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity select(@PathVariable("demoId") Long demoId) {
        Demo demo = new Demo();
        demo.setDemoId(demoId);
        QueryWrapper<Demo> queryWrapper = new QueryWrapper<>(demo);
        return selectResponse(demoService.getOne(queryWrapper));
    }

    /**
     * 保存对象
     * @param demo 对象信息
     * @return  保存成功后的对象信息
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity save(@RequestBody Demo demo) {
        if (demoService.save(demo)) {
            return saveResponse(demo);
        }
        return errorResponse("保存失败");
    }

    /**
     * 更新对象字段
     * @param demoId 对象主键
     * @param demo   需要更新的字段
     * @return  更新是否成功
     */
    @PutMapping(value = "/{demoId}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity update(@PathVariable("demoId") Long demoId,
                                 @RequestBody Demo demo) {
        demo.setDemoId(demoId);
        if (demoService.updateById(demo)) {
            return updateResponse(null);
        }
        return errorResponse("更新失败");
    }

    /**
     * 删除对象(逻辑删除)
     * @param demoId 对象主键
     * @return  是否删除成功
     */
    @DeleteMapping(value = "/{demoId}")
    public ResponseEntity delete(@PathVariable("demoId") Long demoId) {
        if (demoService.removeById(demoId)) {
            return deleteResponse();
        }
        return errorResponse("删除失败");
    }

}
