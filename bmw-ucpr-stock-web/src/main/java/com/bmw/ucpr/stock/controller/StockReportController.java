package com.bmw.ucpr.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ucpr.stock.entity.BpsSieSearchVo;
import com.bmw.ucpr.stock.entity.OcuStockReportSearchVo;
import com.bmw.ucpr.stock.entity.OcuStockReportVo;
import com.bmw.ucpr.stock.service.StockReportService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/stockReport")
public class StockReportController extends BaseController {
	
	//分页数据列表
	private final StockReportService stockReportService;
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockReportList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getStockReportList(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               				@RequestParam(required = false, defaultValue = "10") Long pageSize,
                               				OcuStockReportSearchVo searchCondition) {
        
    	IPage<OcuStockReportVo> siePage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(stockReportService.getStockReportPage(siePage, searchCondition));
    }
	
	//单条数据信息
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockReportDetail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getStockReportDetail(OcuStockReportSearchVo searchCondition) {
    	
        return selectResponse(stockReportService.getStockReportDetail(searchCondition));
    }
	
	//供应
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/supplierList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getSupplierList(OcuStockReportSearchVo searchCondition) {
        
        return selectResponse(stockReportService.getSupplierList(searchCondition));
    }
	
	//cbu List
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/cbuList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getCbuList(OcuStockReportSearchVo searchCondition) {
        
        return selectResponse(stockReportService.getCbuList(searchCondition));
    }
	
	//ckd list
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/ckdList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getCkdList(OcuStockReportSearchVo searchCondition) {
        
        return selectResponse(stockReportService.getCkdList(searchCondition));
    }
	
	//区域
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/ecodeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getEcodeList(OcuStockReportSearchVo searchCondition) {
        
        return selectResponse(stockReportService.getEcodeList(searchCondition));
    }
	
	//UC类型
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/ocuTypeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getOCUTypeList(OcuStockReportSearchVo searchCondition) {
        
        return selectResponse(stockReportService.getOCUTypeList(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/pieChart", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getPieChart(OcuStockReportSearchVo searchCondition) {
        
        return selectResponse(stockReportService.getPieChart(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/barOrLineChart", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBarOrLineChart(OcuStockReportSearchVo searchCondition) {
        
        return selectResponse(stockReportService.getBarOrLineChart(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockReportHistory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getStockReportHistory(OcuStockReportSearchVo searchCondition) {
        
        return selectResponse(stockReportService.getStockReportHistory(searchCondition));
    }
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/stockReportFile", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getStockReportFile(OcuStockReportSearchVo searchCondition) {
        
        return selectResponse(stockReportService.getStockReportFile(searchCondition));
    }

}
