package com.bmw.ucpr.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.core.web.controller.BaseController;
import com.bmw.ucpr.stock.entity.BpsRaDetailVo;
import com.bmw.ucpr.stock.entity.BpsRaSearchVo;
import com.bmw.ucpr.stock.service.UcRaService;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/** 

* @author 作者 Your-Name: zhoulukai

* @version 创建时间：2019年8月6日 下午4:12:20 

* 类说明 

*/
@Slf4j
@RequiredArgsConstructor(onConstructor = @_({@Autowired}), access = AccessLevel.PUBLIC)
@RestController
@RequestMapping(value = "/ucRa")
public class UcRaController extends BaseController{

	private final UcRaService UcraService;	
	
	
	
	/**
	 * @author zlk
	 * selectitem  查询---Supplier
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getSupplierList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getSupplierList(BpsRaSearchVo bpsraSearchVo) {
        
        return selectResponse(UcraService.getSupplierList(bpsraSearchVo));
    }
    
    
    
	/**
	 * @author zlk
	 * selectitem  查询---Status
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getStatusList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getStatusList(BpsRaSearchVo bpsraSearchVo) {
    	
    	
        return selectResponse(UcraService.getStatusList(bpsraSearchVo));
    }
	
    
    
	/**
	 * @author zlk
	 * selectitem  查询---CbuNo
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getCbuNoList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getCbuNoList(BpsRaSearchVo bpsraSearchVo) {
        
        return selectResponse(UcraService.getCbuNoList(bpsraSearchVo));
    }
    
    
    
	/**
	 * @author zlk
	 * selectitem  查询---CkdNo
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getCkdNoList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getCkdNoList(BpsRaSearchVo bpsraSearchVo) {
        
        return selectResponse(UcraService.getCkdNoList(bpsraSearchVo));
    
}
    
    
    
	/**
	 * @author zlk
	 * selectitem  查询---ecode
	 * 
	 */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getEcodeList", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getEcodeList(BpsRaSearchVo bpsraSearchVo) {
        
        return selectResponse(UcraService.getEcodeList(bpsraSearchVo));
    
}
    
	
	
	 /**
     * 分页查询
     * @param pageNumber 当前页数
     * @parm pageSize   每页显示数量
     * @param BpsRaSearchVo       查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity page(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsRaSearchVo bpsraSearchVo) {
        
    	IPage<?> demoPage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(UcraService.getUcraPage(demoPage, bpsraSearchVo));
    }
	
    
    
    
    /**
     * 分页查询
     * @param pageNumber 当前页数
     * @param pageSize   每页显示数量
     * @param demo       查询条件封装对象
     * @return  分页查询结果
     */
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getBpsRaHistory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsSieList(@RequestParam(required = false, defaultValue = "1") Long pageNumber,
                               @RequestParam(required = false, defaultValue = "10") Long pageSize,
                               BpsRaSearchVo bpsraSearchVo) {
        
    	IPage<?> siePage = new Page<>(pageNumber, pageSize);
        
        return pageResponse(UcraService.getBpsRaHistory(siePage, bpsraSearchVo));
    }
    
    
    
    @SuppressWarnings("rawtypes")
	@GetMapping(value = "/getUcRaDetail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getBpsSieDetailSec(BpsRaSearchVo bpsraSearchVo) {
    	
        return selectResponse(UcraService.getUcRaDetail(bpsraSearchVo));
    }
    
    
    
    
    @PostMapping(value = "/updateUcRaDetail", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updateUcRa(@RequestBody BpsRaSearchVo bpsraSearchVo) {
        UcraService.updateUcRa(bpsraSearchVo);
    }

    
    
    @PostMapping(value = "/submitUcRa", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void submitUcRa(@RequestBody BpsRaDetailVo bpsraSearchVo) {
    	UcraService.submitUcRa(bpsraSearchVo);

    }
    
	
}
