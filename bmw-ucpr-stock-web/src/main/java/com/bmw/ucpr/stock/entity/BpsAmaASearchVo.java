package com.bmw.ucpr.stock.entity;

import java.util.Date;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Project：BMW UCDR Web Base
 * System：UCDR
 * Sub System：Admin
 * search condition vo
 * @author chongnan
 * 
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsAmaASearchVo extends BasePO {

	private String language;
	private String customerid;
	private String ucvin;
	private String supplier;
	private Date hf;
	private Date ht;
	private Date af;
	private Date at;
	private String ecode;
	
	private String id;
	private String ucvins;//VIN_7
	
	private String sequence;//DOC SEQUENCE
	private String doccontent;
	private String docmimetype;
	private String docfilename;
	private String username;
	//apply_date condition
	private String con;
	
	private String policy;
	private String cbuno;
	private String ckdno;
	
	private String policyid;
	
	private String category;
	
	
	
}
