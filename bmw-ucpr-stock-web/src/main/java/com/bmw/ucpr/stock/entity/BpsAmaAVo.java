package com.bmw.ucpr.stock.entity;

import java.util.Date;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Project：BMW UCDR Web Base
 * System：UCDR
 * Sub System：Admin
 * BpsAdaVo
 * @author chongnan
 * 
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsAmaAVo extends BasePO {

	private String id;
	private String status;
	private String vstatus;
	private String ucvin;
	private String vinverify;
	private String saletype;	
	private String cbuno;
	private String ckdno;	
	private String dealername;
	private String region;
	private String uccarseries;
	private String model;
	private String source;
	private String platetype;
	private String plateno;	
	private String bodycolor;
	private String interiorcolor;
	private Date productiondate;
	private Date ucfirstregistrationdate;
	private String ucmileage;
	private String newcarwarrdate;
	private String nscbba;
	private String cartype;
	private String ecode;
	private String warrcoveredbyncwarr;
	private String warr3partwarrorwarr;
	private String warrantyno;
	private String warrucwarrstartdate;
	private String warrucwarrenddate;
	private String warrstartmileage;
	private String warrendmileage;
	private String ownername;
	private String mobileno;
	private String regtransferdate;
	private String customerororgname;
	private String address;
	
	private String reservation1;
	private String reservation2;
	private String reservation3;
	private String reservation4;
	private String reservation5;
	private String reservation6;
	
	private Date stockstartdate;
	private Date stockenddate;
	private Date dmsstockdate;
	private String stockplace;
	private String registrationtransferfee;
	private String pointcheckfee;
	private String repairandmaintenancefee;
	private String beautyfee;
	private String salecommission;
	private String otherexpenses;
	private String warrantyfund;
	private String marketfee;
	private String purchase;
	
	private String productiondatestr;
	private String ucfirstregistrationdatestr;
	private String stockstartdatestr;
	private String stockenddatestr;
	private String dmsstockdatestr;
	//used for get bps_document_configration
	private Date applydate;
	
	private String vin7;
	//new add
	private String getlastchange;
	
	private String lastactiondate;
	
	private String policygrouptype;
	private String policygroupname;
	private String incentiveamount;
	
	private String policyid;
	
}
