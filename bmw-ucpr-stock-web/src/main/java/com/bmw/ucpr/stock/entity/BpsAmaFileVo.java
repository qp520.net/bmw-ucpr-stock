package com.bmw.ucpr.stock.entity;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Project：BMW UCDR Web Base
 * System：UCDR
 * Sub System：Admin
 * BpsAdaFileVo
 * @author chongnan
 * 
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsAmaFileVo extends BasePO {	

	private String fileid;
	private String filetitle;
	private String fileneed;
	private byte[] filecontent;
	private String filename;
	private String filetype;
	private String doctype;
	private String filevalid;
	private String filereason;
	
	private String filesequence;
	private boolean filevalids;
	
	private String ucvin;
	private String username;
	
		
	
}
