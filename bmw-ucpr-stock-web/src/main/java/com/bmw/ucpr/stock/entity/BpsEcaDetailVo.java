package com.bmw.ucpr.stock.entity;

import java.util.Date;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Project：BMW UCDR Web Base
 * System：UCDR
 * Sub System：Admin
 * BpsAdaDetailVo
 * @author chongnan
 * 
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsEcaDetailVo extends BasePO {

	private String id;//2
	private String status;//3
	private String vstatus;//used for 10 or 15 check
	private String ucvin;//1
	private String vinverify;//5
	private String saletype;//4	
	private String saletypename;
	private String cbuno;//6
	private String ckdno;//7	
	private String dealername;//8
	private String region;//9
	private String uccarseries;//10
	private String model;//11
	private String source;//12
	private String platetype;//66
	private String plateno;//13	
	private String bodycolor;//14
	private String interiorcolor;//15
	private Date productiondate;//16
	private Date ucfirstregistrationdate;//18
	private Date ucfirstregistration;//20
	private String ucmileage;//21
	private String newcarwarrdate;//22
	private String nscbba;//24
	private String cartype;//67
	private String ecode;//27
	private String warrcoveredbyncwarr;//114
	private String warr3partwarrorwarr;//116
	private String warrantyno;
	private Date warrucwarrstartdate;//89
	private Date warrucwarrenddate;//90
	private String warrstartmileage;//91
	private String checkwarrstartmileage;//92
	private String warrendmileage;//93
	private String checkwarrendmileage;//94
	private String ownername;//69
	private String mobileno;//23
	private Date regtransferdate;//33
	private String customerororgname;//25
	private String address;//26
	
	private String other;//28
	private String redbookcode;//29
	private String redbookretailprice;//30
	private String tradeinncvin;//31
	private String rbwhoesaleprice;//32
	private String checkncvin;//34
	private String ncvin;//35
	private String doc1yes;//36
	private String doc2yes;//37
	private String doc3yes;//38
	private String doc4yes;//39
	private String doc5yes;//40
	private String doc6yes;//41
	private String doc7yes;//42
	private String doc8yes;//43
	private String doc9yes;//44
	private String doc10yes;//45
	private String doc11yes;//46
	private String doc12yes;//47
	private String doc13yes;//48
	private String doc14yes;//49
	private String doc15yes;//50
	private String reason1;//51
	private String reason2;//52
	private String reason3;//53
	private String reason4;//54
	private String reason5;//55
	private String reason6;//56
	private String reason7;//57
	private String reason8;//58
	private String reason9;//59
	private String reason10;//60
	private String reason11;//61
	private String reason12;//62
	private String reason13;//63
	private String reason14;//64
	private String reason15;//65
	private String usedcarcondition;//68
	private String ncwarrtstartdate;//70
	private String needextendedwarr;//71
	
	//second page part
	private Date retailinvoicedate;//72
	private Date checkretailinvoice;//73
	private String finalretailprice;//74
	private String qualitycerificateno;//75
	private String usedcarwarrno;//76
	private String stockstmaptax;//77
	private String stockaddedtax;//78
	private String stockgrossmargin1;//79
	private String stockgrossmarginrate1;//80
	private String stockgrossmargin2;//81
	private String stockgrossmarginrate2;//82
	private String stockcommission;//83
	private String stockcommissionrate;//84
	private String salesconsultant;//85
	private String ucwarrstartmileage;//86
	private String ucwarrendmileage;//87
	private String warrwarrantyno;//88
	private String ageinmonth;//95
	
	private String reservation2;
	private String reservation3;
	private String reservation4;
	private String reservation5;
	private String reservation6;
	
	private Date stockstartdate;//96
	private Date stockenddate;
	private Date dmsstockdate;
	private String stockplace;//97
	private String stackdays;//98
	private String leasingfeeandinterest;//99
	private String registrationtransferfee;//100
	private String pointcheckfee;//101
	private String repairandmaintenancefee;//102
	private String beautyfee;//103
	private String salecommission;//104
	private String otherexpenses;//105
	private String warrantyfund;//106
	private String marketfee;//107
	private String purchase;//108
	private String totalstockcost;//109
	private String advisedpurchase;//110
	private String qualitycetificateno;//111
	private String bonusandserviceprice;//112
	private String checkwarrantyncdate;//113
	private String coveredbyncwarr;//115
	private Date ucwarrstartdate;//117
	private Date ucwarrenddate;//118
	private Date ncwarrstartdate;//119
	private Date ncwarrenddate;//120
	private String ncwarrdays;//121
	
	
	private String productiondatestr;//17
	private String ucfirstregistrationdatestr;//19
	private String stockstartdatestr;
	private String stockenddatestr;
	private String dmsstockdatestr;
	
	private String vin7;
	
	//history list
	private String time;
	private String getuser;
	private String action;
	private String result;
	private String comments;
	
	private String user;
	
	
}
