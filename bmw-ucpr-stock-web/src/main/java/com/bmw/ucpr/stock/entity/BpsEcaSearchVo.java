package com.bmw.ucpr.stock.entity;

import java.util.Date;
import java.util.List;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Project：BMW UCDR Web Base System：UCDR Sub System：Admin search condition vo
 * 
 * @author chongnan
 * 
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsEcaSearchVo extends BasePO {

	private String language;
	private String customerid;
	private String ucvin;
	private String supplier;
	private Date hf;
	private Date ht;
	private Date af;
	private Date at;
	private String ecode;

	private String id;
	private String ucvins;// VIN_7

	private String sequence;// DOC SEQUENCE
	private String doccontent;
	private String docmimetype;
	private String docfilename;
	private String username;
	// apply_date condition
	private String con;

	private String cbuno;
	private String ckdno;
	private List<String> ucvinlistwrite;
	private List<String> ucvinlistupload;
	
	private String status;

	// Lazy Search
	private int firstRecord;
	private int lastRecord;
	private String sort;
	private String sortADSC;
	private String pageFlag;
	private String ocuFlag;
	private String ocuType;
	
	private String category;
	

}
