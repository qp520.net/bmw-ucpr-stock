package com.bmw.ucpr.stock.entity;

import java.util.List;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月6日 下午6:59:11 

* 类说明 

*/
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsFileVo extends BasePO{
	private String fileid;
	private String filetitle;
	private String fileneed;
	private byte[] filecontent;
	private String filename;
	private String filetype;
	private String doctype;
	private String filevalid;
	private String filereason;
	
	
	
	private String filesequence;
	private String filereasonid;
	private boolean filevalids;
	private List<SelectItem> filereasonlist;
	
	private String ucvin;
	private String username;
	
}
