package com.bmw.ucpr.stock.entity;

import java.util.Date;
import java.util.List;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月6日 下午4:21:54 

* 类说明 

*/
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsRaSearchVo extends BasePO{
	private String language;
	private String customerid;
	private String ucvin;
	private String supplier;
	private Date hf;
	private Date ht;
	private Date af;
	private Date at;
	private String ecode;
	
	private String id;
	private String ucvins;//VIN_7
	
	private String sequence;//DOC SEQUENCE
	private String doccontent;
	private String docmimetype;
	private String docfilename;
	private String username;
	//apply_date condition
	private String con;
	
	private String cbuno;
	private String ckdno;
	private List<String> ucvinlistwrite;
	private List<String> ucvinlistupload;
	
	private String status;
	private String policyid;
	private Date rf;
	private Date rt;
	
}
