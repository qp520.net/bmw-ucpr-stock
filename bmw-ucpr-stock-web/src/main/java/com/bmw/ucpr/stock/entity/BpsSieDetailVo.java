package com.bmw.ucpr.stock.entity;

import java.util.Date;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Project：BMW UCDR Web Base
 * System：UCDR
 * Sub System：Admin
 * BpsSieDetailVo
 * @author chongnan
 * 
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsSieDetailVo extends BasePO {
	private String id;//38
	private String ucvin;//9
	private String vinverify;
	private String saletype;//54	
	private String cbuno;//1
	private String ckdno;//2	
	private String dealername;//3
	private String region;//4
	private String uccarseries;//5
	private String model;//6
	private String source;//7
	private String platetype;//51
	private String plateno;	//8
	private String bodycolor;//10
	private String interiorcolor;//11
	private String productiondate;//12
	private Date ucfirstregistrationdate;//13
	private String ucmileage;//14
	private String newcarwarrdate;//15
	private String nscbba;//55
	private String ecode;//19
	private String ownername;//22
	private String mobileno;//23
	private String stockstartdate;//25
	private String stockenddate;
	private String dmsstockdate;
	private String stockplace;//21
	private String registrationtransferfee;//26
	private String pointcheckfee;//27
	private String repairandmaintenancefee;//28
	private String beautyfee;//29
	private String salecommission;//30
	private String otherexpenses;//31
	private String warrantyfund;//32
	private String marketfee;//33
	private String purchase;//34
	
	private String redbookcode;//16
	private String redbookretailprice;//17
	private String rbwhoesaleprice;//18
	private String storeno;//20
	private String ageinmonth;//24
	private String advisedpurchase;//35
	private String qualitycetificateno;//36
	private String bonusandserviceprice;//37
	private String handoverdate;//39
	private String stackdays;//40
	private String leasingfeeandinterest;//41
	private String totalstockcost;//42
	private String stockstmaptax;//43
	private String stockaddedtax;//44
	private String stockgrossmargin1;//45
	private String stockgrossmarginrate1;//46
	private String stockgrossmargin2;//47
	private String stockgrossmarginrate2;//48
	private String stockcommission;//49
	private String stockcommissionrate;//50
	private String usedcarcondition;//52
	private String cartype;//53
	
	private String vin7;//56
	private String username;
	
	private String ocutype;
	private String ocutypeid;
	
}
