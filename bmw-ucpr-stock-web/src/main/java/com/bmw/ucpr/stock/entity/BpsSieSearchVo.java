package com.bmw.ucpr.stock.entity;

import java.util.Date;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Project：BMW UCDR Web Base
 * System：UCDR
 * Sub System：Admin
 * search condition vo
 * @author chongnan
 * 
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsSieSearchVo extends BasePO {
	
	private String language;
	private String customerid;
	private String ucvin;
	private String supplier;
	private String ecode;
	private Date dmsf;
	private Date dmst;
	
	private String ucvins;
	
	private String category;
	
	private String active;
	
}
