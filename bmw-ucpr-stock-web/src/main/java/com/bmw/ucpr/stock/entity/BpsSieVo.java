package com.bmw.ucpr.stock.entity;

import java.util.Date;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Project：BMW UCDR Web Base
 * System：UCDR
 * Sub System：Admin
 * BpsSieVo
 * @author chongnan
 * 
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsSieVo extends BasePO {

	private String id;
	private String ucvin;
	private String vinverify;
	private String saletype;	
	private String cbuno;
	private String ckdno;	
	private String dealername;
	private String region;
	private String uccarseries;
	private String model;
	private String source;
	private String platetype;
	private String plateno;	
	private String bodycolor;
	private String interiorcolor;
	private Date productiondate;
	private Date ucfirstregistrationdate;
	private String ucmileage;
	private String newcarwarrdate;
	private String nscbba;
	private String ecode;
	private String ownername;
	private String mobileno;
	private Date stockstartdate;
	private Date stockenddate;
	private Date dmsstockdate;
	private String stockplace;
	private String registrationtransferfee;
	private String pointcheckfee;
	private String repairandmaintenancefee;
	private String beautyfee;
	private String salecommission;
	private String otherexpenses;
	private String warrantyfund;
	private String marketfee;
	private String purchase;
	
	private String productiondatestr;
	private String ucfirstregistrationdatestr;
	private String stockstartdatestr;
	private String stockenddatestr;
	private String dmsstockdatestr;
	private String ocutype;
	private String ocutypeid;
	
}
