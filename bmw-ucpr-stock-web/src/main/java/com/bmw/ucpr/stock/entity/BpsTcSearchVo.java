package com.bmw.ucpr.stock.entity;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月5日 上午10:50:33 

* 类说明 

*/
@Data
@EqualsAndHashCode(callSuper = true)
public class BpsTcSearchVo extends BasePO{
	private String language;
	private String customerid;
	private String ucvin;
	private String supplier;
	private Date hf;
	private Date ht;
	private Date af;
	private Date at;
	private String ecode;
	
	private String id;
	private String ucvins;//VIN_7
	
	private String sequence;//DOC SEQUENCE
	private String doccontent;
	private String docmimetype;
	private String docfilename;
	private String username;
	//apply_date condition
	private String con;
	
	private String cbuno;
	private String ckdno;
	private List<String> ucvinlistwrite;
	private List<String> ucvinlistupload;
	
	private String status;
	private String policyid;
	
	// Lazy Search
	private int firstRecord;
	private int lastRecord;
	private String sort;
	private String sortADSC;
	private String pageFlag;
}
