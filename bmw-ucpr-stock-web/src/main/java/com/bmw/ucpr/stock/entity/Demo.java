package com.bmw.ucpr.stock.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bmw.core.database.po.BasePO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.entity
 *
 * @author Bruce Maa
 * @since 2019-07-10.16:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "demo")
public class Demo extends BasePO {

    @TableId
    private Long demoId;
    private String name;
    private Integer age;
    private LocalDate birthday;
}
