package com.bmw.ucpr.stock.entity;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

//库存报表
@Data
@EqualsAndHashCode(callSuper = true)
public class OcuChartVo extends BasePO {
	
	private String columnOne;
	private String columnTwo;
	private int total;
	
	private String totaltg;
	private String totalpeac;
	private String totalapp;
	private String region;
	private String cbuNo;
		
	
}
