package com.bmw.ucpr.stock.entity;

import java.util.Date;
import java.util.List;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

//库存报表
@Data
@EqualsAndHashCode(callSuper = true)
public class OcuStockReportSearchVo extends BasePO {
	
	private String vin;
	private String singleVin;
	private String language;
	private String customerId;
	private String userName;
	private List<String> vin7listWrite;
	private List<String> vin7ListUpload;
	private String supplier;
	private String cbuNo;
	private String ckdNo;
	private String ecode;
	private Date fromStockStartDate;
	private Date toStockStartDate;
	private Date fromStockEndDate;
	private Date toStockEndDate;
	private String isUsed;
	
	
	//Lazy Search
	private int firstRecord;
	private int lastRecord;
	private String sort;
	private String sortADSC;
	private String pageFlag;
	
	//chart 字段
	private String columnNameOne;
	private String columnNameTwo;
	
	private String ocuType;
	private String ocuTypeName;
	private String ocuFlag;
	private String con;
	
}