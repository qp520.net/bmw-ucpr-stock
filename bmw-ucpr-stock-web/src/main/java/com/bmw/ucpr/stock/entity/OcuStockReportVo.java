package com.bmw.ucpr.stock.entity;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

//库存报表
@Data
@EqualsAndHashCode(callSuper = true)
public class OcuStockReportVo extends BasePO {
	
	private String isUsed;
	private String brand;
	private String handovered;
	private String cbuNo;
	private String ckdNo;
	private String region;
	private String dealerName;
	private String ucCarSeries;
	private String model;
	private String source;
	private String plateNO;
	private String plateType;
	private String ucVIN;
	private String newCarWarrantyStartDate;
	private String bodyColor;
	private String interiorColor;
	private String productionDate;
	private String ucFirstRegistration;
	private String ucMileage;
	private String ageInMonth;
	private String stockStartDate;
	private String stockEndDate;
	private String dmsStockDate;
	private String stockDays;
	private String leasingFeeAndInterest;
	private String registrationTransferFee;
	private String pointCheckFee;
	private String repairAndMaintenanceFee;
	private String beautyFee;
	private String saleCommission;
	private String otherExpenses;
	private String warrantyFund;
	private String marketFee;
	private String purchasePrice;
	private String totalStockCost;
	private String redbookCode;
	private String redbookRetailPrice;
	private String redbookWhoesalePrice;
	private String usedCarCondition;
	private String advisedPurchase;
	private String qualityCetificateNo;
	private String bonusAndService;
	private String businessAndStampTax;
	private String usedCarValueaddedTax;
	private String grossMargin1;
	private String grossMarginRate1;
	private String grossMargin2;
	private String grossMarginRate2;
	private String commission;
	private String commissionRate;
	private String importhomegrown;
	private String carType;
	private String ecode;
	private String stockPlace;
	private String ownerName;
	private String mobileNo;
	
	private String storeNo;
	private String warrantyDate;
	private String saleType;
	private String id;
	private String user;
	private String status;
	
	private String ocuType;
	private String ocuTypeName;
		
}
