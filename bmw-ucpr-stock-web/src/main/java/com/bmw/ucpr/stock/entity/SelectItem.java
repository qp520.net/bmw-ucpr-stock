package com.bmw.ucpr.stock.entity;

import com.bmw.core.database.po.BasePO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SelectItem extends BasePO {
    private String key="";
	private String value="";
	
	private String numKey;
	private String charKey;
	
	private String value_CN;
	private String value_EN;
	
	private String active;
	
	private String reasonID;
	
	private String reason;
}
