package com.bmw.ucpr.stock.entity;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class StockApproveVo extends BasePO {
    private String ucvin;
	private String reasonValue;	
	private String memo;
	private String approveResult;
	private String approveComment;
	
}
