package com.bmw.ucpr.stock.entity;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class StockCommentVo extends BasePO {
    private String ucvin;
	private String reason;	
	private String memo;
	
}
