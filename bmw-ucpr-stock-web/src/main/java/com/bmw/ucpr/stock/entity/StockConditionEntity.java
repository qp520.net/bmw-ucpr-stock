package com.bmw.ucpr.stock.entity;

import java.util.Date;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class StockConditionEntity extends BasePO {
    private String language;  //语言
	private String customerId;
	private String ucvin;
	private String supplier;
	private String ecode;
	private Date stockStartDateFrom;
	private Date stockStartDateTo;
	private String ocuType;
	
}
