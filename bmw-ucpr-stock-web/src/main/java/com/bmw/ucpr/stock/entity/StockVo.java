package com.bmw.ucpr.stock.entity;

import java.util.Date;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class StockVo extends BasePO {
	private String vin;
	private String applicationStatus;
	private String applicationResult;
	private String saleType;
	private String cbuNo;
	private String ckdNo;
	private String dealerName;
	private String region;
	private String series;
	private String modelCode;
	private String source;
	private String plateType;
	private String plateNo;
	private String bodyColor;
	private String interiorColor;
	private String productionDate;
	private String firstRegisterDate;
	private String mileage;
	private String warrantyDate;
	private String supplier;
	private String ecode;
	private String ownerName;
	private String mobileNo;
	private Date stockStartDate;
	private String stockStartDateStr;
	private Date stockEndDate;
	private String stockEndDateStr;
	private String stockPlace;
	private String ownershipTransferFee;
	private String pointCheckFee;
	private String repairAndMaintainFee;
	private String makeupFee;
	private String saleCommission;
	private String otherExpenses;
	private String warrantyFund;
	private String marketFee;
	private String purchase;
	private String ocuType;
	private String ocuTypeName;
	
}
