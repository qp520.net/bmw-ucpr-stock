package com.bmw.ucpr.stock.entity;

import com.bmw.core.database.po.BasePO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UcLookup extends BasePO {
	private	String	CATEGORY;
	private	String	NUM_VAL;
	private	String	CHAR_VAL;
	private	String	TEXT_EN;
	private	String	TEXT_CN;
	private	String	ACTIVE;
	
	
	private	String	numVal;
	private	String	charVal;
	private	String	textEn;
	private	String	textCn;

}
