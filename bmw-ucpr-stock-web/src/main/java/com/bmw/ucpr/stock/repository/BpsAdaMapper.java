package com.bmw.ucpr.stock.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.BpsAdaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAdaSearchVo;
import com.bmw.ucpr.stock.entity.BpsAdaVo;
import com.bmw.ucpr.stock.entity.SelectItem;

@Mapper
public interface BpsAdaMapper extends BaseMapper<BpsAdaVo> {
	
	Page<BpsAdaVo> getBpsAdaPage(IPage<?> bpsAdaPage, @Param("condition") BpsAdaSearchVo searchCondition);
	//数据列表
	List<BpsAdaVo> getBpsAdaList(BpsAdaSearchVo searchCondition);
	//供应商List
	List<SelectItem> getSupplierList(BpsAdaSearchVo searchCondition);
	//UC类别 List
	List<SelectItem> getOCUTypeList(BpsAdaSearchVo searchCondition);
	//Ecode List
	List<SelectItem> getEcodeList(BpsAdaSearchVo searchCondition);
	//获取单条车辆信息
	BpsAdaDetailVo getBpsAdaDetail(BpsAdaSearchVo searchCondition);
	//历史记录
	Page<BpsAdaDetailVo> getBpsAdaPageHistory(IPage<?> bpsAdaHistory, @Param("condition") BpsAdaSearchVo searchCondition);
	//List<BpsAdaDetailVo> getBpsAdaHistory(BpsAdaSearchVo searchCondition);
	
	List<SelectItem> getLookUpList(BpsAdaSearchVo searchCondition);
	//获取File
	List<BpsAdaFileVo> getBpsAdaFile(BpsAdaSearchVo searchCondition);
	
	String getBpsAdaApplyValid(BpsAdaSearchVo bpsAdaSearchVo);
	
	String getBpsAdaApplyDateValid(BpsAdaSearchVo bpsAdaSearchVo);
	
	String getBpsAdaVinValid(BpsAdaSearchVo bpsAdaSearchVo);
	
	String getneedextendedwarr(BpsAdaDetailVo bpsAdaDetailVo);
	
	String getwarrucwarrenddate(BpsAdaDetailVo bpsAdaDetailVo);
	
	String getwarrendmileage(BpsAdaDetailVo bpsAdaDetailVo);
	
	String getncwarrdays(BpsAdaDetailVo bpsAdaDetailVo);
	
	String gettotalstockcost(BpsAdaDetailVo bpsAdaDetailVo);
	
	String getstockgrossmargin1(BpsAdaDetailVo bpsAdaDetailVo);
	
	String getstockgrossmargin2(BpsAdaDetailVo bpsAdaDetailVo);
	
	String getstockgrossmarginrate1(BpsAdaDetailVo bpsAdaDetailVo);
	
	String getstockgrossmarginrate2(BpsAdaDetailVo bpsAdaDetailVo);
	
	String getstockcommission(BpsAdaDetailVo bpsAdaDetailVo);
	
	String getstockcommissionrate(BpsAdaDetailVo bpsAdaDetailVo);
	
	void updateBpsAda(BpsAdaDetailVo bpsAdaDetailVo);
	
	void submitBpsAda(BpsAdaDetailVo bpsAdaDetailVo);
	
	void updateBpsAdaCode(BpsAdaDetailVo bpsAdaDetailVo);
	
	void submitBpsAdaCode(BpsAdaDetailVo bpsAdaDetailVo);

}
