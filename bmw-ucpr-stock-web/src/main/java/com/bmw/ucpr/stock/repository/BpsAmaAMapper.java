package com.bmw.ucpr.stock.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.BpsAmaADetailVo;
import com.bmw.ucpr.stock.entity.BpsAmaASearchVo;
import com.bmw.ucpr.stock.entity.BpsAmaAVo;
import com.bmw.ucpr.stock.entity.SelectItem;

@Mapper
public interface BpsAmaAMapper extends BaseMapper<BpsAmaAVo> {
	
	
	Page<BpsAmaAVo> getBpsAmaAPage(IPage<?> bpsAmaAPage, @Param("condition") BpsAmaASearchVo condition);
	
	List<SelectItem> getPolicyList(BpsAmaASearchVo condition);
	
	List<BpsAmaAVo> getBpsAmaAList(BpsAmaASearchVo condition);
	
	BpsAmaADetailVo getBpsAmaADetail(BpsAmaASearchVo condition);
	
	List<BpsAmaADetailVo> getBpsAmaAHistory(BpsAmaASearchVo condition);
	
	String getBpsAmaApplyResetValid(BpsAmaASearchVo condition);
	
	void updateBpsAmaA(BpsAmaADetailVo bpsAmaADetailVo);
	
	void submitBpsAmaA(BpsAmaADetailVo bpsAmaADetailVo);

}
