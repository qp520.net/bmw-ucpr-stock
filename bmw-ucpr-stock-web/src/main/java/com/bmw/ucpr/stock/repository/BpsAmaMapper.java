package com.bmw.ucpr.stock.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.BpsAmaFileVo;
import com.bmw.ucpr.stock.entity.BpsAmaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAmaSearchVo;
import com.bmw.ucpr.stock.entity.BpsAmaVo;
import com.bmw.ucpr.stock.entity.SelectItem;

@Mapper
public interface BpsAmaMapper extends BaseMapper<BpsAmaVo> {
	
	Page<BpsAmaVo> getBpsAmaPage(IPage<?> bpsAmaPage, @Param("condition") BpsAmaSearchVo condition);	
	
	List<SelectItem> getSupplierList(BpsAmaSearchVo condition);
	
	List<SelectItem> getOCUTypeList(BpsAmaSearchVo condition);
	
	List<SelectItem> getEcodeList(BpsAmaSearchVo condition);	
	
	List<BpsAmaVo> getBpsAmaList(BpsAmaSearchVo condition);
	
	int getBpsAmaListCount(BpsAmaSearchVo condition);	
	
	BpsAmaDetailVo getBpsAmaDetail(BpsAmaSearchVo condition);	
	
	Page<BpsAmaDetailVo> getBpsAmaHistoryPage(IPage<?> bpsAmaHistoryPage, @Param("condition") BpsAmaSearchVo condition);
	//List<BpsAmaDetailVo> getBpsAmaHistory(BpsAmaSearchVo condition);	
	
	List<BpsAmaFileVo> getBpsAmaFile(BpsAmaSearchVo condition);
	
	BpsAmaFileVo getBpsAmaDocD(BpsAmaSearchVo condition);
	
	String getBpsAmaCanApply(BpsAmaSearchVo condition);
	
	String getBpsAmaApplyDate(BpsAmaSearchVo condition);
	
	String getBpsAmaApplyValid(BpsAmaSearchVo condition);
	
	String getBpsAmaApplyAuditValid(BpsAmaSearchVo condition);	
	
	List<SelectItem> getLookUpList(BpsAmaSearchVo condition);
			
	void submitBpsAma(BpsAmaDetailVo bpsAmaDetailVo);
	
	void submitBpsAmaBatch(BpsAmaDetailVo bpsAmaDetailVo);
	
	void submitBpsAmaCode(BpsAmaDetailVo bpsAmaDetailVo);
	
	void updateBpsAma(BpsAmaDetailVo bpsAmaDetailVo);

}
