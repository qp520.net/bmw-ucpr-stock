package com.bmw.ucpr.stock.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.BpsEcaDetailVo;
import com.bmw.ucpr.stock.entity.BpsEcaSearchVo;
import com.bmw.ucpr.stock.entity.BpsEcaVo;
import com.bmw.ucpr.stock.entity.SelectItem;

@Mapper
public interface BpsEcaMapper extends BaseMapper<BpsEcaVo> {
	
	Page<BpsEcaVo> getBpsEcaPage(IPage<?> bpsEcaPage, @Param("condition") BpsEcaSearchVo condition);
	
	List<SelectItem> getCbuNoList(BpsEcaSearchVo condition);
	
	List<SelectItem> getCkdNoList(BpsEcaSearchVo condition);
	
	List<SelectItem> getStatusList(BpsEcaSearchVo condition);
	
	List<SelectItem> getOCUTypeList(BpsEcaSearchVo condition);
	
	BpsEcaDetailVo getBpsEcaDetail(BpsEcaSearchVo condition);
	
	Page<BpsEcaDetailVo> getBpsEcaHistoryPage(IPage<?> bpsEcaHistoryPage, @Param("condition") BpsEcaSearchVo condition);
	//List<BpsEcaDetailVo> getBpsEcaHistory(BpsEcaSearchVo condition);
	
	List<BpsAdaFileVo> getBpsEcaFile(BpsEcaSearchVo condition);
	//TODO
	BpsAdaFileVo getBpsEcaFileContent(BpsEcaSearchVo condition);
	//TODO
	List<SelectItem> getBpsEcaFileReason(BpsEcaSearchVo condition);
	//TODO
	void insertBpsEcaFile(BpsAdaFileVo bpsFileVo);
	//TODO
	void insertBpsEcaFileBatch(List<BpsAdaFileVo> bpsFileVo);
	//TODO
	void updateBpsEcaFile(BpsAdaFileVo bpsFileVo);
	//TODO
	void updateBpsEcaFileBatch(List<BpsAdaFileVo> bpsFileVo);
	//TODO
	int checkBpsEcaFile(BpsEcaSearchVo condition);
	
	List<SelectItem> getLookUpList(BpsEcaSearchVo condition);
	//TODO
	void updateBpsEca(BpsEcaDetailVo bpsEcaDetailVo);
	
	void submitBpsEca(BpsEcaDetailVo bpsEcaDetailVo);
	
	void submitStockEca(BpsEcaDetailVo bpsEcaDetailVo);
	
	void submitBpsEcaBatch(BpsEcaDetailVo bpsEcaDetailVo);
	
	void submitBpsEcaCode(BpsEcaDetailVo bpsEcaDetailVo);
	
}
