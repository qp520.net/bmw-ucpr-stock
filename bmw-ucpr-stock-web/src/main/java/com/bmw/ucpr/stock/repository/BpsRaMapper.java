package com.bmw.ucpr.stock.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.BpsRaDetailVo;
import com.bmw.ucpr.stock.entity.BpsRaSearchVo;
import com.bmw.ucpr.stock.entity.BpsRaVo;
import com.bmw.ucpr.stock.entity.SelectItem;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月7日 上午11:23:47 

* 类说明 

*/
@Mapper
public interface BpsRaMapper extends BaseMapper<BpsRaVo>{

	Page<BpsRaVo> getBpsRaPage(IPage<?> bpsRaPage, @Param("condition")BpsRaSearchVo bpsRaSearchVo);

	BpsRaDetailVo getBpsRaDetail(BpsRaSearchVo bpsRaSearchVo);

	void submitBpsRa(BpsRaDetailVo bpsRaDetailVo);
	                   
	Page<BpsRaDetailVo> getBpsRaHistory(IPage<?> siePage, @Param("condition")BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getCkdNoList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getEcodeList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getCbuNoList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getStatusList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getSupplierList(BpsRaSearchVo bpsraSearchVo);
	
	
}
