package com.bmw.ucpr.stock.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.BpsAdaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.BpsSieDetailVo;
import com.bmw.ucpr.stock.entity.BpsSieSearchVo;
import com.bmw.ucpr.stock.entity.BpsSieVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.entity.UcLookup;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.repository
 *
 */
@Mapper
public interface BpsSieMapper extends BaseMapper<BpsSieVo> {
	
	Page<BpsSieVo> getBpsSiePage(IPage<?> page, @Param("condition") BpsSieSearchVo bpsSieSearchVo);

	List<BpsSieVo> getBpsSieList(BpsSieSearchVo bpsSieSearchVo);
	
	int getBpsSieListCount(BpsSieSearchVo bpsSieSearchVo);
	
	BpsSieDetailVo getBpsSieDetail(BpsSieSearchVo bpsSieSearchVo);
	
	BpsSieDetailVo getBpsSieDetailSec(BpsSieSearchVo bpsSieSearchVo);
	
	List<SelectItem> getSupplierList(BpsSieSearchVo bpsSieSearchVo);
	
	Page<SelectItem> getSupplierPage(IPage<SelectItem> page, @Param("condition") BpsSieSearchVo bpsSieSearchVo);

	List<SelectItem> getOCUTypeList(BpsSieSearchVo bpsSieSearchVo);
	
	List<SelectItem> initSaleTypeListByOcu(BpsSieSearchVo bpsSieSearchVo);
	
	List<SelectItem> getEcodeList(BpsSieSearchVo bpsSieSearchVo);
	
	List<UcLookup> getLookUpList(BpsSieSearchVo bpsSieSearchVo);
	
	String getBpsSieValid(BpsSieSearchVo bpsSieSearchVo);
	
	String getBpsSieDateValid(BpsSieSearchVo bpsSieSearchVo);
	
	String getBpsSieVinValid(BpsSieSearchVo bpsSieSearchVo);
	
	String gettotalstockcost(BpsSieDetailVo bpsSieDetailVo);
	
	String getstockgrossmargin1(BpsSieDetailVo bpsSieDetailVo);
	
	String getstockgrossmargin2(BpsSieDetailVo bpsSieDetailVo);
	
	String getstockgrossmarginrate1(BpsSieDetailVo bpsSieDetailVo);
	
	String getstockgrossmarginrate2(BpsSieDetailVo bpsSieDetailVo);
	
	String getstockcommission(BpsSieDetailVo bpsSieDetailVo);
	
	String getstockcommissionrate(BpsSieDetailVo bpsSieDetailVo);
	
	void updateOcutype(BpsSieDetailVo bpsSieDetailVo);
	
	void updateBpsSie(BpsSieDetailVo bpsSieDetailVo);
	
	void submitBpsSie(BpsSieDetailVo bpsSieDetailVo);
	
	void updateBpsSieCode(BpsSieDetailVo bpsSieDetailVo);
	
	List<BpsAdaDetailVo> getBpsAdaHistory(BpsSieSearchVo bpsSieSearchVo);
	
	Page<BpsAdaDetailVo> getBpsAdaHistoryPage(IPage<?> page, @Param("condition") BpsSieSearchVo bpsSieSearchVo);
	
	List<BpsAdaFileVo> getBpsAdaFile(BpsSieSearchVo bpsSieSearchVo);
	
}
