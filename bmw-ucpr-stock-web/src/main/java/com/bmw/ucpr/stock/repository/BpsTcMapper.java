package com.bmw.ucpr.stock.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.BpsFileVo;
import com.bmw.ucpr.stock.entity.BpsTcDetailVo;
import com.bmw.ucpr.stock.entity.BpsTcSearchVo;
import com.bmw.ucpr.stock.entity.BpsTcVo;
import com.bmw.ucpr.stock.entity.SelectItem;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月5日 上午10:30:15 

* 类说明 

*/
@Mapper
public interface BpsTcMapper extends BaseMapper<BpsTcVo>{

	List<SelectItem> getSupplierList(BpsTcSearchVo bpsTcsearchVo);

	List<SelectItem> getStatusList(BpsTcSearchVo bpsTcsearchVo);

	List<SelectItem> getCbuNoList(BpsTcSearchVo bpsTcsearchVo);

	List<SelectItem> getCkdNoList(BpsTcSearchVo bpsTcsearchVo);

	List<SelectItem> getEcodeList(BpsTcSearchVo bpsTcsearchVo);

	Page<BpsTcVo> getBpstcList(IPage<?> lookupPage, @Param("condition")BpsTcSearchVo bpsTcsearchVo);

	Page<BpsTcDetailVo> getBpsTcHistory(IPage<?> bpstcPage, @Param("condition")BpsTcSearchVo bpsTcSearchVo);

	BpsTcDetailVo getBpsTcDetail(BpsTcSearchVo bpstcsearchvo);

	void updateBpsTc(BpsTcDetailVo bpstcDetailVo);

	void updateBpsTcCode(BpsTcDetailVo bpstcDetailVo);

	void submitBpsTc(BpsTcDetailVo bpstcDetailVo);

	List<BpsFileVo> getBpsTcFile(BpsTcSearchVo bpsTcSearchVo);


}
