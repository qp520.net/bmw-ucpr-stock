package com.bmw.ucpr.stock.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bmw.ucpr.stock.entity.Demo;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.repository
 *
 * @author Bruce Maa
 * @since 2019-07-10.16:03
 */
public interface DemoRepository extends BaseMapper<Demo> {
}
