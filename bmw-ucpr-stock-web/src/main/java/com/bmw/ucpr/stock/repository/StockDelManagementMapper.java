package com.bmw.ucpr.stock.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.StockApproveVo;
import com.bmw.ucpr.stock.entity.StockConditionEntity;
import com.bmw.ucpr.stock.entity.StockVo;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.repository
 *
 */
@Mapper
public interface StockDelManagementMapper extends BaseMapper<StockVo> {
	
	Page<StockVo> getStockList(IPage<?> page, @Param("condition") StockConditionEntity condition);

	StockApproveVo getDelComment(StockConditionEntity condition);
	
	void updateApprove(StockApproveVo vo);
	
	void updateHistory(StockApproveVo vo);
	
	void approveProcedure(StockApproveVo vo);	
	
}
