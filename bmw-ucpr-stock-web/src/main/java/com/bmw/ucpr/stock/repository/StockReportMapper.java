package com.bmw.ucpr.stock.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.BpsAdaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.OcuChartVo;
import com.bmw.ucpr.stock.entity.OcuStockReportSearchVo;
import com.bmw.ucpr.stock.entity.OcuStockReportVo;
import com.bmw.ucpr.stock.entity.SelectItem;

@Mapper
public interface StockReportMapper extends BaseMapper<OcuStockReportVo> {
	
	Page<OcuStockReportVo> getStockReportPage(IPage<?> page, @Param("condition") OcuStockReportSearchVo searchCondition);
	//供应商List
	List<SelectItem> getSupplierList(OcuStockReportSearchVo searchCondition);
	//CBU List
	List<SelectItem> getCbuList(OcuStockReportSearchVo searchCondition);
	//CKD List
	List<SelectItem> getCkdList(OcuStockReportSearchVo searchCondition);
	//Ecode List
	List<SelectItem> getEcodeList(OcuStockReportSearchVo searchCondition);
	//UC类别 List
	List<SelectItem> getOCUTypeList(OcuStockReportSearchVo searchCondition);
	//库存报表列表
	List<OcuStockReportVo> getStockReportList(OcuStockReportSearchVo searchCondition);
	//获取库存车辆总条数
	int getStockReportCount(OcuStockReportSearchVo searchData);
	//获取单条车辆信息
	OcuStockReportVo getStockReportDetail(OcuStockReportSearchVo searchCondition);
	//创建饼图
	List<OcuChartVo> getPieChart(OcuStockReportSearchVo searchCondition);
	//创建柱图、线图
	List<OcuChartVo> getBarOrLineChart(OcuStockReportSearchVo searchCondition);
	//获取历史数据
	List<BpsAdaDetailVo> getStockReportHistory(OcuStockReportSearchVo searchCondition);
	//获取File
	List<BpsAdaFileVo> getStockReportFile(OcuStockReportSearchVo searchCondition);
}
