package com.bmw.ucpr.stock.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bmw.ucpr.stock.entity.BpsRaDetailVo;
import com.bmw.ucpr.stock.entity.BpsRaSearchVo;
import com.bmw.ucpr.stock.entity.BpsRaVo;
import com.bmw.ucpr.stock.entity.SelectItem;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月6日 下午4:32:09 

* 类说明 

*/
@Mapper
public interface UcRaMapper extends BaseMapper<BpsRaVo>{

	Page<BpsRaVo> getUcraPage(IPage<?> ucraPage, @Param("condition")BpsRaSearchVo bpsraSearchVo);

	BpsRaDetailVo getBpsRaDetail(BpsRaSearchVo bpsraSearchVo);

	void submitUcRaC(BpsRaDetailVo bpsraSearchVo);

	List<SelectItem> getSupplierList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getStatusList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getCbuNoList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getCkdNoList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getEcodeList(BpsRaSearchVo bpsraSearchVo);

	Page<BpsRaDetailVo> getBpsRaHistory(IPage<?> bpsucraPage, @Param("condition") BpsRaSearchVo bpsraSearchVo);

}
