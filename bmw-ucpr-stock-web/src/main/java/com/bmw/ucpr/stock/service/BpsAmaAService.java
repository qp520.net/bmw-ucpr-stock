package com.bmw.ucpr.stock.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ucpr.stock.entity.BpsAmaADetailVo;
import com.bmw.ucpr.stock.entity.BpsAmaASearchVo;
import com.bmw.ucpr.stock.entity.BpsAmaAVo;
import com.bmw.ucpr.stock.entity.SelectItem;

public interface BpsAmaAService extends IService<BpsAmaAVo> {

	Page<BpsAmaAVo> getBpsAmaAPage(IPage<?> bpsAmaAPage, @Param("condition") BpsAmaASearchVo condition);
	
	List<SelectItem> getPolicyList(BpsAmaASearchVo condition);
	
	List<BpsAmaAVo> getBpsAmaAList(BpsAmaASearchVo condition);
	
	BpsAmaADetailVo getBpsAmaADetail(BpsAmaASearchVo condition);
	
	List<BpsAmaADetailVo> getBpsAmaAHistory(BpsAmaASearchVo condition);
	
	String getBpsAmaApplyResetValid(BpsAmaASearchVo condition);
	
	void updateBpsAmaA(BpsAmaADetailVo bpsAmaADetailVo);
	
	void submitBpsAmaA(BpsAmaADetailVo bpsAmaADetailVo);
}
