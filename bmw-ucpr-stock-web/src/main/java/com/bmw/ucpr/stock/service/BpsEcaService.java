package com.bmw.ucpr.stock.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.BpsEcaDetailVo;
import com.bmw.ucpr.stock.entity.BpsEcaSearchVo;
import com.bmw.ucpr.stock.entity.BpsEcaVo;
import com.bmw.ucpr.stock.entity.SelectItem;

public interface BpsEcaService extends IService<BpsEcaVo> {
	
	Page<BpsEcaVo> getBpsEcaPage(IPage<?> bpsEcaPage, @Param("condition") BpsEcaSearchVo condition);
	
	List<SelectItem> getCbuNoList(BpsEcaSearchVo condition);
	
	List<SelectItem> getCkdNoList(BpsEcaSearchVo condition);
	
	List<SelectItem> getStatusList(BpsEcaSearchVo condition);
	
	List<SelectItem> getOCUTypeList(BpsEcaSearchVo condition);
	
	BpsEcaDetailVo getBpsEcaDetail(BpsEcaSearchVo condition);
	
	Page<BpsEcaDetailVo> getBpsEcaHistoryPage(IPage<?> bpsEcaHistoryPage, @Param("condition") BpsEcaSearchVo condition);
	//List<BpsEcaDetailVo> getBpsEcaHistory(BpsEcaSearchVo condition);
	
	//文件
	List<BpsAdaFileVo> getBpsEcaFile(BpsEcaSearchVo condition);
	
	BpsAdaFileVo getBpsEcaFileContent(BpsEcaSearchVo condition);
	
	List<SelectItem> getBpsEcaFileReason(BpsEcaSearchVo condition);
	
	void insertBpsEcaFile(BpsAdaFileVo bpsFileVo);
	
	void insertBpsEcaFileBatch(List<BpsAdaFileVo> bpsFileVo);
	
	void updateBpsEcaFile(BpsAdaFileVo bpsFileVo);
	
	void updateBpsEcaFileBatch(List<BpsAdaFileVo> bpsFileVo);
	
	int checkBpsEcaFile(BpsEcaSearchVo condition);
	//
	
	List<SelectItem> getLookUpList(BpsEcaSearchVo condition);
	
	void updateBpsEca(BpsEcaDetailVo bpsEcaDetailVo);
	
	void submitBpsEca(BpsEcaDetailVo bpsEcaDetailVo);
	
	void submitStockEca(BpsEcaDetailVo bpsEcaDetailVo);
	
	void submitBpsEcaBatch(BpsEcaDetailVo bpsEcaDetailVo);
	
	void submitBpsEcaCode(BpsEcaDetailVo bpsEcaDetailVo);

}
