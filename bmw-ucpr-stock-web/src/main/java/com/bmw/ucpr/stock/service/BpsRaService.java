package com.bmw.ucpr.stock.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ucpr.stock.entity.BpsRaDetailVo;
import com.bmw.ucpr.stock.entity.BpsRaSearchVo;
import com.bmw.ucpr.stock.entity.BpsRaVo;
import com.bmw.ucpr.stock.entity.SelectItem;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月7日 上午11:20:34 

* 类说明 

*/
public interface BpsRaService extends IService<BpsRaVo>{

	Page<BpsRaVo> getBpsRaPage(IPage<?> bpsRaPage, BpsRaSearchVo bpsRaSearchVo);

	BpsRaDetailVo getBpsRaDetail(BpsRaSearchVo bpsRaSearchVo);

	void submitBpsRa(BpsRaDetailVo bpsRaDetailVo);

	Page<BpsRaDetailVo> getBpsRaHistory(IPage<?> siePage, BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getSupplierList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getStatusList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getCbuNoList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getEcodeList(BpsRaSearchVo bpsraSearchVo);

	List<SelectItem> getCkdNoList(BpsRaSearchVo bpsraSearchVo);

	
}
