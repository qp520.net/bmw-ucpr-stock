package com.bmw.ucpr.stock.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ucpr.stock.entity.StockCommentVo;
import com.bmw.ucpr.stock.entity.StockConditionEntity;
import com.bmw.ucpr.stock.entity.StockVo;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.service
 *
 */
public interface BpsSdaService extends IService<StockVo> {
	Page<StockVo> getStockList(IPage<?> page, StockConditionEntity condition);

	String getSwitchFlg(StockConditionEntity condition);
	
	StockCommentVo getComment(StockConditionEntity condition);
	
	void updateComment(StockCommentVo vo);
	
}
