package com.bmw.ucpr.stock.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ucpr.stock.entity.StockApproveVo;
import com.bmw.ucpr.stock.entity.StockConditionEntity;
import com.bmw.ucpr.stock.entity.StockVo;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.service
 *
 */
public interface BpsSdmService extends IService<StockVo> {

	Page<StockVo> getStockList(IPage<?> page, StockConditionEntity condition);

	StockApproveVo getDelComment(StockConditionEntity condition);
	
	void updateApprove(StockApproveVo vo);
	
	void updateHistory(StockApproveVo vo);
	
	void approveProcedure(StockApproveVo vo);	
}
