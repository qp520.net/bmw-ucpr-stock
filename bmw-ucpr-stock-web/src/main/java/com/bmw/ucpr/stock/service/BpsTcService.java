package com.bmw.ucpr.stock.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ucpr.stock.entity.BpsFileVo;
import com.bmw.ucpr.stock.entity.BpsTcDetailVo;
import com.bmw.ucpr.stock.entity.BpsTcSearchVo;
import com.bmw.ucpr.stock.entity.BpsTcVo;
import com.bmw.ucpr.stock.entity.SelectItem;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月5日 上午11:09:18 

* 类说明 

*/
public interface BpsTcService extends IService<BpsTcVo>{

	List<SelectItem> getSupplierList(BpsTcSearchVo psTcsearchVo);

	List<SelectItem> getStatusList(BpsTcSearchVo bpsTcsearchVo);

	List<SelectItem> getCbuNoList(BpsTcSearchVo bpsTcsearchVo);

	List<SelectItem> getCkdNoList(BpsTcSearchVo bpsTcsearchVo);

	List<SelectItem> getEcodeList(BpsTcSearchVo bpsTcsearchVo);

	Page<BpsTcVo>   getBpsTcList(IPage<?> lookupPage, BpsTcSearchVo bpsTcsearchVo);

	Page<BpsTcDetailVo> getBpsTcHistory(IPage<?> bpstcPage, BpsTcSearchVo bpsTcSearchVo);

	BpsTcDetailVo  getBpsTcDetail(BpsTcSearchVo bpstcsearchvo);

	void updateBpsTc(BpsTcDetailVo bpstcDetailVo);

	void updateBpsTcCode(BpsTcDetailVo bpstcDetailVo);

	void submitBpsTc(BpsTcDetailVo bpstcDetailVo);

	List<BpsFileVo> getBpsTcFile(BpsTcSearchVo bpsTcSearchVo);

	
}
