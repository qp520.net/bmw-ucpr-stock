package com.bmw.ucpr.stock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bmw.ucpr.stock.entity.Demo;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.service
 *
 * @author Bruce Maa
 * @since 2019-07-10.16:05
 */
public interface DemoService extends IService<Demo> {
}
