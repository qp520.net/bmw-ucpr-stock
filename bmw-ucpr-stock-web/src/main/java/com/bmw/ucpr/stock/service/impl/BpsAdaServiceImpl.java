package com.bmw.ucpr.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.BpsAdaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.BpsAdaSearchVo;
import com.bmw.ucpr.stock.entity.BpsAdaVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.repository.BpsAdaMapper;
import com.bmw.ucpr.stock.service.BpsAdaService;

@Service
public class BpsAdaServiceImpl extends ServiceImpl<BpsAdaMapper, BpsAdaVo> implements BpsAdaService {

	@Autowired
	BpsAdaMapper bpsAdaMapper;
	
	@Override
	public Page<BpsAdaVo> getBpsAdaPage(IPage<?> bpsAdaPage, BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getBpsAdaPage(bpsAdaPage, searchCondition);
	}

	@Override
	public List<BpsAdaVo> getBpsAdaList(BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getBpsAdaList(searchCondition);
	}

	@Override
	public List<SelectItem> getSupplierList(BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getSupplierList(searchCondition);
	}

	@Override
	public List<SelectItem> getOCUTypeList(BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getOCUTypeList(searchCondition);
	}

	@Override
	public List<SelectItem> getEcodeList(BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getEcodeList(searchCondition);
	}

	@Override
	public BpsAdaDetailVo getBpsAdaDetail(BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getBpsAdaDetail(searchCondition);
	}
	
	@Override
	public Page<BpsAdaDetailVo> getBpsAdaPageHistory(IPage<?> bpsAdaHistory, BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getBpsAdaPageHistory(bpsAdaHistory, searchCondition);
	}

	/*@Override
	public List<BpsAdaDetailVo> getBpsAdaHistory(BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getBpsAdaHistory(searchCondition);
	}*/

	@Override
	public List<SelectItem> getLookUpList(BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getLookUpList(searchCondition);
	}

	@Override
	public List<BpsAdaFileVo> getBpsAdaFile(BpsAdaSearchVo searchCondition) {
		return bpsAdaMapper.getBpsAdaFile(searchCondition);
	}

	@Override
	public String getBpsAdaApplyValid(BpsAdaSearchVo bpsAdaSearchVo) {
		return bpsAdaMapper.getBpsAdaApplyValid(bpsAdaSearchVo);
	}

	@Override
	public String getBpsAdaApplyDateValid(BpsAdaSearchVo bpsAdaSearchVo) {
		return bpsAdaMapper.getBpsAdaApplyDateValid(bpsAdaSearchVo);
	}

	@Override
	public String getBpsAdaVinValid(BpsAdaSearchVo bpsAdaSearchVo) {
		return bpsAdaMapper.getBpsAdaVinValid(bpsAdaSearchVo);
	}

	@Override
	public String getneedextendedwarr(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getneedextendedwarr(bpsAdaDetailVo);
	}

	@Override
	public String getwarrucwarrenddate(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getwarrucwarrenddate(bpsAdaDetailVo);
	}

	@Override
	public String getwarrendmileage(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getwarrendmileage(bpsAdaDetailVo);
	}

	@Override
	public String getncwarrdays(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getncwarrdays(bpsAdaDetailVo);
	}

	@Override
	public String gettotalstockcost(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.gettotalstockcost(bpsAdaDetailVo);
	}

	@Override
	public String getstockgrossmargin1(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getstockgrossmargin1(bpsAdaDetailVo);
	}

	@Override
	public String getstockgrossmargin2(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getstockgrossmargin2(bpsAdaDetailVo);
	}

	@Override
	public String getstockgrossmarginrate1(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getstockgrossmarginrate1(bpsAdaDetailVo);
	}

	@Override
	public String getstockgrossmarginrate2(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getstockgrossmarginrate2(bpsAdaDetailVo);
	}

	@Override
	public String getstockcommission(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getstockcommission(bpsAdaDetailVo);
	}

	@Override
	public String getstockcommissionrate(BpsAdaDetailVo bpsAdaDetailVo) {
		return bpsAdaMapper.getstockcommissionrate(bpsAdaDetailVo);
	}

	@Override
	public void updateBpsAda(BpsAdaDetailVo bpsAdaDetailVo) {
		bpsAdaMapper.updateBpsAda(bpsAdaDetailVo);
		
	}

	@Override
	public void submitBpsAda(BpsAdaDetailVo bpsAdaDetailVo) {
		bpsAdaMapper.submitBpsAda(bpsAdaDetailVo);
	}

	@Override
	public void updateBpsAdaCode(BpsAdaDetailVo bpsAdaDetailVo) {
		bpsAdaMapper.updateBpsAdaCode(bpsAdaDetailVo);
	}

	@Override
	public void submitBpsAdaCode(BpsAdaDetailVo bpsAdaDetailVo) {
		bpsAdaMapper.submitBpsAdaCode(bpsAdaDetailVo);
	}

}
