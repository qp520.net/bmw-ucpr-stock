package com.bmw.ucpr.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.BpsAmaADetailVo;
import com.bmw.ucpr.stock.entity.BpsAmaASearchVo;
import com.bmw.ucpr.stock.entity.BpsAmaAVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.repository.BpsAmaAMapper;
import com.bmw.ucpr.stock.service.BpsAmaAService;

@Service
public class BpsAmaAServiceImpl extends ServiceImpl<BpsAmaAMapper, BpsAmaAVo> implements BpsAmaAService {

	@Autowired
	BpsAmaAMapper bpsAmaAMapper;
	
	@Override
	public Page<BpsAmaAVo> getBpsAmaAPage(IPage<?> bpsAmaAPage, BpsAmaASearchVo condition) {
		return bpsAmaAMapper.getBpsAmaAPage(bpsAmaAPage, condition);
	}

	@Override
	public List<SelectItem> getPolicyList(BpsAmaASearchVo condition) {
		return bpsAmaAMapper.getPolicyList(condition);
	}

	@Override
	public List<BpsAmaAVo> getBpsAmaAList(BpsAmaASearchVo condition) {
		return bpsAmaAMapper.getBpsAmaAList(condition);
	}

	@Override
	public BpsAmaADetailVo getBpsAmaADetail(BpsAmaASearchVo condition) {
		return bpsAmaAMapper.getBpsAmaADetail(condition);
	}

	@Override
	public List<BpsAmaADetailVo> getBpsAmaAHistory(BpsAmaASearchVo condition) {
		return bpsAmaAMapper.getBpsAmaAHistory(condition);
	}

	@Override
	public String getBpsAmaApplyResetValid(BpsAmaASearchVo condition) {
		return bpsAmaAMapper.getBpsAmaApplyResetValid(condition);
	}

	@Override
	public void updateBpsAmaA(BpsAmaADetailVo bpsAmaADetailVo) {
		bpsAmaAMapper.updateBpsAmaA(bpsAmaADetailVo);
	}

	@Override
	public void submitBpsAmaA(BpsAmaADetailVo bpsAmaADetailVo) {
		bpsAmaAMapper.submitBpsAmaA(bpsAmaADetailVo);
	}

}
