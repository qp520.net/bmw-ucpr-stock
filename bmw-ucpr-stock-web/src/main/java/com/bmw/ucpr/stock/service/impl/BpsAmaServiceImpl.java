package com.bmw.ucpr.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.BpsAmaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAmaFileVo;
import com.bmw.ucpr.stock.entity.BpsAmaSearchVo;
import com.bmw.ucpr.stock.entity.BpsAmaVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.repository.BpsAmaMapper;
import com.bmw.ucpr.stock.service.BpsAmaService;

@Service
public class BpsAmaServiceImpl extends ServiceImpl<BpsAmaMapper, BpsAmaVo> implements BpsAmaService {
	
	@Autowired
	BpsAmaMapper bpsAmaMapper;

	@Override
	public Page<BpsAmaVo> getBpsAmaPage(IPage<?> bpsAmaPage, BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaPage(bpsAmaPage, condition);
	}

	@Override
	public List<SelectItem> getSupplierList(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getSupplierList(condition);
	}

	@Override
	public List<SelectItem> getOCUTypeList(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getOCUTypeList(condition);
	}

	@Override
	public List<SelectItem> getEcodeList(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getEcodeList(condition);
	}

	@Override
	public List<BpsAmaVo> getBpsAmaList(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaList(condition);
	}

	@Override
	public int getBpsAmaListCount(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaListCount(condition);
	}

	@Override
	public BpsAmaDetailVo getBpsAmaDetail(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaDetail(condition);
	}

	/*@Override
	public List<BpsAmaDetailVo> getBpsAmaHistory(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaHistory(condition);
	}*/
	
	@Override
	public Page<BpsAmaDetailVo> getBpsAmaHistoryPage(IPage<?> bpsAmaHistoryPage, BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaHistoryPage(bpsAmaHistoryPage, condition);
	}

	@Override
	public List<BpsAmaFileVo> getBpsAmaFile(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaFile(condition);
	}

	@Override
	public BpsAmaFileVo getBpsAmaDocD(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaDocD(condition);
	}

	@Override
	public String getBpsAmaCanApply(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaCanApply(condition);
	}

	@Override
	public String getBpsAmaApplyDate(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaApplyDate(condition);
	}

	@Override
	public String getBpsAmaApplyValid(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaApplyValid(condition);
	}

	@Override
	public String getBpsAmaApplyAuditValid(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getBpsAmaApplyAuditValid(condition);
	}

	@Override
	public List<SelectItem> getLookUpList(BpsAmaSearchVo condition) {
		return bpsAmaMapper.getLookUpList(condition);
	}

	@Override
	public void submitBpsAma(BpsAmaDetailVo bpsAmaDetailVo) {
		bpsAmaMapper.submitBpsAma(bpsAmaDetailVo);
	}

	@Override
	public void submitBpsAmaBatch(BpsAmaDetailVo bpsAmaDetailVo) {
		bpsAmaMapper.submitBpsAmaBatch(bpsAmaDetailVo);
	}

	@Override
	public void submitBpsAmaCode(BpsAmaDetailVo bpsAmaDetailVo) {
		bpsAmaMapper.submitBpsAmaCode(bpsAmaDetailVo);
	}

	@Override
	public void updateBpsAma(BpsAmaDetailVo bpsAmaDetailVo) {
		bpsAmaMapper.updateBpsAma(bpsAmaDetailVo);
	}

}
