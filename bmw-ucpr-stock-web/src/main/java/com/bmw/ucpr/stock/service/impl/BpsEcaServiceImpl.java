package com.bmw.ucpr.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.BpsEcaDetailVo;
import com.bmw.ucpr.stock.entity.BpsEcaSearchVo;
import com.bmw.ucpr.stock.entity.BpsEcaVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.repository.BpsEcaMapper;
import com.bmw.ucpr.stock.service.BpsEcaService;

@Service
public class BpsEcaServiceImpl extends ServiceImpl<BpsEcaMapper, BpsEcaVo> implements BpsEcaService {
	
	@Autowired
	BpsEcaMapper bpsEcaMapper;

	@Override
	public Page<BpsEcaVo> getBpsEcaPage(IPage<?> bpsEcaPage, BpsEcaSearchVo condition) {
		return bpsEcaMapper.getBpsEcaPage(bpsEcaPage, condition);
	}

	@Override
	public List<SelectItem> getCbuNoList(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getCbuNoList(condition);
	}

	@Override
	public List<SelectItem> getCkdNoList(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getCkdNoList(condition);
	}

	@Override
	public List<SelectItem> getStatusList(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getStatusList(condition);
	}

	@Override
	public List<SelectItem> getOCUTypeList(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getOCUTypeList(condition);
	}

	@Override
	public BpsEcaDetailVo getBpsEcaDetail(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getBpsEcaDetail(condition);
	}

	/*@Override
	public List<BpsEcaDetailVo> getBpsEcaHistory(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getBpsEcaHistory(condition);
	}*/
	
	@Override
	public Page<BpsEcaDetailVo> getBpsEcaHistoryPage(IPage<?> bpsEcaHistoryPage, BpsEcaSearchVo condition) {
		return bpsEcaMapper.getBpsEcaHistoryPage(bpsEcaHistoryPage, condition);
	}

	@Override
	public List<BpsAdaFileVo> getBpsEcaFile(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getBpsEcaFile(condition);
	}

	@Override
	public BpsAdaFileVo getBpsEcaFileContent(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getBpsEcaFileContent(condition);
	}

	@Override
	public List<SelectItem> getBpsEcaFileReason(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getBpsEcaFileReason(condition);
	}

	@Override
	public void insertBpsEcaFile(BpsAdaFileVo bpsFileVo) {
		bpsEcaMapper.insertBpsEcaFile(bpsFileVo);
	}

	@Override
	public void insertBpsEcaFileBatch(List<BpsAdaFileVo> bpsFileVo) {
		bpsEcaMapper.insertBpsEcaFileBatch(bpsFileVo);
	}

	@Override
	public void updateBpsEcaFile(BpsAdaFileVo bpsFileVo) {
		bpsEcaMapper.updateBpsEcaFile(bpsFileVo);;
	}

	@Override
	public void updateBpsEcaFileBatch(List<BpsAdaFileVo> bpsFileVo) {
		bpsEcaMapper.updateBpsEcaFileBatch(bpsFileVo);
		
	}

	@Override
	public int checkBpsEcaFile(BpsEcaSearchVo condition) {
		return bpsEcaMapper.checkBpsEcaFile(condition);
	}

	@Override
	public List<SelectItem> getLookUpList(BpsEcaSearchVo condition) {
		return bpsEcaMapper.getLookUpList(condition);
	}

	@Override
	public void updateBpsEca(BpsEcaDetailVo bpsEcaDetailVo) {
		bpsEcaMapper.updateBpsEca(bpsEcaDetailVo);
	}

	@Override
	public void submitBpsEca(BpsEcaDetailVo bpsEcaDetailVo) {
		bpsEcaMapper.submitBpsEca(bpsEcaDetailVo);
	}

	@Override
	public void submitStockEca(BpsEcaDetailVo bpsEcaDetailVo) {
		bpsEcaMapper.submitStockEca(bpsEcaDetailVo);
	}

	@Override
	public void submitBpsEcaBatch(BpsEcaDetailVo bpsEcaDetailVo) {
		bpsEcaMapper.submitBpsEcaBatch(bpsEcaDetailVo);
		
	}

	@Override
	public void submitBpsEcaCode(BpsEcaDetailVo bpsEcaDetailVo) {
		bpsEcaMapper.submitBpsEcaCode(bpsEcaDetailVo);
	}

	
}
