package com.bmw.ucpr.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.BpsRaDetailVo;
import com.bmw.ucpr.stock.entity.BpsRaSearchVo;
import com.bmw.ucpr.stock.entity.BpsRaVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.repository.BpsRaMapper;
import com.bmw.ucpr.stock.repository.BpsSieMapper;
import com.bmw.ucpr.stock.service.BpsRaService;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月7日 上午11:23:05 

* 类说明 

*/
@Service
public class BpsRaServiceImpl extends ServiceImpl<BpsRaMapper, BpsRaVo> implements BpsRaService {

	@Autowired
	BpsRaMapper bpsRaMapper;
	
	@Override
	public Page<BpsRaVo> getBpsRaPage(IPage<?> bpsRaPage, BpsRaSearchVo bpsRaSearchVo) {
		// TODO Auto-generated method stub
		return bpsRaMapper.getBpsRaPage(bpsRaPage,bpsRaSearchVo);
	}


	@Override
	public BpsRaDetailVo getBpsRaDetail(BpsRaSearchVo bpsRaSearchVo) {
		// TODO Auto-generated method stub
		return bpsRaMapper.getBpsRaDetail(bpsRaSearchVo);
	}


	@Override
	public void submitBpsRa(BpsRaDetailVo bpsRaDetailVo) {
		// TODO Auto-generated method stub
		bpsRaMapper.submitBpsRa(bpsRaDetailVo);
		
	}


	@Override
	public Page<BpsRaDetailVo> getBpsRaHistory(IPage<?> siePage, BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return bpsRaMapper.getBpsRaHistory(siePage,bpsraSearchVo);
	}


	@Override
	public List<SelectItem> getSupplierList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return bpsRaMapper.getSupplierList(bpsraSearchVo);
	}


	@Override
	public List<SelectItem> getStatusList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return bpsRaMapper.getStatusList(bpsraSearchVo);
	}


	@Override
	public List<SelectItem> getCbuNoList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return bpsRaMapper.getCbuNoList(bpsraSearchVo);
	}


	@Override
	public List<SelectItem> getEcodeList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return bpsRaMapper.getEcodeList(bpsraSearchVo);
	}


	@Override
	public List<SelectItem> getCkdNoList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub 
		
		
		return bpsRaMapper.getCkdNoList(bpsraSearchVo);
	}
	
}
