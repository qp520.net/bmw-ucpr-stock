package com.bmw.ucpr.stock.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.StockCommentVo;
import com.bmw.ucpr.stock.entity.StockConditionEntity;
import com.bmw.ucpr.stock.entity.StockVo;
import com.bmw.ucpr.stock.repository.StockDelApplicationMapper;
import com.bmw.ucpr.stock.service.BpsSdaService;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.service.impl
 *
 */
@Service
public class BpsSdaServiceImpl extends ServiceImpl<StockDelApplicationMapper, StockVo> implements BpsSdaService {

	@Autowired
	StockDelApplicationMapper stockDelApplicationMapper;
	
	@Override
	public Page<StockVo> getStockList(IPage<?> page, StockConditionEntity condition) {
		return stockDelApplicationMapper.getStockList(page, condition);
	}

	@Override
	public String getSwitchFlg(StockConditionEntity condition) {
		return stockDelApplicationMapper.getSwitchFlg(condition);
	}

	@Override
	public StockCommentVo getComment(StockConditionEntity condition) {
		return stockDelApplicationMapper.getComment(condition);
	}

	@Override
	public void updateComment(StockCommentVo vo) {
		stockDelApplicationMapper.updateComment(vo);
	}
}
