package com.bmw.ucpr.stock.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.StockApproveVo;
import com.bmw.ucpr.stock.entity.StockConditionEntity;
import com.bmw.ucpr.stock.entity.StockVo;
import com.bmw.ucpr.stock.repository.StockDelManagementMapper;
import com.bmw.ucpr.stock.service.BpsSdmService;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.service.impl
 *
 */
@Service
public class BpsSdmServiceImpl extends ServiceImpl<StockDelManagementMapper, StockVo> implements BpsSdmService {

	@Autowired
	StockDelManagementMapper stockDelManagementMapper;
	
	@Override
	public StockApproveVo getDelComment(StockConditionEntity condition) {
		return stockDelManagementMapper.getDelComment(condition);
	}

	@Override
	public void updateApprove(StockApproveVo vo) {
		stockDelManagementMapper.updateApprove(vo);
	}

	@Override
	public void updateHistory(StockApproveVo vo) {
		stockDelManagementMapper.updateHistory(vo);
	}

	@Override
	public void approveProcedure(StockApproveVo vo) {
		stockDelManagementMapper.approveProcedure(vo);
	}

	@Override
	public Page<StockVo> getStockList(IPage<?> page, StockConditionEntity condition) {
		return stockDelManagementMapper.getStockList(page, condition);
	}
}
