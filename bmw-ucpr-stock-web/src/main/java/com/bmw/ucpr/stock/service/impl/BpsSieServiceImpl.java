package com.bmw.ucpr.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.BpsAdaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.BpsSieDetailVo;
import com.bmw.ucpr.stock.entity.BpsSieSearchVo;
import com.bmw.ucpr.stock.entity.BpsSieVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.entity.UcLookup;
import com.bmw.ucpr.stock.repository.BpsSieMapper;
import com.bmw.ucpr.stock.service.BpsSieService;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.service.impl
 *
 */
@Service
public class BpsSieServiceImpl extends ServiceImpl<BpsSieMapper, BpsSieVo> implements BpsSieService {

	@Autowired
	BpsSieMapper bpsSieMapper;
	
	@Override
	public List<SelectItem> getSupplierList(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getSupplierList(bpsSieSearchVo);
	}

	@Override
	public Page<SelectItem> getSupplierPage(IPage<SelectItem> page, BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getSupplierPage(page, bpsSieSearchVo);
	}

	@Override
	public Page<BpsSieVo> getBpsSiePage(IPage<?> page, BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsSiePage(page, bpsSieSearchVo);
	}

	@Override
	public List<BpsSieVo> getBpsSieList(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsSieList(bpsSieSearchVo);
	}

	@Override
	public int getBpsSieListCount(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsSieListCount(bpsSieSearchVo);
	}

	@Override
	public BpsSieDetailVo getBpsSieDetail(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsSieDetail(bpsSieSearchVo);
	}

	@Override
	public List<SelectItem> getEcodeList(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getEcodeList(bpsSieSearchVo);
	}

	@Override
	public List<UcLookup> getLookUpList(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getLookUpList(bpsSieSearchVo);
	}

	@Override
	public String getBpsSieValid(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsSieValid(bpsSieSearchVo);
	}

	@Override
	public String getBpsSieDateValid(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsSieDateValid(bpsSieSearchVo);
	}

	@Override
	public String getBpsSieVinValid(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsSieVinValid(bpsSieSearchVo);
	}

	@Override
	public String gettotalstockcost(BpsSieDetailVo bpsSieDetailVo) {
		return bpsSieMapper.gettotalstockcost(bpsSieDetailVo);
	}

	@Override
	public String getstockgrossmargin1(BpsSieDetailVo bpsSieDetailVo) {
		return bpsSieMapper.getstockgrossmargin1(bpsSieDetailVo);
	}

	@Override
	public String getstockgrossmargin2(BpsSieDetailVo bpsSieDetailVo) {
		return bpsSieMapper.getstockgrossmargin2(bpsSieDetailVo);
	}

	@Override
	public String getstockgrossmarginrate1(BpsSieDetailVo bpsSieDetailVo) {
		return bpsSieMapper.getstockgrossmarginrate1(bpsSieDetailVo);
	}

	@Override
	public String getstockgrossmarginrate2(BpsSieDetailVo bpsSieDetailVo) {
		return bpsSieMapper.getstockgrossmarginrate2(bpsSieDetailVo);
	}

	@Override
	public String getstockcommission(BpsSieDetailVo bpsSieDetailVo) {
		return bpsSieMapper.getstockcommission(bpsSieDetailVo);
	}

	@Override
	public String getstockcommissionrate(BpsSieDetailVo bpsSieDetailVo) {
		return bpsSieMapper.getstockcommissionrate(bpsSieDetailVo);
	}

	@Override
	public void updateBpsSie(BpsSieDetailVo bpsSieDetailVo) {
		bpsSieMapper.updateBpsSie(bpsSieDetailVo);
	}

	@Override
	public void submitBpsSie(BpsSieDetailVo bpsSieDetailVo) {
		bpsSieMapper.submitBpsSie(bpsSieDetailVo);
	}

	@Override
	public void updateBpsSieCode(BpsSieDetailVo bpsSieDetailVo) {
		bpsSieMapper.updateBpsSieCode(bpsSieDetailVo);
	}

	@Override
	public List<SelectItem> getOCUTypeList(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getOCUTypeList(bpsSieSearchVo);
	}

	@Override
	public List<SelectItem> initSaleTypeListByOcu(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.initSaleTypeListByOcu(bpsSieSearchVo);
	}

	@Override
	public void updateOcutype(BpsSieDetailVo bpsSieDetailVo) {
		bpsSieMapper.updateOcutype(bpsSieDetailVo);
	}

	@Override
	public List<BpsAdaDetailVo> getBpsAdaHistory(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsAdaHistory(bpsSieSearchVo);
	}

	@Override
	public List<BpsAdaFileVo> getBpsAdaFile(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsAdaFile(bpsSieSearchVo);
	}

	@Override
	public BpsSieDetailVo getBpsSieDetailSec(BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsSieDetailSec(bpsSieSearchVo);
	}

	@Override
	public Page<BpsAdaDetailVo> getBpsAdaHistoryPage(IPage<?> page, BpsSieSearchVo bpsSieSearchVo) {
		return bpsSieMapper.getBpsAdaHistoryPage(page, bpsSieSearchVo);
	}
}
