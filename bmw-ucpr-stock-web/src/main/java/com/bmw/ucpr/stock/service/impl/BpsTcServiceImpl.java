package com.bmw.ucpr.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.BpsFileVo;
import com.bmw.ucpr.stock.entity.BpsTcDetailVo;
import com.bmw.ucpr.stock.entity.BpsTcSearchVo;
import com.bmw.ucpr.stock.entity.BpsTcVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.repository.BpsSieMapper;
import com.bmw.ucpr.stock.repository.BpsTcMapper;
import com.bmw.ucpr.stock.service.BpsTcService;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月5日 上午11:20:50 

* 类说明 

*/
@Service
public class BpsTcServiceImpl extends ServiceImpl<BpsTcMapper, BpsTcVo> implements BpsTcService{
 
	@Autowired
	BpsTcMapper bpstcmapper;
	
	@Override
	public List<SelectItem> getSupplierList(BpsTcSearchVo bpsTcsearchVo) {
		// TODO Auto-generated method stub
		return bpstcmapper.getSupplierList(bpsTcsearchVo);
		
		
	}

	@Override
	public List<SelectItem> getStatusList(BpsTcSearchVo bpsTcsearchVo) {
		// TODO Auto-generated method stub
		return bpstcmapper.getStatusList(bpsTcsearchVo);	
		
	}

	@Override
	public List<SelectItem> getCbuNoList(BpsTcSearchVo bpsTcsearchVo) {
		// TODO Auto-generated method stub
		return bpstcmapper.getCbuNoList(bpsTcsearchVo);
	}

	@Override
	public List<SelectItem> getCkdNoList(BpsTcSearchVo bpsTcsearchVo) {
		// TODO Auto-generated method stub
		return bpstcmapper.getCkdNoList(bpsTcsearchVo);
	}

	@Override
	public List<SelectItem> getEcodeList(BpsTcSearchVo bpsTcsearchVo) {
		// TODO Auto-generated method stub
		return bpstcmapper.getEcodeList(bpsTcsearchVo);
	}

	@Override
	public Page<BpsTcVo> getBpsTcList(IPage<?> lookupPage, BpsTcSearchVo bpsTcsearchVo) {
		// TODO Auto-generated method stub
		return bpstcmapper.getBpstcList(lookupPage,bpsTcsearchVo);
	}


	@Override
	public BpsTcDetailVo getBpsTcDetail(BpsTcSearchVo bpstcsearchvo) {
		// TODO Auto-generated method stub
		return bpstcmapper.getBpsTcDetail(bpstcsearchvo);
	}

	@Override
	public Page<BpsTcDetailVo> getBpsTcHistory(IPage<?> bpstcPage, BpsTcSearchVo bpsTcSearchVo) {
		// TODO Auto-generated method stub
		return bpstcmapper.getBpsTcHistory(bpstcPage,bpsTcSearchVo);
	   
	}

	@Override
	public void updateBpsTc(BpsTcDetailVo bpstcDetailVo) {
		// TODO Auto-generated method stub
		bpstcmapper.updateBpsTc(bpstcDetailVo);
	}

	@Override
	public void updateBpsTcCode(BpsTcDetailVo bpstcDetailVo) {
		// TODO Auto-generated method stub
		bpstcmapper.updateBpsTcCode(bpstcDetailVo);
	}

	@Override
	public void submitBpsTc(BpsTcDetailVo bpstcDetailVo) {
		// TODO Auto-generated method stub
		bpstcmapper.submitBpsTc(bpstcDetailVo);
	}

	@Override
	public List<BpsFileVo> getBpsTcFile(BpsTcSearchVo bpsTcSearchVo) {
		return bpstcmapper.getBpsTcFile(bpsTcSearchVo);
	}
	
}



