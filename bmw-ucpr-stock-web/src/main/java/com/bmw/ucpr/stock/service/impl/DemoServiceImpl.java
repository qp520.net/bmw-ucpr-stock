package com.bmw.ucpr.stock.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.Demo;
import com.bmw.ucpr.stock.repository.DemoRepository;
import com.bmw.ucpr.stock.service.DemoService;
import org.springframework.stereotype.Service;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.service.impl
 *
 * @author Bruce Maa
 * @since 2019-07-10.16:06
 */
@Service
public class DemoServiceImpl extends ServiceImpl<DemoRepository, Demo> implements DemoService {
}
