package com.bmw.ucpr.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.BpsAdaDetailVo;
import com.bmw.ucpr.stock.entity.BpsAdaFileVo;
import com.bmw.ucpr.stock.entity.OcuChartVo;
import com.bmw.ucpr.stock.entity.OcuStockReportSearchVo;
import com.bmw.ucpr.stock.entity.OcuStockReportVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.repository.StockReportMapper;
import com.bmw.ucpr.stock.service.StockReportService;

/**
 * projectName:bmw-ucpr-stock
 * com.bmw.ucpr.stock.service.impl
 *
 */
@Service
public class StockReportServiceImpl extends ServiceImpl<StockReportMapper, OcuStockReportVo> implements StockReportService {

	@Autowired
	StockReportMapper stockReportMapper;
	
	@Override
	public Page<OcuStockReportVo> getStockReportPage(IPage<?> page, OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getStockReportPage(page, searchCondition);
	}

	@Override
	public List<SelectItem> getSupplierList(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getSupplierList(searchCondition);
	}

	@Override
	public List<SelectItem> getCbuList(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getCbuList(searchCondition);
	}

	@Override
	public List<SelectItem> getCkdList(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getCkdList(searchCondition);
	}

	@Override
	public List<SelectItem> getEcodeList(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getEcodeList(searchCondition);
	}

	@Override
	public List<SelectItem> getOCUTypeList(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getOCUTypeList(searchCondition);
	}

	@Override
	public List<OcuStockReportVo> getStockReportList(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getStockReportList(searchCondition);
	}

	@Override
	public int getStockReportCount(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getStockReportCount(searchCondition);
	}

	@Override
	public OcuStockReportVo getStockReportDetail(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getStockReportDetail(searchCondition);
	}

	@Override
	public List<OcuChartVo> getPieChart(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getPieChart(searchCondition);
	}

	@Override
	public List<OcuChartVo> getBarOrLineChart(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getBarOrLineChart(searchCondition);
	}

	@Override
	public List<BpsAdaDetailVo> getStockReportHistory(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getStockReportHistory(searchCondition);
	}

	@Override
	public List<BpsAdaFileVo> getStockReportFile(OcuStockReportSearchVo searchCondition) {
		return stockReportMapper.getStockReportFile(searchCondition);
	}

}
