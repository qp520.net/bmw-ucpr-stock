package com.bmw.ucpr.stock.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bmw.ucpr.stock.entity.BpsRaDetailVo;
import com.bmw.ucpr.stock.entity.BpsRaSearchVo;
import com.bmw.ucpr.stock.entity.BpsRaVo;
import com.bmw.ucpr.stock.entity.SelectItem;
import com.bmw.ucpr.stock.repository.BpsSieMapper;
import com.bmw.ucpr.stock.repository.UcRaMapper;
import com.bmw.ucpr.stock.service.UcRaService;

/** 

* @author 作者 Your-Name: 

* @version 创建时间：2019年8月6日 下午4:19:57 

* 类说明 

*/
@Service
public class UcRaServiceImpl extends ServiceImpl<UcRaMapper, BpsRaVo> implements UcRaService{

	@Autowired
	UcRaMapper ucRaMapper;
	
	@Override
	public Page<BpsRaVo> getUcraPage(IPage<?> ucraPage, BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return ucRaMapper.getUcraPage(ucraPage,bpsraSearchVo);
		
		
	}

	@Override
	public BpsRaDetailVo getUcRaDetail(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return ucRaMapper.getBpsRaDetail(bpsraSearchVo);
	}

	@Override
	public void updateUcRa(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateUcRaCode(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void submitUcRa(BpsRaDetailVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		ucRaMapper.submitUcRaC(bpsraSearchVo);
	}

	@Override
	public List<SelectItem> getSupplierList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return ucRaMapper.getSupplierList(bpsraSearchVo);
	}

	@Override
	public List<SelectItem> getStatusList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return ucRaMapper.getStatusList(bpsraSearchVo);
	}

	@Override
	public List<SelectItem> getCbuNoList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return ucRaMapper.getCbuNoList(bpsraSearchVo);
	}

	@Override
	public List<SelectItem> getCkdNoList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return ucRaMapper.getCkdNoList(bpsraSearchVo);
	}

	@Override
	public List<SelectItem> getEcodeList(BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return ucRaMapper.getEcodeList(bpsraSearchVo);
	}

	@Override
	public Page<BpsRaDetailVo> getBpsRaHistory(IPage<?> bpsucraPage, BpsRaSearchVo bpsraSearchVo) {
		// TODO Auto-generated method stub
		return ucRaMapper.getBpsRaHistory(bpsucraPage,bpsraSearchVo);
	}

}
