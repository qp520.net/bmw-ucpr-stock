package com.bmw.ucpr.stock.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Project：BMW SSA Web Base
 * System：UCPR
 * 日期工具类
 * @author 
 * 
 */
public class DateUtils {

	public static final String YYYYMMDD = "yyyy-MM-dd";

	/**
	 * 日期转换为字符串
	 * 
	 * @param date
	 * @param format
	 * @return
	 */
	public static String dateToString(Date date, String format) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat df = new SimpleDateFormat(format);
		String strDate = df.format(date);
		return strDate;
	}

	/**
	 * 字符串转换为日期
	 * 
	 * @param dateStr
	 * @param format
	 * @return
	 */
	public static Date stringToDate(String dateStr, String format) {
		if (dateStr == null || "".equals(dateStr)) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		java.util.Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 取得系统当前时间字符串
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurrentDateStr(String format) {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(format);
		String time = df.format(date);
		return time;
	}

	/**
	 * 获取当年的最后一天
	 * 
	 * @return
	 */
	public static Date getCurrentYearLastDate() {
		Calendar currCal = Calendar.getInstance();
		int currentYear = currCal.get(Calendar.YEAR);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, currentYear);
		calendar.roll(Calendar.DAY_OF_YEAR, -1);
		Date currYearLast = calendar.getTime();
		return currYearLast;
	}
	
	
	
	/**
	 *  获取过去的多长时间
	 *  time=Calendar.**,num=-*
	 *  过去的几个小时、日期
	 * @return
	 */

	public static String getUpperTimeString(int time, int num) {
		SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendar = Calendar.getInstance();
		Date date = new Date(System.currentTimeMillis());
		calendar.setTime(date);
		calendar.add(time, num);
		date = calendar.getTime();
		return f.format(date);

	}
	
	/**
	 *  flag 0获取当天凌晨时间
	 *  flag 1获取今天最末时间
	 *  flag 2获取昨天开始时间
	 *  flag 其它值获取当前系统时间
	 * @return
	 */
	
	public static String weeHourString(Date date, int flag) {
		SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		long millisecond = hour * 60 * 60 * 1000 + minute * 60 * 1000 + second
				* 1000;
		if (flag == 0) {
			cal.setTimeInMillis(cal.getTimeInMillis() - millisecond);
			return f.format(cal.getTime());
		} else if (flag == 1) {
			cal.setTimeInMillis(cal.getTimeInMillis() - millisecond);
			cal.setTimeInMillis(cal.getTimeInMillis() + 23 * 60 * 60 * 1000
					+ 59 * 60 * 1000 + 59 * 1000);
		} else if (flag == 2) {
			long millisecond2 = 24 * 60 * 60 * 1000;
			cal.setTimeInMillis(cal.getTimeInMillis() - millisecond
					- millisecond2);
		}
		return f.format(cal.getTime());
	}
	
	
	/**
	 *  获取周一日起
	 *  n=0获取当周周一日期
	 *  大于0的正整数，是几就向下推几周
	 *  小于0的正整数，是几就向上推几周
	 * @return
	 */
	public static String getNowWeekMonday(int flag) {
		SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Calendar currentDate = Calendar.getInstance();
	    currentDate.setFirstDayOfWeek(Calendar.MONDAY);
	    currentDate.set(Calendar.HOUR_OF_DAY, 0);
	    currentDate.set(Calendar.MINUTE, 0);
	    currentDate.set(Calendar.SECOND, 0);
	    currentDate.set(Calendar.MILLISECOND, 0);
	    currentDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
	    Date date=currentDate.getTime();
	    if(flag == 0){
	    	 return f.format(date.getTime());
	    }else if (flag == 1) {
	    	return f.format(date.getTime() - 7 * 24 * 60 * 60 * 1000);
		}
		return null;
	}
	
	 
	/**
	 *  获取最近月份的第*天
	 *  n=0获取上月最后一天
	 *  大于0的正整数，是几就从1号向下向下推几天
	 *  小于0的正整数，是几就从上月最后一天向上推几天
	 * @return
	 */
	public static String getMondayForstDay(int n) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();    
		c.set(Calendar.HOUR_OF_DAY, 0);
	    c.set(Calendar.MINUTE, 0);
	    c.set(Calendar.SECOND, 0);
	    c.set(Calendar.MILLISECOND, 0);
		c.add(Calendar.MONTH, 0);
		//设置为1号,当前日期既为本月第一天 
		c.set(Calendar.DAY_OF_MONTH,n);
		String first = format.format(c.getTime());
		return first;
	}
	
	
	
	   /**
	     * 获取前半年的开始时间
	     * @return
	     */
    
    public  static String getHalfYearStartTimeString(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int currentMonth = c.get(Calendar.MONTH) + 1;
        Date now = null;
        try {
            if (currentMonth >= 1 && currentMonth <= 6){
                c.set(Calendar.MONTH, 0);
            }else if (currentMonth >= 7 && currentMonth <= 12){
                c.set(Calendar.MONTH, 6);
            }
            c.set(Calendar.DATE, 1);
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return longSdf.format(now);
        
    }
    
    
    /**
     * 获取后半年的结束时间
     * @return
     */
    
    public static  String getHalfYearEndTimeString(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int currentMonth = c.get(Calendar.MONTH) ;
        Date now = null;
        try {
            if (currentMonth >= 0 && currentMonth <= 6){
                c.set(Calendar.MONTH, 5);
                c.set(Calendar.DATE, 30);
            }else if (currentMonth >= 7 && currentMonth <= 12){
                c.set(Calendar.MONTH, 11);
                c.set(Calendar.DATE, 31);
            }
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return longSdf.format(now);
    }
    
    /**
     * 当前年的开始时间，即2016-01-01 00:00:00
     *
     * @return
     */
    
    public static  String getCurrentYearStartTimeString() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = null;
        try {
            c.set(Calendar.MONTH, 0);
            c.set(Calendar.DATE, 1);
            now = shortSdf.parse(shortSdf.format(c.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return f.format(now);
    } 
    /**
     * 为date1加一年
     * @param date1
     * @return
     */
    public static Date addOneYear(Date date1){
	    Calendar rightNow = Calendar.getInstance();
		rightNow.setTime(date1);
		rightNow.add(Calendar.YEAR,1);//日期加1年
		Date dt1=rightNow.getTime();
		return dt1;
    }
    
    /** 
     * 得到几天前的时间 
     * @param d 
     * @param day 
     * @return 
     */  
    public static Date getDateBefore(Date d,int day){  
     Calendar now =Calendar.getInstance();  
     now.setTime(d);  
     now.set(Calendar.DATE,now.get(Calendar.DATE)-day);  
     return now.getTime();  
    }
    
    /** 
     * 得到几天后的时间 
     * @param d 
     * @param day 
     * @return 
     */  
    public static Date getDateAfter(Date d,int day){  
     Calendar now =Calendar.getInstance();  
     now.setTime(d);  
     now.set(Calendar.DATE,now.get(Calendar.DATE)+day);  
     return now.getTime();  
    } 
    
}
